# -*- coding: utf-8 -*-
# Django settings for OpenShift project.
import imp, os, string
from decimal import Decimal
import djcelery
djcelery.setup_loader()

# a setting to determine whether we are running on OpenShift
ON_OPENSHIFT = False
if os.environ.has_key('OPENSHIFT_REPO_DIR'):
    ON_OPENSHIFT = True

SESSION_COOKIE_SECURE = ON_OPENSHIFT
CSRF_COOKIE_SECURE = ON_OPENSHIFT

PROJECT_DIR = os.path.dirname(os.path.realpath(__file__))
if ON_OPENSHIFT:
    DEBUG = bool(os.environ.get('DEBUG', False))
    if DEBUG:
        print("WARNING: The DEBUG environment is set to True.")
else:
    DEBUG = True
TEMPLATE_DEBUG = DEBUG

if DEBUG:
    ALLOWED_HOSTS = []
else:
    ALLOWED_HOSTS = ['*']

ADMINS = (
    ('Hisham', 'hveeran@smartkart.ca'),
    ('Sami', 'sami@smartkart.ca'),
    ('Karan', 'karan@smartkart.ca'),
)
MANAGERS = ADMINS

if ON_OPENSHIFT:
    # os.environ['OPENSHIFT_MYSQL_DB_*'] variables can be used with databases created
    # with rhc cartridge add (see /README in this git repo)
    DATABASES = {
        'default': {
            'ENGINE' : 'django.db.backends.postgresql_psycopg2',
            'NAME': os.environ['OPENSHIFT_APP_NAME'],
            'USER': os.environ['OPENSHIFT_POSTGRESQL_DB_USERNAME'],
            'PASSWORD': os.environ['OPENSHIFT_POSTGRESQL_DB_PASSWORD'],
            'HOST': os.environ['OPENSHIFT_POSTGRESQL_DB_HOST'],
            'PORT': os.environ['OPENSHIFT_POSTGRESQL_DB_PORT']
        }
    }
else:
    DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'prod',  # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'adminlk3ra9u',
        'PASSWORD': '5ILPU_ttUmjb',
        'HOST': '127.0.0.1',                 # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '60536',                      # Set to empty string for default.
    }
}
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Toronto'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.environ.get('OPENSHIFT_DATA_DIR', '')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
# STATIC_ROOT = os.path.join(PROJECT_DIR, '..', 'static')
if ON_OPENSHIFT:
    STATIC_ROOT = os.path.join(os.environ.get('OPENSHIFT_REPO_DIR'), 'wsgi', 'static')
    #STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')
else:
    STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
if not ON_OPENSHIFT:
    STATICFILES_DIRS = (os.path.join(os.path.dirname(PROJECT_DIR),'static'),)
else:
    STATICFILES_DIRS = (
        # Put strings here, like "/home/html/static" or "C:/www/django/static".
        # Always use forward slashes, even on Windows.
        # Don't forget to use absolute paths, not relative paths.
    )

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make a dictionary of default keys
default_keys = { 'SECRET_KEY': 'vm4rl5*ymb@2&d_(gc$gb-^twq9w(u69hi--%$5xrh!xk(t%hw' }

# Replace default keys with dynamic values if we are in OpenShift
use_keys = default_keys
if ON_OPENSHIFT:
    imp.find_module('openshiftlibs')
    import openshiftlibs
    use_keys = openshiftlibs.openshift_secure(default_keys)

# Make this unique, and don't share it with anybody.
SECRET_KEY = use_keys['SECRET_KEY']

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_DIR, 'templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'usermanagement',
    'product',
    'store',
    'cart',
    'order',
    'payment',
    'coupon',

    'tastypie',    
    'south',
    'stripe',
    'djcelery',
    'djcelery_email',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.

LOGGING = {
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': ['require_debug_false'],
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

############################   SMARTKART CUSTOM SETTINGS   ######################################
# SmartKart Mark Up Category 
MARK_UP_RATE1 = 1.25
MARK_UP_RATE2 = 1.25
MARK_UP_RATE3 = 1.25
MARK_UP_RATE4 = 1.20
MARK_UP_RATE5 = 1.20
MARK_UP_RATE6 = 1.20
MARK_UP_RATE7 = 1.20
MARK_UP_RATE8 = 1.20
MARK_UP_RATE9 = 1.20
MARK_UP_RATE10 = 1.20
MARK_UP_RATE11 = 1.20
MARK_UP_RATE12 = 1.20
MARK_UP_RATE13 = 1.20
MARK_UP_RATE14 = 1.20
MARK_UP_RATE15 = 1.20

#To Round Decimal to 2 digits
TWO_PLACES = Decimal(10) ** -2 


#Driver's Pay Calculator
DRIVERS_BASE_PAY = 4.99
DRIVERS_COMMISION_BASE = 20

# SmartKart Coupon Generator
COUPON_TYPES = getattr('settings', 'COUPONS_COUPON_TYPES', (
        ('monetary', 'Money Coupon'),
        ('percentage', 'Percentage Coupon'),
        ('free_delivery', 'Free Delivery Coupon'),
        ('multiple_monetary', 'Multiple Monetary'),
        ('multiple_percentage', 'Multiple Percentage'),
    ))

CODE_LENGTH = getattr('settings', 'COUPNS_CODE_LENGTH', 5)

CODE_CHARS = getattr('settings', 'COUPNS_CODE_CHARS', string.letters+string.digits)

#SmartKart Custom
AUTO_LOGOUT_DELAY = 60
TAX_RATE = 0.13

TASTYPIE_DEFAULT_FORMATS = ['json']

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

AUTH_USER_MODEL = 'usermanagement.User'

AUTHENTICATION_BACKENDS = (
    'usermanagement.backends.SmartKartDjangoBackend',
)

#Strip Test Key
STRIPE_API_KEY = 'sk_live_bP0yD1zR5Xki3sMGoyrQ9XX6'

#Strip Production Key
#STRIPE_API_KEY = 'sk_test_wyGh7aFc2huGul4afo70R8jm'

#Email
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'auto-confirm@smartkart.ca'
EMAIL_HOST_PASSWORD = 'confirm$k@rt123'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'TIMEOUT': 30
    }
}

#Celery and Redis
if ON_OPENSHIFT:
    BROKER_URL = "redis://5489219c5973ca4b3c000120-smartkartca.rhcloud.com/0"
    CELERY_RESULT_BACKEND = "redis"
    CELERY_REDIS_HOST = "5489219c5973ca4b3c000120-smartkartca.rhcloud.com"
    CELERY_REDIS_PORT = 36416
    CELERY_REDIS_DB = 0
    

################################################################################################
