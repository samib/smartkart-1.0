from django.conf.urls import patterns, url, include
# from tastypie.api import Api
from product.api import ProductResource#, DepartmentResource, AisleResource


# v1_api = Api(api_name='v1')
# v1_api.register(ProductResource())
# v1_api.register(DepartmentResource())
# v1_api.register(AisleResource())

product_resource = ProductResource()

urlpatterns = patterns('',
    url(r'^add_product/$', 'product.views.addProduct', None, 'addProduct'),
    url(r'^api/', include(product_resource.urls)),
#     url(r'^api/', include(v1_api.urls)),  
);


