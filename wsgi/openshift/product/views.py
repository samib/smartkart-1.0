from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def addProduct(request):
    return render(request, 'general/addproducts.html')