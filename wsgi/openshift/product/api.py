import json

from decimal import Decimal

from django.conf.urls import url
from django.core.exceptions import ObjectDoesNotExist
from django import forms

from tastypie.validation import FormValidation
from tastypie.authorization import Authorization
from tastypie.authentication import SessionAuthentication
from tastypie.exceptions import BadRequest
from tastypie.resources import ModelResource
from tastypie.throttle import CacheThrottle
from tastypie.utils import trailing_slash

from product.models import Product

class ProductForm(forms.ModelForm):

    class Meta:
        model = Product

class PriceChangeNotificationAuthorization(Authorization):
    def read_list(self, object_list, bundle):
        change = bundle.request.GET.get('change', '')
        if(bundle.request.user.is_admin and change =='true'):
            return object_list.filter(price_change=True)
        else:
            raise BadRequest('Not admin or invalid parameter provided')
        
    def read_detail(self, object_list, bundle):
        # if(bundle.request.user.is_admin):
        #     return object_list.all()
        # else:
        raise BadRequest('Not admin')
        
class ProductResource(ModelResource):
    class Meta:
        queryset = Product.objects.all()
        resource_name = 'product'        
        allowed_methods = ['get', 'post', 'patch', 'put'] 
        include_resource_uri = True
        limit = 0
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        validation = FormValidation(form_class=ProductForm)
        authorization = PriceChangeNotificationAuthorization()
        #fields = ['id','name', 'brand_name', 'description', 'image_url', 'price', 'store_id', 'department_id', 'aisle_id',
        #           'taxable', 'product_available', 'position_on_aisle']
        #excludes = ['cost_price']        
        
    def prepend_urls(self):
        return [
            url(r'^(?P<resource_name>%s)/admin/add_product%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('add_product'), name='addProduct'),
            url(r'^(?P<resource_name>%s)/update%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update_product'), name='api_update_product'), #TODO: Add admin to url
            url(r'^(?P<resource_name>%s)/price%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('notify_price_change'), name='api_notify_price_change'),
            url(r'^admin/(?P<resource_name>%s)/price/approve%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('approve_price_change'), name='api_approve_price_change'),
            url(r'^admin/(?P<resource_name>%s)/price/change%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update_price'), name='api_update_price'),
#             url(r'^admin/(?P<resource_name>%s)/price/changes%s$' %
#                 (self._meta.resource_name, trailing_slash()),
#                 self.wrap_view('get_price_changes'), name='api_get_price_changes'),
        ]
            
    def add_product(self, request, **kwargs):
        self.method_check(request, allowed=['post'])    
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin has access to add products'
            })
        else:
            data = json.loads(request.body)
            name = data.get('name', '')
            brand_name = data.get('brand_name', '')
            description = data.get('description', '')
            cost_price = data.get('cost_price', '')
            image_url = data.get('image_url', '')
            store_id = data.get('store_id', '')
            department_id = data.get('department_id', '')
            aisle_id = data.get('aisle_id', '')
            taxable = data.get('taxable', '')
            mark_up_category = data.get('mark_up_category')
            position_on_aisle = data.get('position_on_aisle')
            size = data.get('size', '')
            leProduit = Product.objects.create_product(name=name, brand_name=brand_name, description=description, cost_price=cost_price, mark_up_category=int(mark_up_category),
                                           image_url = image_url, store_id=store_id, department_id=department_id, aisle_id=aisle_id, taxable=taxable,
                                            position_on_aisle=position_on_aisle, size=size)
            
            return self.create_response(request, {
                'success': True,
                'message': 'Sucessfully added the product',
                'new_product_id' : leProduit.id
            })

    def notify_price_change(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        if not (request.user.is_admin or request.user.is_driver):
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin/driver can access this'
            })
        else:            
            data = json.loads(request.body)
            product_id = data.get('id', '')
            modified_price = Decimal(data.get('new_price', ''))
            product = Product.objects.notify_price_change(product_id=product_id, modified_price=modified_price)            
            if(product):
                return self.create_response(request, {
                     'success': True,
                     'message': 'Successfully notified price change'
                })
            else:
                return self.create_response(request, {
                     'success': False,
                     'message': 'Failed to notify price change, try again'
                })

    def update_price(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        if not (request.user.is_admin):
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin can access this'
            })
        else:            
            data = json.loads(request.body)
            product_id = data.get('id', '')
            new_cost_price = Decimal(data.get('new_cost_price', ''))
            new_selling_price = Decimal(data.get('new_selling_price', ''))
            try:
                product = Product.objects.get(id=product_id)
                product.cost_price = new_cost_price
                product.price = new_selling_price
                product.save()
                return self.create_response(request, {
                     'success': True,
                     'message': 'Successfully update price'
                })
            except ObjectDoesNotExist:
                return self.create_response(request, {
                     'success': False,
                     'message': 'Cannot find the specified product'
                })
    
#     def get_price_changes(self, request, **kwargs):
#         self.method_check(request, allowed=['get'])
#         self.is_authenticated(request)
#         if not (request.user.is_admin):
#             return self.create_response(request, {
#                 'success': False,
#                 'message': 'Only Admin can access get_price_changes'
#             })
#         else:
#             products = Product.objects.filter(price_change=True)
#             dictionaries = [p.as_dict() for p in products]
#             
#             return self.create_response(request, {
#                  'success': True,
#                  'products': dictionaries
#             })
            
    def approve_price_change(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        if not (request.user.is_admin):
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin can access approve price change'
            })
        else:            
            data = json.loads(request.body)
            product_id = data.get('id', '')
            product = Product.objects.get(pk=product_id)
            product.approve_price_change()
            product.save()
            
            return self.create_response(request, {
                 'success': True,
                 'message': 'Successfully modified the price'
            })
                    
    def update_product(self, request, **kwargs):
        self.method_check(request, allowed=['put'])    
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin has access to modify products'
            })
        else:            
            data = json.loads(request.body)
            product_id = data.get('id', '')
            name = data.get('name', '')
            brand_name = data.get('brand_name', '')
            description = data.get('description', '')
            cost_price = data.get('cost_price', '')
            image_url = data.get('image_url', '')
            store_id = data.get('store_id', '')
            department_id = data.get('department_id', '')
            aisle_id = data.get('aisle_id', '')
            taxable = data.get('taxable', '')
            product_available = data.get('product_available')
            mark_up_category = data.get('mark_up_category')
            position_on_aisle = data.get('position_on_aisle')
            size = data.get('size')
            Product.objects.update_product_info(product_id=product_id, name=name, brand_name=brand_name, description=description, cost_price=cost_price,
                                            mark_up_category = mark_up_category, image_url = image_url, store_id=store_id, department_id=department_id, aisle_id=aisle_id,
                                            taxable=taxable, product_available=product_available, position_on_aisle=position_on_aisle, size=size)
            return self.create_response(request, {
                'success': True,
                'message': 'Sucessfully updated product'
            })