from decimal import Decimal

from django.db import models

import openshift.settings
#from store.models import Store, Department, Aisle

class ProductManger(models.Manager):
    def create_product(self, name, brand_name, description, image_url, cost_price, mark_up_category, store_id, department_id, aisle_id, taxable, position_on_aisle, size):
        
        if not name:
            raise ValueError('products must have a name')
        
        if not image_url:
            raise ValueError('products must have a image')
        
        if not cost_price:
            raise ValueError('products must have a cost price')
        
        if not mark_up_category:
            raise ValueError('products should have mark_up_category')
            
        if not store_id:
            raise ValueError('Products must have a store')
        
        if not department_id:
            raise ValueError('Products must be assigned to a store department')
        
        if not aisle_id:
            raise ValueError('Products must be assigned to a store department aisle')
        
        product = self.create(name = name, brand_name = brand_name, description=description, image_url = image_url,
                               cost_price = cost_price, mark_up_category=mark_up_category, store_id=store_id, department_id=department_id,
                               aisle_id=aisle_id, taxable=taxable, position_on_aisle = position_on_aisle, size = size, modified_price=0)
        product.set_price()
        product.set_product_available()
        product.save()
        return product
    
    def update_product_availablity_status(self, product_id, status):
        return Product.objects.filter(pk = product_id).update(product_available = status)
    
    def get_products_for_store(self, store_id):
        return Product.objects.filter(store__exact=store_id)
    
    def modify_price(self, product_id, price):
        return Product.objects.filter(pk = product_id).update(price = price)
         
    def modify_availability(self, product_id, product_available):
        return Product.objects.filter(pk=product_id).update(product_available=product_available)
    
    def update_product_info(self, product_id, name, brand_name, description, image_url, cost_price, mark_up_category, store_id, department_id, aisle_id, taxable, product_available, position_on_aisle, size):
            if not product_id:
                raise ValueError('update_product_info must have a product_id') 
            
            if not name:
                raise ValueError('update_product_info must have a name')
            
            if not image_url:
                raise ValueError('update_product_info must have a image')
            
            if not cost_price:
                raise ValueError('update_product_info must have a cost price')
            
            if not mark_up_category:
                raise ValueError('update_product_info should have mark_up_category')
            
            if not store_id:
                raise ValueError('update_product_info must have a store')
            
            if not department_id:
                raise ValueError('update_product_info must be assigned to a store department')
            
            if not aisle_id:
                raise ValueError('update_product_info must be assigned to a store department aisle')
        
            product = Product.objects.get(pk=product_id)            
            product.update_info(name = name, brand_name = brand_name, description=description, image_url=image_url,
                                   cost_price = cost_price, mark_up_category=mark_up_category, store=store_id,
                                   department=department_id, aisle=aisle_id, taxable=taxable, product_available=product_available,
                                   position_on_aisle = position_on_aisle, size = size, modified_price=0)
            product.set_price()
            product.save()         
            return product
        
    def notify_price_change(self, product_id, modified_price):
            
            if not product_id:
                raise ValueError('notify_price_change must have product_id')
            
            if not modified_price:
                raise ValueError('notify_price_change must have modified_price')
            
            product = Product.objects.filter(pk=product_id).update(price_change=True, modified_price=modified_price)
            return product
        
class Product(models.Model):
    objects = ProductManger()
    name = models.CharField(max_length=200) #LCBO: name
    brand_name = models.CharField(max_length=100, blank=True, null=True) #LCBO: producer_name
    description = models.TextField(blank=True, null=True) #LCBO: description
    image_url = models.CharField(max_length=200) #LCBO: image_thumb_url   
    cost_price = models.DecimalField(max_digits=6, decimal_places=2) #LCBO: regular_price_in_cents, covert to dollar amount
    price = models.DecimalField(max_digits=6, decimal_places=2, null=True)
    mark_up_category = models.SmallIntegerField()
    store = models.ForeignKey('store.Store', related_name='store')   
    department = models.ForeignKey('store.Department')
    aisle = models.ForeignKey('store.Aisle', related_name="products")
    taxable = models.BooleanField()
    product_available = models.BooleanField(default = False) #LCBO: is_dead
    position_on_aisle = models.CharField(max_length=50, blank=True, null=True)
    size = models.CharField(max_length=200, blank=True, null=True) #LCBO: package
    price_change = models.BooleanField(default=False)
    modified_price = models.DecimalField(max_digits=6, decimal_places=2, null=True)

    ## The following properties were added as part of LCBO integration ##

    store_product_id = models.IntegerField(blank=True, null=True) #LCBO: id
    package_unit_type = models.CharField(max_length=50, blank=True, null=True)
    package_unit_volume_in_milliliters = models.IntegerField(blank=True, null=True)
    total_package_units = models.SmallIntegerField(null=True)
    alcohol_content = models.DecimalField(max_digits=6, decimal_places=2, null=True)
    price_per_liter_of_alcohol_in_cents = models.IntegerField(blank=True, null=True)
    price_per_liter_in_cents = models.IntegerField(blank=True, null=True)
    sugar_content = models.CharField(max_length=50, blank=True, null=True)
    has_value_added_promotion = models.BooleanField(default=False)
    has_limited_time_offer = models.BooleanField(default=False)
    is_seasonal = models.BooleanField(default=False)
    is_vqa = models.BooleanField(default=False)
    is_kosher = models.BooleanField(default=False) 
    tasting_note = models.CharField(max_length=200, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    large_image_url = models.CharField(max_length=200, blank=True, null=True)  #LCBO: image_url
    sugar_in_grams_per_liter = models.IntegerField(blank=True, null=True)
    clearance_sale_savings_in_cents = models.IntegerField(blank=True, null=True)
    has_clearance_sale = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name
    
    def set_price(self):
        if self.mark_up_category == 1:
            self.price = Decimal(self.cost_price) * Decimal(openshift.settings.MARK_UP_RATE1)
        elif self.mark_up_category == 2:
            self.price = Decimal(self.cost_price) * Decimal(openshift.settings.MARK_UP_RATE2)
        elif self.mark_up_category == 3:
            self.price = Decimal(self.cost_price) * Decimal(openshift.settings.MARK_UP_RATE3)
        else:
            self.price = Decimal(self.cost_price) * Decimal(1.20)
                 
    def set_product_available(self):
        self.product_available = self.aisle.get_status
    
    def update_info(self, name, brand_name, description, image_url, cost_price, mark_up_category, store, department, aisle, taxable, product_available, position_on_aisle, size, modified_price):
        self.name = name
        self.brand_name = brand_name
        self.description = description
        self.image_url = image_url
        self.cost_price = cost_price
        self.mark_up_category = mark_up_category
        #self.store = Store.objects.filter(pk = store)        
        #self.department = Department.objects.filter(pk = department)
        #self.aisle = Aisle.objects.filter(pk = aisle)
        self.taxable = taxable
        self.product_available = product_available
        self.position_on_aisle = position_on_aisle
        self.size = size
        self.modified_price = modified_price
        
    def set_modified_price(self, modified_price):
        self.modified_price = modified_price
        
    def approve_price_change(self):
        if self.mark_up_category == 1:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE1)
        elif self.mark_up_category == 2:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE2)
        elif self.mark_up_category == 3:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE3)
        elif self.mark_up_category == 4:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE4)
        elif self.mark_up_category == 5:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE5)
        elif self.mark_up_category == 6:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE6)
        elif self.mark_up_category == 7:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE7)
        elif self.mark_up_category == 8:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE8)
        elif self.mark_up_category == 9:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE9)
        elif self.mark_up_category == 10:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE10)
        elif self.mark_up_category == 11:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE11)
        elif self.mark_up_category == 12:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE12)
        elif self.mark_up_category == 13:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE13)
        elif self.mark_up_category == 14:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE14)
        elif self.mark_up_category == 15:
            self.price = Decimal(self.modified_price) * Decimal(openshift.settings.MARK_UP_RATE15)
        else:
            raise ValueError('Product approve_price_change failed, invalid mark up category')
        self.cost_price = self.modified_price
        self.modified_price = 0
        self.price_change = False
        # TO check if it is saved in api
        