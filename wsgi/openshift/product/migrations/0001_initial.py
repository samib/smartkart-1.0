# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Product'
        db.create_table(u'product_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('brand_name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('image_url', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('cost_price', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2)),
            ('mark_up_category', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(related_name='store', to=orm['store.Store'])),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Department'])),
            ('aisle', self.gf('django.db.models.fields.related.ForeignKey')(related_name='products', to=orm['store.Aisle'])),
            ('taxable', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('product_available', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('position_on_aisle', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('size', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('price_change', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('modified_price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2)),
            ('store_product_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('package_unit_type', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('package_unit_volume_in_milliliters', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('total_package_units', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('alcohol_content', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2)),
            ('price_per_liter_of_alcohol_in_cents', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('price_per_liter_in_cents', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('sugar_content', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('has_value_added_promotion', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_limited_time_offer', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_seasonal', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_vqa', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_kosher', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('tasting_note', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('large_image_url', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('sugar_in_grams_per_liter', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('clearance_sale_savings_in_cents', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('has_clearance_sale', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'product', ['Product'])


    def backwards(self, orm):
        # Deleting model 'Product'
        db.delete_table(u'product_product')


    models = {
        u'product.product': {
            'Meta': {'object_name': 'Product'},
            'aisle': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'to': u"orm['store.Aisle']"}),
            'alcohol_content': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2'}),
            'brand_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'clearance_sale_savings_in_cents': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cost_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Department']"}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'has_clearance_sale': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_limited_time_offer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_value_added_promotion': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'is_kosher': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_seasonal': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_vqa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'large_image_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'mark_up_category': ('django.db.models.fields.SmallIntegerField', [], {}),
            'modified_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'package_unit_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'package_unit_volume_in_milliliters': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'position_on_aisle': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2'}),
            'price_change': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price_per_liter_in_cents': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'price_per_liter_of_alcohol_in_cents': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'product_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'size': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'store'", 'to': u"orm['store.Store']"}),
            'store_product_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sugar_content': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'sugar_in_grams_per_liter': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tasting_note': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'taxable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'total_package_units': ('django.db.models.fields.SmallIntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'store.aisle': {
            'Meta': {'object_name': 'Aisle'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'aisle_icon_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'aisle_name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'aisles'", 'to': u"orm['store.Department']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'store.department': {
            'Meta': {'object_name': 'Department'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'department_name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'departments'", 'to': u"orm['store.Store']"})
        },
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'delivery_end_time': ('django.db.models.fields.TimeField', [], {}),
            'delivery_start_time': ('django.db.models.fields.TimeField', [], {}),
            'has_alcohol': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_free_delivery_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'min_order_for_free_delivery': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'minimum_order': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'one_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'phone': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'store_logo_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'three_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'three_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'time_zone': ('django.db.models.fields.CharField', [], {'default': "'America/Toronto'", 'max_length': '50'}),
            'two_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'two_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        }
    }

    complete_apps = ['product']