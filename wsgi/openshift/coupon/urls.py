'''
Created on June 26, 2014

@author: Hisham
'''
from django.conf.urls import patterns, url, include
from coupon.api import CouponResource

coupon_resource = CouponResource()

urlpatterns = patterns('',
    url(r'^api/', include(coupon_resource.urls)),
);