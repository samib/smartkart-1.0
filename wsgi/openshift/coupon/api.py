import json

from datetime import datetime
from decimal import Decimal

from django import forms
from django.conf.urls import url
from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder

import openshift.settings
from tastypie import fields
from tastypie.validation import FormValidation
from tastypie.authorization import Authorization
from tastypie.authentication import SessionAuthentication
from tastypie.exceptions import Unauthorized
from tastypie.resources import ModelResource
from tastypie.throttle import CacheThrottle
from tastypie.utils import trailing_slash

from cart.models import Cart, CartItem, DeliveryCharge
from coupon.models import Coupon

class CouponForm(forms.ModelForm):

    class Meta:
        model = Coupon

class CouponAuthorization(Authorization):
   
    def read_list(self, object_list, bundle):
        if bundle.request.user.is_admin:
            return object_list.all()
        else:
            raise Unauthorized("Only admin user can access this.")
        
    def read_detail(self, object_list, bundle):
        if bundle.request.user.is_admin:
            return object_list.all()
        else:
            raise Unauthorized("Only admin user can access this.")
            
class CouponResource(ModelResource):
    redeemed_by = fields.ForeignKey('order.api.UserNameResource', 'redeemed_by', full=True, null=True)
    created_by = fields.ForeignKey('order.api.UserNameResource', 'created_by', full=True)

    class Meta:
        queryset = Coupon.objects.all()
        resource_name = 'coupon'        
        allowed_methods = ['get', 'post']
        limit = 0
        validation = FormValidation(form_class=CouponForm)
        authentication = SessionAuthentication()
        authorization = CouponAuthorization()
        include_resource_uri = False
        #TODO:// Add Auth
        
    def prepend_urls(self):
        return [
            url(r'^(?P<resource_name>%s)/admin/create%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_coupon'), name='api_create_coupon'),
            url(r'^(?P<resource_name>%s)/redeem%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('redeem_coupon'), name='api_redeem_coupon'),
            url(r'^(?P<resource_name>%s)/remove%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('remove_coupon'), name='api_remove_coupon'),
        ]
            
    def create_coupon(self, request, **kwargs):    
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin has access to create coupons'
            })
        else:
            self.method_check(request, allowed=['post'])
            data = json.loads(request.body)
            quantity = data.get('quantity', '')
            value = data.get('value', '')
            type_of_coupon = data.get('type', '')
            expiry_date = datetime.strptime(data.get('expiry_date', ''), '%Y-%m-%d')
            if not expiry_date:
                return self.create_response(request, {
                    'success': False,
                    'message': 'Coupon must have an expiry date'
                })
            if(type_of_coupon == 'percentage' and (value > 100)):
                return self.create_response(request, {
                    'success': False,
                    'message': 'Percentage coupons cannot have a value greater than 100%'
                })
            coupons = Coupon.objects.create_coupons(quantity=quantity, type_of_coupon=type_of_coupon, value=value, user=request.user, expiry_date=expiry_date)
            _coupons = []
            for c in coupons:
                _coupons.append({
                            "product": {
                                        "value": c.value,
                                        "type": c.type,
                                        "code": c.code,
                                        "created_by" : c.created_by.first_name
                                    }                         
                        })
        data = { 'success':True, 'message':'Successfully created coupons', 'coupons':_coupons}
        data_string = json.dumps(data, cls=DjangoJSONEncoder)
        return HttpResponse(data_string, mimetype='application/json')
        
    def redeem_coupon(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        code = data.get('code', '')
        cart_id = data.get('cart_id', '')
        num_of_stores = data.get('num_of_stores', '')
        re_apply = data.get('re_apply', '')

        try:
            coupon = Coupon.objects.get(code=code)
        except Coupon.DoesNotExist:
            return self.create_response(request, {
             'success': False,
             'message': 'Invalid Coupon'
            })

        try:
            cart = Cart.objects.get(pk=cart_id)
        except ObjectDoesNotExist:
            return self.create_response(request, {
             'success': False,
             'message': 'Invalid Cart'
            })

        if(cart.coupon and cart.coupon.id != coupon.id):
            return self.create_response(request, {
             'success': False,
             'message': 'You cannot apply multiple coupons'
            })

        current_datetime = datetime.now()
        current_date = current_datetime.date()
        
        if(current_date > coupon.expiry_date): #TODO: Review this
            if(re_apply):                
                cart.coupon = None
                cart.dollar_amount = None
                cart.discount_per_store = None
                cart.save()
            return self.create_response(request, {
             'success': False,
             'message': 'Sorry the coupon has expired'
            })

        if(re_apply and (coupon.type == 'free_delivery' or coupon.type == 'multiple_percentage' or coupon.type == 'multiple_monetary')):
            cart.coupon = None
            cart.dollar_amount = None
            cart.discount_per_store = None
            cart.save()
            return self.create_response(request, {
             'success': True,
             'message': 'Please re-apply the coupon at the checkout'
            })

        if not coupon.is_checked_out:
            print 'not redeemed'
            if(coupon.type == 'free_delivery'):
                print request.user.is_first_order
                if(request.user.is_first_order):
                    delivery_charges_for_cart = DeliveryCharge.objects.filter(cart__id=cart_id).prefetch_related('store')
                    if delivery_charges_for_cart.exists():           
                        delivery_charges_amount = Decimal(0.00)
                        for charge in delivery_charges_for_cart:
                            if(charge.is_one_hour):
                                delivery_charges_amount += Decimal(charge.store.one_hour_delivery_fee)
                            elif(charge.is_two_hour):
                                delivery_charges_amount += Decimal(charge.store.two_hour_delivery_fee)
                            elif(charge.is_three_hour):
                                delivery_charges_amount += Decimal(charge.store.three_hour_delivery_fee)
                        cart.delivery_charge = delivery_charges_amount
                        cart.coupon = coupon
                        cart.save()
                        return self.create_response(request, {
                             'success': True,
                             'message': 'Successfully redeemed free delivery coupon',
                             'discount': delivery_charges_amount,
                             'discount_tax_deduction': round(delivery_charges_amount * Decimal(openshift.settings.TAX_RATE),2)
                        })
                else:
                    return self.create_response(request, {
                             'success': False,
                             'message': 'Free delivery coupon can only be applied for your first order'
                        })
            else:
                items = CartItem.objects.filter(cart=cart, cart__user__exact=request.user)
                result = coupon.redeem(request.user, cart, items, num_of_stores)
                if(not result):
                    print 'total amount less than the discount'
                    return self.create_response(request, {
                     'success': False,
                     'message': 'This coupon cannot be applied for this purchase'
                    }) 
                return self.create_response(request, {
                     'success': True,
                     'message': 'Successfully redeemed coupon',
                     'discount': cart.dollar_amount,
                     'random_hash':coupon.random_hash,
                     'discount_tax_deduction': 0.00
                })
        else:
            print 'redeemed'
            return self.create_response(request, {
             'success': False,
             'message': 'Coupon already used'
            })

    def remove_coupon(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        cart_id = data.get('cart_id', '')

        try:
            cart = Cart.objects.get(user__exact=request.user, pk=cart_id)
        except ObjectDoesNotExist:
            return self.create_response(request, {
             'success': False,
             'message': 'Invalid Cart'
            }) 

        try:
            coupon = Coupon.objects.get(code=cart.coupon__code)
        except Coupon.DoesNotExist:
            return self.create_response(request, {
             'success': False,
             'message': 'Invalid Coupon'
            })

        if(coupon.type == 'percentage' or coupon.type == 'monetary'):
            coupon.redeemed_at = None
            coupon.redeemed_by = None        
            coupon.random_hash = None
            coupon.is_checked_out = False
            coupon.save()

        cart.coupon = None
        cart.dollar_amount = None
        cart.discount_per_store = None
        cart.save()
        return self.create_response(request, {
             'success': True,
             'message': 'Successfully removed the coupon'
            })




