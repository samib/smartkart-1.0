# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Coupon.dollar_amount'
        db.delete_column(u'coupon_coupon', 'dollar_amount')

        # Deleting field 'Coupon.discount_per_store'
        db.delete_column(u'coupon_coupon', 'discount_per_store')


    def backwards(self, orm):
        # Adding field 'Coupon.dollar_amount'
        db.add_column(u'coupon_coupon', 'dollar_amount',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True),
                      keep_default=False)

        # Adding field 'Coupon.discount_per_store'
        db.add_column(u'coupon_coupon', 'discount_per_store',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True),
                      keep_default=False)


    models = {
        u'coupon.coupon': {
            'Meta': {'object_name': 'Coupon'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'created_by'", 'null': 'True', 'to': u"orm['usermanagement.User']"}),
            'expiry_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_checked_out': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'random_hash': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'redeemed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'redeemed_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'redeemed_by'", 'null': 'True', 'to': u"orm['usermanagement.User']"}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        },
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'delivery_end_time': ('django.db.models.fields.TimeField', [], {}),
            'delivery_start_time': ('django.db.models.fields.TimeField', [], {}),
            'has_alcohol': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_free_delivery_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'min_order_for_free_delivery': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'minimum_order': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'one_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'phone': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'store_logo_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'three_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'three_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'time_zone': ('django.db.models.fields.CharField', [], {'default': "'America/Toronto'", 'max_length': '50'}),
            'two_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'two_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        },
        u'usermanagement.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'email_subscription': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'expiry': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2016, 3, 6, 0, 0)'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'forgot_password_hash': ('django.db.models.fields.CharField', [], {'default': "'tgeeZwsBhc4vFbstEwa8umuHQPXH46vmgsQ8grHR'", 'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_driver': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_first_order': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'lastOrderID': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'last_store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']", 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        }
    }

    complete_apps = ['coupon']