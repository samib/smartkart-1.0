from datetime import datetime
import random
from decimal import Decimal

from django.db import models
from django.db import IntegrityError
from django.utils.timezone import get_default_timezone
from django.utils.translation import ugettext_lazy as _
from django.db.models import Sum

from openshift.settings import COUPON_TYPES, CODE_LENGTH, CODE_CHARS

from usermanagement.models import User
from usermanagement.utils import make_random_string

class CouponManager(models.Manager):
    def create_coupon(self, type_of_coupon, value, user, expiry_date):
        if(type_of_coupon == 'percentage' and (value > 100)):
            raise ValueError('A percentage coupon cannot have value more than 100')
            
        coupon = self.create(
                value=value,
                code=Coupon.generate_code(),
                type=type_of_coupon,
                created_by=user,
                expiry_date=expiry_date
            )
        try:
            coupon.save()
        except IntegrityError:
            # Try again with other code
            return Coupon.objects.create_coupon(type_of_coupon, value, user)
        else:
            return coupon

    def create_coupons(self, quantity, type_of_coupon, value, user, expiry_date):
        coupons = []
        for i in xrange(quantity):
            coupons.append(self.create_coupon(type_of_coupon, value, user, expiry_date))
        return coupons

class Coupon(models.Model):
    value = models.DecimalField(_("Value"), max_digits=6, decimal_places=2)
    code = models.CharField(_("Code"), max_length=30, unique=True, blank=True)
    type = models.CharField(_("Type"), max_length=20, choices=COUPON_TYPES)
    created_by = models.ForeignKey(User, verbose_name=_("Created by"), related_name='created_by', null=True, blank=True)
    redeemed_by = models.ForeignKey(User, verbose_name=_("Redeemed by"), related_name='redeemed_by', null=True, blank=True)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    redeemed_at = models.DateTimeField(_("Redeemed at"), blank=True, null=True)
    is_checked_out = models.BooleanField(default = False)
    #dollar_amount = models.DecimalField(_("Dollar Amount"), max_digits=6, decimal_places=2, blank=True, null=True)
    #discount_per_store = models.DecimalField(_("Discount Per Store"), max_digits=6, decimal_places=2, blank=True, null=True)
    random_hash = models.CharField(max_length=10, blank=True, null=True)
    expiry_date = models.DateField()
    objects = CouponManager()
# 
#     class Meta:
#         ordering = ['created_at']
#         verbose_name = _("Coupon")
#         verbose_name_plural = _("Coupons")

#     def __unicode__(self):
#         return self.code

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = Coupon.generate_code()
        super(Coupon, self).save(*args, **kwargs)

    @classmethod
    def generate_code(cls):
        return "".join(random.choice(CODE_CHARS) for i in xrange(CODE_LENGTH))

    def redeem(self, user, cart, items, num_of_stores):

        if len(items) > 0:
            net_price = items.aggregate(Sum('price'))['price__sum'] #CartItem.objects.filter(cart__id=cart_id, cart__user__exact=user).aggregate(Sum('price'))['price__sum']
            tax = items.aggregate(Sum('tax'))['tax__sum']
        else:
            net_price = 0
            tax = 0

        #self.dollar_amount = 0
        total = net_price + tax

        if self.type == 'monetary' or self.type == 'multiple_monetary':
            cart.dollar_amount = self.value
        elif self.type == 'percentage' or self.type == 'multiple_percentage':
            if(self.value>100):
                return False #In case if somebody created a coupon code with value 200 #TODO: raise error
            cart.dollar_amount = (total) * Decimal(float(self.value)/100)
        else:
            raise ValueError('Invalid coupon type') #TODO: Better error management

        if cart.dollar_amount > total:
            return False 

        if self.type == 'percentage' or self.type == 'monetary':
            self.redeemed_at = datetime.now(get_default_timezone())
            self.redeemed_by = user
            #self.cart_id = cart_id
            self.random_hash = make_random_string(10)                
            #Cart.objects.filter(pk=cart_id).update(coupon=self)
            self.save()
                
        cart.coupon = self
        cart.discount_per_store = cart.dollar_amount / int(num_of_stores)        
        cart.save()
        return True
