'''
Created on Oct 27, 2013

@author: Hisham
'''
from django.conf.urls import patterns, url, include
from store.api import StoreResource, DepartmentResource, AisleResource, CustomerStoreResource, SearchProductResource, CustomerProductResource, DevliveryTimeManagerResource, DepartmentDealResource

store_resource = StoreResource()
department_resource = DepartmentResource()
aisle_resource = AisleResource()
customer_store_resource = CustomerStoreResource()
search_product_resource = SearchProductResource()
customer_product_resource = CustomerProductResource()
block_devlivery_time_resource = DevliveryTimeManagerResource()
department_deal_resource = DepartmentDealResource()
#devlivery_time_resource = DevliveryTimeResource()

urlpatterns = patterns('',
    #url(r'^add_store/$', 'store.views.addStore', None, 'addStore'),
    url(r'^api/', include(store_resource.urls)),
    url(r'^api/', include(department_resource.urls)),
    url(r'^api/', include(aisle_resource.urls)),
    url(r'^api/', include(customer_store_resource.urls)),
    url(r'^api/', include(search_product_resource.urls)),
    url(r'^api/', include(customer_product_resource.urls)),
    url(r'^api/', include(block_devlivery_time_resource.urls)),
    url(r'^api/', include(department_deal_resource.urls)),
    #url(r'^api/', include(devlivery_time_resource.urls)),
);