from django.db import models

from product.models import Product

# Create your models here.
class StoreManager(models.Manager):
    
    def add_store(self, name, street, city, postal_code, phone, minimum_order, min_order_for_free_delivery, store_logo_url,
                  one_hour_delivery_fee, two_hour_delivery_fee, three_hour_delivery_fee, delivery_start_time, delivery_end_time):
        
        if not name:
            raise ValueError('Store must have a name')
        
        if not street:
            raise ValueError('Store must have a street')
        
        if not minimum_order:
            raise ValueError('Store must have a minimum_order')
        
        if not store_logo_url:
            raise ValueError('Store must have a store_logo_url')
        
        if not min_order_for_free_delivery:
            raise ValueError('Store must have a min_order_for_free_delivery')
        
        if not one_hour_delivery_fee:
            raise ValueError('Store must have a one_hour_delivery_fee')
        
        if not two_hour_delivery_fee:
            raise ValueError('Store must have a two_hour_delivery_fee')
        
        if not three_hour_delivery_fee:
            raise ValueError('Store must have a three_hour_delivery_fee')
        
        if not delivery_start_time:
            raise ValueError('Store must have a delivery_start_time')
        
        if not delivery_end_time:
            raise ValueError('Store must have a delivery_end_time')
        
        store = self.create(name=name, street=street, city=city, postal_code=postal_code, phone=phone, minimum_order=minimum_order,
                            min_order_for_free_delivery=min_order_for_free_delivery, store_logo_url=store_logo_url, one_hour_delivery_fee = one_hour_delivery_fee,
                            two_hour_delivery_fee = two_hour_delivery_fee, three_hour_delivery_fee= three_hour_delivery_fee, delivery_start_time = delivery_start_time,
                            delivery_end_time=delivery_end_time)
        return store
    
    def update_store(self, store_id, name, street, city, postal_code, phone, delivery_fee, minimum_order, min_order_for_free_delivery, store_logo_url,
                     one_hour_delivery_fee, two_hour_delivery_fee, three_hour_delivery_fee, one_hour_delivery_available, two_hour_delivery_available,
                     three_hour_delivery_available, delivery_start_time, delivery_end_time, has_alcohol, time_zone, is_free_delivery_active):
        
        if not name:
            raise ValueError('Store must have a name')
        
        if not street:
            raise ValueError('Store must have a street')
        
        if not delivery_fee:
            raise ValueError('Store must have a delivery_fee')
        
        if not minimum_order:
            raise ValueError('Store must have a minimum_order')
        
        if not store_logo_url:
            raise ValueError('Store must have a store_logo_url')
        
        if not min_order_for_free_delivery:
            raise ValueError('Store must have a min_order_for_free_delivery')
        
        if not one_hour_delivery_fee:
            raise ValueError('Store must have a one_hour_delivery_fee')
        
        if not two_hour_delivery_fee:
            raise ValueError('Store must have a two_hour_delivery_fee')
        
        if not three_hour_delivery_fee:
            raise ValueError('Store must have a three_hour_delivery_fee')
        
        if not one_hour_delivery_available:
            raise ValueError('Store must have a one_hour_delivery_available')
    
        if not two_hour_delivery_available:
            raise ValueError('Store must have a two_hour_delivery_available')
        
        if not three_hour_delivery_available:
            raise ValueError('Store must have a three_hour_delivery_available')
        
        if not delivery_start_time:
            raise ValueError('Store must have a delivery_start_time')
        
        if not delivery_end_time:
            raise ValueError('Store must have a delivery_end_time')
        
        if not has_alcohol:
            raise ValueError('Store must have a has_alcohol')
        
        if not time_zone:
            raise ValueError('Store must have a time_zone')
        
        if not is_free_delivery_active:
            raise ValueError('Store must have a is_free_delivery_active')
        
        store = Store.objects.filter(pk=store_id).update(name=name, street=street, city=city, postal_code=postal_code, phone=phone,
                                                        minimum_order=minimum_order, min_order_for_free_delivery = min_order_for_free_delivery,  
                                                        store_logo_url=store_logo_url, one_hour_delivery_fee = one_hour_delivery_fee,
                                                        two_hour_delivery_fee = two_hour_delivery_fee, three_hour_delivery_fee = three_hour_delivery_fee,
                                                        one_hour_delivery_available = one_hour_delivery_available, two_hour_delivery_available = two_hour_delivery_available,
                                                        three_hour_delivery_available = three_hour_delivery_available, delivery_start_time = delivery_start_time,
                                                        delivery_end_time = delivery_end_time, has_alcohol = has_alcohol, time_zone = time_zone,
                                                        is_free_delivery_active = is_free_delivery_active)
        return store
    
    def change_active_status(self, store_id, active):
        if not store_id:
            raise ValueError('change_active_status store must have a store_id')
        
        if not active:
            raise ValueError('change_active_status store must have a active status')
        
        store = Store.objects.filter(pk=store_id).update(active=active)
        if not active:
            Product.objects.filter(store=store).update(product_available=False)
            departments = Department.objects.filter(store=store)            
            for department in departments:
                department.update(active=False)
                Aisle.objects.filter(department=department).update(active=False)
        return store
    
class Store(models.Model):
    objects = StoreManager()
    name = models.CharField(max_length=80)
    street = models.CharField(max_length=100)
    city = models.CharField(max_length=50)
    postal_code = models.CharField(max_length=6)
    phone = models.BigIntegerField(max_length=10)
    active = models.BooleanField(default=False)
    #delivery_fee = models.DecimalField(max_digits=6, decimal_places=2)
    minimum_order = models.DecimalField(max_digits=6, decimal_places=2)
    min_order_for_free_delivery = models.DecimalField(max_digits=6, decimal_places=2)
    store_logo_url = models.CharField(max_length=200)
    one_hour_delivery_fee = models.DecimalField(max_digits=6, decimal_places=2)
    two_hour_delivery_fee = models.DecimalField(max_digits=6, decimal_places=2)
    three_hour_delivery_fee = models.DecimalField(max_digits=6, decimal_places=2)
    one_hour_delivery_available = models.BooleanField(default=False)
    two_hour_delivery_available = models.BooleanField(default=False)
    three_hour_delivery_available = models.BooleanField(default=True)    
    delivery_start_time = models.TimeField()
    delivery_end_time = models.TimeField()
    has_alcohol = models.BooleanField(default=False)
    time_zone = models.CharField(max_length=50, default="America/Toronto")
    is_free_delivery_active = models.BooleanField(default=False)
    
class DepartmentManager(models.Manager):
    def add_department(self, store_id, department_name):
        if not store_id:
            raise ValueError('Department must have a store_id')
        
        if not department_name:
            raise ValueError('Department must have a name')
        
        department = self.create(store_id=store_id, department_name=department_name)
        #department = self.create(store=Store.objects.get(pk=store_id), department_name=department_name)
        return department
    
    def update_department(self, department_id, department_name, store_id):
        if not department_name:
            raise ValueError('update_department must have a department_name')
        
        if not department_id:
            raise ValueError('update_department must have a department_id')
        
        if not store_id:
            raise ValueError('update_department must have a store_id')
                
        department = Department.objects.filter(pk=department_id).update(Store.objects.get(pk=store_id), department_name=department_name)
        return department
    
    def change_active_status(self, department_id, active):
        if not department_id:
            raise ValueError('change_active_status department must have a department_id')
        
        if not active:
            raise ValueError('change_active_status department must have a active status')
        
        department = Department.objects.get(pk=department_id).update(active=active)
        if not active:
            Aisle.objects.filter(department=department).update(active=False)
            Product.objects.filter(department=department).update(product_available=False)            
        return department
       
class Department(models.Model):
    objects = DepartmentManager()
    store =  models.ForeignKey(Store, related_name="departments")
    department_name = models.CharField(max_length=80)
    active = models.BooleanField(default=False)

class AisleManager(models.Manager):    
    def add_aisle(self, department_id, aisle_name, store_aisle_number):
        if not department_id:
            raise ValueError('Aisle must have a department_id')
        
        if not aisle_name:
            raise ValueError('Aisle must have a name')
        
        if not store_aisle_number:
            raise ValueError('Aisle must have a store aisle number')
        
        #aisle = self.create(department=Department.objects.get(pk=department_id), aisle_name=aisle_name, store_aisle_number= store_aisle_number)
        aisle = self.create(department_id=department_id, aisle_name=aisle_name, store_aisle_number= store_aisle_number)
        return aisle
    
    def update_aisle(self, aisle_id, department_id, aisle_name, store_aisle_number, active):
        if not aisle_id:
            raise ValueError('Update aisle must have a aisle_id')
        
        if not department_id:
            raise ValueError('Update aisle must have a department_id')
        
        if not aisle_name:
            raise ValueError('Update aisle must have a name')
        
        if not store_aisle_number:
            raise ValueError('Update aisle must have a store aisle number')
        
        aisle = Aisle.objects.filter(pk=aisle_id).update(department=Department.objects.get(pk=department_id), aisle_name=aisle_name, 
                                                         store_aisle_number=store_aisle_number)
        return aisle
    
    def change_active_status(self, aisle_id, active):
        if not aisle_id:
            raise ValueError('change_active_status aisle must have a aisle_id')
        
        if not active:
            raise ValueError('change_active_status aisle must have a active status')
        
        aisle = Aisle.objects.filter(pk=aisle_id).update(active=active)
        if not active:
            Product.objects.filter(aisle=aisle).update(product_available=False)
        else:
            Product.objects.filter(aisle=aisle).update(product_available=True)
        return aisle
    
class Aisle(models.Model):
    objects = AisleManager()
    department = models.ForeignKey(Department, related_name='aisles')
    aisle_name = models.CharField(max_length=80)
    #store_aisle_number = models.CharField(max_length=80)
    active = models.BooleanField(default=False)
    aisle_icon_url = models.CharField(max_length=200)
    
    def get_status(self):
        return self.active

# class DeliveryTime(models.Model):
#     display_name = models.CharField(max_length=15)
#     
#     def as_dict(self):
#         return {
#             "id": self.id,
#             "available_time": self.display_name
#         }
            
class BlockedDeliveryTime(models.Model):
    delivery_date = models.DateField()
    #display_name = models.ForeignKey(DeliveryTime)
    from_time = models.TimeField() 
    to_time = models.TimeField()
    store = models.ForeignKey(Store)
    