# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Store'
        db.create_table(u'store_store', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('postal_code', self.gf('django.db.models.fields.CharField')(max_length=6)),
            ('phone', self.gf('django.db.models.fields.BigIntegerField')(max_length=10)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('minimum_order', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('min_order_for_free_delivery', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('store_logo_url', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('one_hour_delivery_fee', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('two_hour_delivery_fee', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('three_hour_delivery_fee', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('one_hour_delivery_available', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('two_hour_delivery_available', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('three_hour_delivery_available', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('delivery_start_time', self.gf('django.db.models.fields.TimeField')()),
            ('delivery_end_time', self.gf('django.db.models.fields.TimeField')()),
            ('has_alcohol', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('time_zone', self.gf('django.db.models.fields.CharField')(default='America/Toronto', max_length=50)),
            ('is_free_delivery_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'store', ['Store'])

        # Adding model 'Department'
        db.create_table(u'store_department', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(related_name='departments', to=orm['store.Store'])),
            ('department_name', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'store', ['Department'])

        # Adding model 'Aisle'
        db.create_table(u'store_aisle', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(related_name='aisles', to=orm['store.Department'])),
            ('aisle_name', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('aisle_icon_url', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'store', ['Aisle'])

        # Adding model 'BlockedDeliveryTime'
        db.create_table(u'store_blockeddeliverytime', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('delivery_date', self.gf('django.db.models.fields.DateField')()),
            ('from_time', self.gf('django.db.models.fields.TimeField')()),
            ('to_time', self.gf('django.db.models.fields.TimeField')()),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Store'])),
        ))
        db.send_create_signal(u'store', ['BlockedDeliveryTime'])


    def backwards(self, orm):
        # Deleting model 'Store'
        db.delete_table(u'store_store')

        # Deleting model 'Department'
        db.delete_table(u'store_department')

        # Deleting model 'Aisle'
        db.delete_table(u'store_aisle')

        # Deleting model 'BlockedDeliveryTime'
        db.delete_table(u'store_blockeddeliverytime')


    models = {
        u'store.aisle': {
            'Meta': {'object_name': 'Aisle'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'aisle_icon_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'aisle_name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'aisles'", 'to': u"orm['store.Department']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'store.blockeddeliverytime': {
            'Meta': {'object_name': 'BlockedDeliveryTime'},
            'delivery_date': ('django.db.models.fields.DateField', [], {}),
            'from_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']"}),
            'to_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'store.department': {
            'Meta': {'object_name': 'Department'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'department_name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'departments'", 'to': u"orm['store.Store']"})
        },
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'delivery_end_time': ('django.db.models.fields.TimeField', [], {}),
            'delivery_start_time': ('django.db.models.fields.TimeField', [], {}),
            'has_alcohol': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_free_delivery_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'min_order_for_free_delivery': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'minimum_order': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'one_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'phone': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'store_logo_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'three_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'three_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'time_zone': ('django.db.models.fields.CharField', [], {'default': "'America/Toronto'", 'max_length': '50'}),
            'two_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'two_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        }
    }

    complete_apps = ['store']