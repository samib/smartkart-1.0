'''
Created on Nov 09, 2013

@author: Hisham
'''
import json
from datetime import datetime

from django.core.exceptions import ObjectDoesNotExist
from django.conf.urls import url
from django.db.models import Q

from tastypie import fields
from tastypie.authentication import SessionAuthentication
from tastypie.resources import ModelResource, ALL
from tastypie.authorization import Authorization
from tastypie.exceptions import Unauthorized, BadRequest
from tastypie.utils import trailing_slash
from tastypie.throttle import CacheThrottle
from tastypie.resources import ALL
from tastypie.cache import SimpleCache

from store.models import Store, Department, Aisle, BlockedDeliveryTime
from product.models import Product

"""
    The following are resources called ONLY by Admin
"""
class AdminAuthorization(Authorization):
    def read_list(self, object_list, bundle):
        print 'Read List AdminAuthorization'        
        if bundle.request.user.is_admin:
            return object_list.all()
        else:
            raise Unauthorized("Only admin user can access this.")
    
    def read_detail(self, object_list, bundle):
        print 'Read Detail'
        if bundle.request.user.is_admin:
            return object_list.all()
        else:
            raise Unauthorized("Only admin user can access this.")
               
class StoreResource(ModelResource):
    departments = fields.ToManyField('store.api.DepartmentResource', 'departments', full=True) 
    class Meta:
        queryset = Store.objects.all()
        resource_name = 'store'
        fields = ['id', 'name','store_logo_url']
        allowed_methods = ['post', 'get']
        excludes = ['street', 'city', 'postal_code', 'phone']
        include_resource_uri = True
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        authorization = AdminAuthorization()
         
    def prepend_urls(self): #TODO: Add admin/
        return [
            url(r"^admin/(?P<resource_name>%s)/add%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('add_store'), name="api_add_store"),
            url(r"^admin/(?P<resource_name>%s)/update%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update_store'), name="api_update_store"),
            url(r"^admin/(?P<resource_name>%s)/status%s$" % 
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('change_status'), name="api_change_status"),#TODO: Remove
        ]
            
    def add_store(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin has access to add store'
            })
        else:
            data = json.loads(request.body)    
            name = data.get('name', '')
            street = data.get('street', '')
            city = data.get('city', '')
            postal_code = data.get('postal_code', '')
            phone = data.get('phone', '')
            minimum_order = data.get('minimum_order', '')
            min_order_for_free_delivery = data.get('min_order_for_free_delivery', '')
            store_logo_url = data.get('store_logo_url', '')            
            one_hour_delivery_fee = data.get('one_hour_delivery_fee', '') 
            two_hour_delivery_fee = data.get('two_hour_delivery_fee', '') 
            three_hour_delivery_fee = data.get('three_hour_delivery_fee', '')
            delivery_start_time = data.get('delivery_start_time', '') 
            delivery_end_time = data.get('delivery_start_time', '')

            store = Store.objects.add_store(name=name, street=street, city=city, postal_code=postal_code, phone=phone, 
                                    minimum_order=minimum_order, min_order_for_free_delivery=min_order_for_free_delivery, store_logo_url=store_logo_url,
                                    one_hour_delivery_fee = one_hour_delivery_fee, two_hour_delivery_fee = two_hour_delivery_fee, three_hour_delivery_fee= three_hour_delivery_fee,
                                    delivery_start_time = delivery_start_time, delivery_end_time=delivery_end_time)
            if store:        
                return self.create_response(request, {
                    'success': True
                })
            else:
                return self.create_response(request, {
                    'success': False,
                    'message': 'Failed adding store'
                })
        
    def update_store(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin has access to update store'
            })
        else:
            data = json.loads(request.body)
            store_id = data.get('store_id', '')  
            name = data.get('name', '')
            street = data.get('street', '')
            city = data.get('city', '')
            postal_code = data.get('postal_code', '')
            phone = data.get('phone', '')
            minimum_order = data.get('minimum_order', '')
            min_order_for_free_delivery = data.get('min_order_for_free_delivery', '')
            store_logo_url = data.get('store_logo_url', '')            
            one_hour_delivery_fee = data.get('one_hour_delivery_fee', '') 
            two_hour_delivery_fee = data.get('two_hour_delivery_fee', '') 
            three_hour_delivery_fee = data.get('three_hour_delivery_fee', '')
            delivery_start_time = data.get('delivery_start_time', '') 
            delivery_end_time = data.get('delivery_start_time', '')
            one_hour_delivery_available = data.get('one_hour_delivery_available', '')
            two_hour_delivery_available = data.get('two_hour_delivery_available', '')
            three_hour_delivery_available = data.get('three_hour_delivery_available', '')    
            has_alcohol = data.get('has_alcohol', '')
            time_zone = data.get('time_zone', '')
            is_free_delivery_active = data.get('is_free_delivery_active', '')

            store = Store.objects.update_store(store_id = store_id, name=name, street=street, city=city, postal_code=postal_code, phone=phone,
                                              minimum_order=minimum_order, min_order_for_free_delivery = min_order_for_free_delivery,  
                                              store_logo_url=store_logo_url, one_hour_delivery_fee = one_hour_delivery_fee,
                                              two_hour_delivery_fee = two_hour_delivery_fee, three_hour_delivery_fee = three_hour_delivery_fee,
                                              one_hour_delivery_available = one_hour_delivery_available, two_hour_delivery_available = two_hour_delivery_available,
                                              three_hour_delivery_available = three_hour_delivery_available, delivery_start_time = delivery_start_time,
                                              delivery_end_time = delivery_end_time, has_alcohol = has_alcohol, time_zone = time_zone,
                                              is_free_delivery_active = is_free_delivery_active)    
            if store:        
                return self.create_response(request, {
                    'success': True
                })
            else:
                return self.create_response(request, {
                    'success': False,
                    'message': 'Failed updaing store'
                })
    
    def change_status(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin has access to change status of store'
            })
        else:
            data = json.loads(request.body)
            store_id = data.get('store_id', '')
            active = data.get('active', '') 
            store = Store.objects.change_active_status(store_id=store_id, active=active)
            if store:        
                return self.create_response(request, {
                    'success': True
                })
            else:
                return self.create_response(request, {
                    'success': False,
                    'message': 'Failed changing the status of the store'
                })
                
class DepartmentResource(ModelResource):
    aisles = fields.ToManyField('store.api.AisleResource', 'aisles', full=True) 
    class Meta:
        queryset = Department.objects.all()
        resource_name = 'department'
        fields = ['id', 'department_name', 'store']
        allowed_methods = ['post','get']
        include_resource_uri = True
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        authorization = AdminAuthorization()
        
    def prepend_urls(self):
        return [
            url(r"^admin/(?P<resource_name>%s)/add%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('add_department'), name="api_add_department"),
            url(r"admin/^(?P<resource_name>%s)/update%s$" % 
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('update_department'), name="api_update_department"),
            url(r"admin/^(?P<resource_name>%s)/status%s$" % 
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('change_status'), name="api_change_status"),
        ]
            
    def add_department(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin has access to add department'
            })
        else:
            data = json.loads(request.body)
            department_name = data.get('department_name', '')
            store_id = data.get('store_id', '')
            Department.objects.add_department(store_id=store_id, department_name=department_name)
            return self.create_response(request, {
                'success': True
            })
    
    def update_department(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin can update department'
            })
        else:
            data = self.deserialize(request, request.body, format=request.META.get('CONTENT_TYPE', 'application/json'))
            department_id = data.get('department_id', '')
            department_name = data.get('department_name', '')
            store_id = data.get('store_id', '')
            Department.objects.update_department(department_id=department_id, department_name=department_name, store_id=store_id)
            return self.create_response(request, {
                'success': True
            })
    
    def change_status(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin can change status of the department'
            })
        else:
            data = json.loads(request.body)
            department_id = data.get('department_id', '')
            active = data.get('active', '') 
            Department.objects.change_active_status(department_id=department_id, active=active)
            return self.create_response(request, {
                'success': True
            })
          
class AisleResource(ModelResource):
    products = fields.ToManyField('store.api.ProductResource', 'products', full=True)
    class Meta:
        queryset = Aisle.objects.all()
        resource_name = 'aisle'
        fields = ['id', 'aisle_name', 'department_id', 'store_aisle_number']
        allowed_methods = ['post','get']
        include_resource_uri = True
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        authorization = AdminAuthorization()
        
    def prepend_urls(self):
        return [
            url(r"^admin/(?P<resource_name>%s)/add%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('add_aisle'), name="api_add_aisle"),
             url(r"^admin/(?P<resource_name>%s)/update%s$" %
                 (self._meta.resource_name, trailing_slash()),
                 self.wrap_view('update_aisle'), name="api_update_aisle"),
             url(r"^admin/(?P<resource_name>%s)/status%s$" %
                 (self._meta.resource_name, trailing_slash()),
                 self.wrap_view('change_status'), name="api_change_status"),
        ]
            
    def add_aisle(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin can add aisle'
            })
        else:
            data = json.loads(request.body)
            department_id = data.get('department_id', '')
            aisle_name = data.get('aisle_name', '')
            store_aisle_number = data.get('store_aisle_number', '')
            
            Aisle.objects.add_aisle(department_id=department_id, aisle_name=aisle_name, store_aisle_number=store_aisle_number)
            
            return self.create_response(request, {
                'success': True
            })
    
    def update_aisle(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin can update aisle'
            })
        else:
            data = json.loads(request.body)
            aisle_id = data.get('aisle_id', '')
            department_id = data.get('department_id', '')
            aisle_name = data.get('aisle_name', '')
            store_aisle_number = data.get('store_aisle_number', '')
            Aisle.objects.update_aisle(aisle_id=aisle_id, department_id=department_id, aisle_name=aisle_name, store_aisle_number=store_aisle_number)      
            return self.create_response(request, {
                'success': True
            })
    
    def change_status(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin can change status of the aisle'
            })
        else:
            data = json.loads(request.body)
            aisle_id = data.get('aisle_id', '')
            active = data.get('active', '') 
            Aisle.objects.change_active_status(aisle_id=aisle_id, active=active)
            return self.create_response(request, {
                'success': True
            })
  
class ProductResource(ModelResource):
    
    class Meta:
        queryset = Product.objects.all()
        allowed_methods = ['get']
        include_resource_uri = True  
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        authorization = AdminAuthorization()
        
"""
    The following are resources called by customer, which hides admin privileged data
"""
class StoreAuthorization(Authorization):

    def read_list(self, object_list, bundle):
        return object_list.all()
        # if bundle.request.GET.get('id'):
        #     return object_list.filter(pk = bundle.request.GET.get('id'))
    
    def read_detail(self, object_list, bundle):
            raise BadRequest("Invalid request");
        
class CustomerStoreResource(ModelResource):
    departments = fields.ToManyField('store.api.CustomerDepartmentResource', 'departments', full=True)
    
    class Meta:        
        queryset = Store.objects.exclude(active=False)
        resource_name = 'current/store'
        allowed_methods = ['get']
        excludes = ['street', 'city', 'postal_code', 'phone', 'active']
        include_resource_uri = False
        limit = 0
        max_limit = 0
        authentication = SessionAuthentication()
        authorization = StoreAuthorization()
        throttle = CacheThrottle(throttle_at=200)
        filtering = { "id": ALL }
        cache = SimpleCache(timeout=10)
        
class CustomerDepartmentResource(ModelResource):
    aisles = fields.ToManyField('store.api.CustomerAisleResource', 'aisles', full=True)
    
    class Meta:
        queryset = Department.objects.exclude(active=False)
        fields = ['id', 'department_name']
        allowed_methods = ['get']
        include_resource_uri = False
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        cache = SimpleCache(timeout=10)

class CustomerAisleResource(ModelResource):
    
    class Meta:
        queryset = Aisle.objects.exclude(active=False)
        fields = ['id', 'aisle_name', 'department_id', 'store_aisle_number']
        allowed_methods = ['get']
        include_resource_uri = False
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        cache = SimpleCache(timeout=10)
        
class AisleProductAuthorization(Authorization):

    def read_list(self, object_list, bundle):
        print "getting products from a given aisle in a specified store"
        if bundle.request.GET.get('aisle__id') and bundle.request.GET.get('store__id'):
            return object_list.filter(aisle__id = bundle.request.GET.get('aisle__id'), store__id = bundle.request.GET.get('store__id'), product_available=True)
        else:
            raise BadRequest("Invalid request");
        
class CustomerProductResource(ModelResource):
     
    class Meta:
        queryset = Product.objects.exclude(product_available=False)
        resource_name = 'asile/products'
        allowed_methods = ['get']
        excludes = ['cost_price', 'product_available', 'price_change', 'position_on_aisle', 'search_tags', 'mark_up_category']
        include_resource_uri = True
        limit = 0
        max_limit = 0
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        authorization = AisleProductAuthorization()
        filtering = { "aisle__id": ALL }
        cache = SimpleCache(timeout=10)

class DealAuthorization(Authorization):

    def read_list(self, object_list, bundle):
        print "getting deals from a given dept in a specified store"
        if bundle.request.GET.get('department__id') and bundle.request.GET.get('store__id'):
            return object_list.filter(department__id = bundle.request.GET.get('department__id'), store__id = bundle.request.GET.get('store__id'),
                    product_available=True).exclude(image_url__icontains='smartkart_logo.jpg')
            
        else:
            raise BadRequest("Invalid request");

    def read_detail(self, object_list, bundle):
            raise BadRequest("Invalid request");

class DepartmentDealResource(ModelResource):
     
    class Meta:
        queryset = Product.objects.exclude(product_available=False, image_url__icontains='smartkart_logo.jpg')
        resource_name = 'deal/department'
        allowed_methods = ['get']
        excludes = ['cost_price', 'product_available', 'price_change', 'position_on_aisle', 'search_tags', 'mark_up_category']
        include_resource_uri = False
        limit = 6
        #max_limit = 0
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        authorization = DealAuthorization()
        filtering = {   "department__id": ALL,
                        "store__id": ALL,
                    }                     
        cache = SimpleCache(timeout=10)
 
'''
     Search APIs
'''             
class SearchStoreResource(ModelResource):
    
    class Meta:
        queryset = Store.objects.exclude(active=False)
        allowed_methods = ['get']
        excludes = ['street', 'city', 'postal_code', 'phone', 'active']
        include_resource_uri = False
        limit = 0
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        filtering = {
           "has_alcohol": ALL,
        }

class SearchDepartmentResource(ModelResource):
    
    class Meta:
        queryset = Department.objects.exclude(active=False)
        fields = ['id']
        allowed_methods = ['get']
        include_resource_uri = False
        limit = 0
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
                
class SearchAisleResource(ModelResource):
    
    class Meta:
        queryset = Aisle.objects.exclude(active=False)        
        fields = ['id', 'aisle_name']        
        allowed_methods = ['get']
        include_resource_uri = False
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
                
class SearchProductResource(ModelResource):
    store = fields.ForeignKey(SearchStoreResource, 'store', full=True)
    aisle = fields.ForeignKey(SearchAisleResource, 'aisle', full=True)
    department = fields.ForeignKey(SearchDepartmentResource, 'department', full=True)
    
    class Meta:
        queryset = Product.objects.exclude(product_available=False, store__active=False, department__active=False, aisle__active=False)
        resource_name = 'search'
        allowed_methods = ['get']
        excludes = ['cost_price', 'product_available', 'price_change', 'position_on_aisle', 'modified_price', 'mark_up_category']
        include_resource_uri = False
        limit = 0
        max_limit = 0  
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        
    def apply_filters(self, request, applicable_filters):        
        base_object_list = super(SearchProductResource, self).apply_filters(request, applicable_filters)
        query = request.GET.get('query', None)
        filters = {}
        if query:
            qset = (
                Q(name__icontains=query, **filters) |
                Q(brand_name__icontains=query, **filters) |
                Q(description__icontains=query, **filters) |
                Q(aisle__aisle_name__icontains=query, **filters)
            )
            base_object_list = base_object_list.filter(qset, product_available=True, store__active=True, department__active=True, aisle__active=True).distinct()
        else:
            raise BadRequest('No search query')
        return base_object_list.filter(**filters).distinct()
       
'''
    Admin add delivery time
'''
# class DevliveryTimeResource(ModelResource):
#     class Meta:
#         queryset = DeliveryTime.objects.all()
#         resource_name = 'delivery_time'
#         include_resource_uri = True
#         allowed_methods = ['post','get']
#         authentication = SessionAuthentication()
#     
#     def prepend_urls(self):
#         return [
#             url(r"^admin/(?P<resource_name>%s)/add%s$" %
#                 (self._meta.resource_name, trailing_slash()),
#                 self.wrap_view('add_delivery_time'), name="api_add_delivery_time"),
#         ]
#             
#     def add_delivery_time(self, request, **kwargs):
#         self.method_check(request, allowed=['post'])   
#         self.is_authenticated(request)
#         if not request.user.is_admin:
#             return self.create_response(request, {
#                 'success': False,
#                 'message': 'Sorry! Only admin can add delivery time.'
#             })      
#         data = json.loads(request.body)
#         delivery_time = data.get('delivery_time', '')
#         delivery_time_obj = DeliveryTime.objects.create(display_name=delivery_time)
#         return self.create_response(request, {
#             'success': True,
#             'message': 'Successfully added new delivery time.',
#             'id': delivery_time_obj.id
#         })
        
'''
    Block delivery time apis
'''
class DeliveryTimeAuthorization(Authorization):
    def read_list(self, object_list, bundle):
        delivery_date = bundle.request.GET.get('delivery_date', '')
        store_id = bundle.request.GET.get('store_id', '')
        if(delivery_date and store_id):
            _delivery_date = datetime.strptime(delivery_date , '%Y-%m-%d')
            return object_list.filter(delivery_date = _delivery_date, store__id=store_id)
        else:
            raise BadRequest('Invalid request for delivery time')

class DevliveryTimeManagerResource(ModelResource):
    class Meta:
        queryset = BlockedDeliveryTime.objects.all()
        resource_name = 'delivery'
        include_resource_uri = False
        #allowed_methods = ['get','post', 'delete']
        authentication = SessionAuthentication()
        filtering = { "delivery_date": ALL, 'store__id' : ALL }
        authorization = DeliveryTimeAuthorization()
        
    def prepend_urls(self):
        return [
            url(r"^admin/(?P<resource_name>%s)/block%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('block_time'), name="api_block_time"),
            url(r"^admin/(?P<resource_name>%s)/block/remove%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('remove_blocked_time'), name="api_remove_blocked_time"),
            url(r"^(?P<resource_name>%s)/available%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_available_times'), name="api_get_available_times"),
        ]
        
    def block_time(self, request, **kwargs):
        self.method_check(request, allowed=['post'])   
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Sorry! Only admin can block delivery time.'
            })        
        data = json.loads(request.body)
        try:
            delivery_date = datetime.strptime(data.get('delivery_date', ''),'%Y-%m-%d').date()
            store_id = data.get('store_id', '')
            from_time_to_block = datetime.strptime(data.get('from_time_to_block', ''), '%I:%M %p').time()
            to_time_to_block = datetime.strptime(data.get('to_time_to_block', ''), '%I:%M %p').time()
            BlockedDeliveryTime.objects.create(delivery_date=delivery_date,from_time=from_time_to_block,to_time=to_time_to_block,store_id=store_id)
        except Exception as e:
            return self.create_response(request, {
              'success': False,
              'message': 'Invalid date or time'
             })
        return self.create_response(request, {
            'success': True,
            'message':'Successfully blocked the delivery time'
        })
        
    def remove_blocked_time(self, request, **kwargs):
        self.method_check(request, allowed=['delete'])   
        self.is_authenticated(request)
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Sorry! Only admin can remove blocked delivery time.'
            })    
        
        delivery_date = datetime.strptime(request.GET.get('date', ''),'%Y-%m-%d').date()
        store_id = request.GET.get('store_id', '')
        from_blocked_time = datetime.strptime(request.GET.get('from_blocked_time', ''), '%I:%M %p').time()
        to_blocked_time = datetime.strptime(request.GET.get('to_blocked_time', ''), '%I:%M %p').time()
        try:
            blocked_time = BlockedDeliveryTime.objects.get(delivery_date=delivery_date,from_time=from_blocked_time,to_time=to_blocked_time,store__pk=store_id)
            blocked_time.delete()
            return self.create_response(request, {
                'success': True,
                'message':'Successfully un-blocked the delivery time'
            })
        except ObjectDoesNotExist:
            return self.create_response(request, {
                'success': False,
                'message':'Cannot find a matching delivey time to unblock'
            })
        
    def get_available_times(self, request, **kwargs):
        self.method_check(request, allowed=['get'])   
        self.is_authenticated(request)
        
        from pytz import timezone  
        from datetime import date, time, timedelta
        from django.core.serializers.json import DjangoJSONEncoder
        from django.http import HttpResponse
        
        delivery_date = datetime.strptime(request.GET.get('date', ''),'%Y-%m-%d').date()
        store_id = request.GET.get('store_id', '')
        store = Store.objects.get(pk=store_id)
        blocked_delivery_times = BlockedDeliveryTime.objects.filter(delivery_date=delivery_date, store=store)
        
        tz = timezone(store.time_zone)
        current_datetime = datetime.now(tz)
        current_date = current_datetime.date()
        
        if current_date == delivery_date:
            print 'delivery is today ' +str(blocked_delivery_times.count())
            options = []

            display_start_time = current_datetime.time()

            while(display_start_time < store.delivery_start_time):
                display_start_time = (datetime.combine(date(1,1,1),display_start_time) + timedelta(hours=1)).time()

            if display_start_time.minute > 30:
                display_start_time = (datetime.combine(date(1,1,1),display_start_time) + timedelta(hours=1)).time()

            display_end_time = (datetime.combine(date(1,1,1),display_start_time) + timedelta(hours=1)).time()
            added_one_hour = False
            added_two_hour = False
            
            display_start_time = display_start_time.replace(minute=0, second=0, microsecond=0)
            #display_end_time = (datetime.combine(date(1,1,1),display_start_time) + timedelta(hours=1)).time()
            display_end_time = display_end_time.replace(minute=0, second=0, microsecond=0)
            while display_end_time <= store.delivery_end_time and display_end_time >= store.delivery_start_time:
                #TODO:Find out why it doesn't work without the following if statements
                if(display_end_time == store.delivery_end_time):
                    break

                blocked = False
                for bt in blocked_delivery_times:
                    if ((display_start_time == bt.from_time) and (display_end_time == bt.to_time)):
                        blocked = True
                        break
                
                if blocked:
                    print "blocked"
                    options.append({
                                "time": {
                                            "from_time": display_start_time.strftime('%I:%M %p'),#time.strftime('%I:%M %p'),
                                            "to_time": display_end_time.strftime('%I:%M %p'),
                                            "display_name": display_start_time.strftime('%I:%M %p')+' - '+display_end_time.strftime('%I:%M %p')+' - SOLD OUT',#time.strftime('%I:%M %p')+' - '+_time.strftime('%I:%M %p')+' - SOLD OUT',
                                            "fee": store.one_hour_delivery_fee,
                                            "urgent":False,
                                            "available": False
                                        }                      
                                })
                    display_start_time = display_end_time
                    display_end_time = (datetime.combine(date(1,1,1),display_end_time) + timedelta(hours=1)).time()
                    continue

                print "not blocked"
                
                if(not added_one_hour):
                    added_one_hour = True
                    if (store.one_hour_delivery_available):
                        print 'one'
                        options.append({
                                    "time": {
                                                "from_time": display_start_time.strftime('%I:%M %p'),#time.strftime('%I:%M %p'),
                                                "to_time": display_end_time.strftime('%I:%M %p'),
                                                "display_name": display_start_time.strftime('%I:%M %p')+' - '+display_end_time.strftime('%I:%M %p')+' - $' + str(store.one_hour_delivery_fee),#time.strftime('%I:%M %p')+' - $' + str(store.one_hour_delivery_fee),
                                                "fee": store.one_hour_delivery_fee,
                                                "urgent":True,
                                                "is_one_hour":True,
                                                "is_two_hour":False,
                                                "is_three_hour":False,
                                                "available": True
                                            }                         
                                    })
                    display_start_time = display_end_time
                    display_end_time = (datetime.combine(date(1,1,1),display_end_time) + timedelta(hours=1)).time()
                    continue
                elif(not added_two_hour):                    
                    added_two_hour = True
                    if(store.two_hour_delivery_available):
                        print 'two'
                        options.append({
                                    "time": {
                                                "from_time": display_start_time.strftime('%I:%M %p'),#time.strftime('%I:%M %p'),
                                                "to_time": display_end_time.strftime('%I:%M %p'),
                                                "display_name": display_start_time.strftime('%I:%M %p')+' - '+display_end_time.strftime('%I:%M %p')+' - $' + str(store.two_hour_delivery_fee),#time.strftime('%I:%M %p')+' - $' + str(store.two_hour_delivery_fee),
                                                "fee": store.two_hour_delivery_fee,
                                                "urgent":True,
                                                "is_one_hour":False,
                                                "is_two_hour":True,
                                                "is_three_hour":False,
                                                "available": True
                                            }                       
                                    })
                    display_start_time = display_end_time
                    display_end_time = (datetime.combine(date(1,1,1),display_end_time) + timedelta(hours=1)).time()
                    continue  
                elif(store.three_hour_delivery_available and not blocked):
                    options.append({
                                "time": {
                                            "from_time": display_start_time.strftime('%I:%M %p'),#time.strftime('%I:%M %p'),
                                            "to_time": display_end_time.strftime('%I:%M %p'),
                                            "display_name": display_start_time.strftime('%I:%M %p')+' - '+display_end_time.strftime('%I:%M %p')+' - $' + str(store.three_hour_delivery_fee),#time.strftime('%I:%M %p')+' - $' + str(store.three_hour_delivery_fee),
                                            "fee": store.three_hour_delivery_fee,
                                            "urgent":False,
                                            "is_one_hour":False,
                                            "is_two_hour":False,
                                            "is_three_hour":True,
                                            "available": True
                                        }                         
                                })
                #time = (datetime.combine(date(1,1,1),time) + timedelta(hours=1)).time()
                display_start_time = display_end_time
                display_end_time = (datetime.combine(date(1,1,1),display_end_time) + timedelta(hours=1)).time()
                
            if(len(options)>0):
                data = [{"success" : True, 'delivery_options':options}]
            else:
                data = [{"success" : False, 'message': 'No delivery time available'}]
            data_string = json.dumps(data, cls=DjangoJSONEncoder)
            return HttpResponse(data_string, mimetype='application/json')
        else:
            print 'delivery not today'
            options = []
            if(store.one_hour_delivery_available or store.two_hour_delivery_available or store.three_hour_delivery_available):
                display_start_time = store.delivery_start_time
                display_start_time = display_start_time.replace(minute=0, second=0, microsecond=0)
                display_end_time = (datetime.combine(date(1,1,1),display_start_time) + timedelta(hours=1)).time()
                display_end_time = display_end_time.replace(minute=0, second=0, microsecond=0)
                #while display_end_time <= store.delivery_end_time >= display_end_time:
                while store.delivery_end_time >= display_end_time:
                    #TODO:Find out why it doesn't work without the following if statements
                    if(display_end_time == store.delivery_end_time):
                        break
                    
                    blocked = False
                    for bt in blocked_delivery_times:
                        if ((display_start_time == bt.from_time) and (display_end_time == bt.to_time)):
                            blocked = True
                            break
                
                    if blocked:
                        print "blocked"
                        options.append({
                                    "time": {
                                                "from_time": display_start_time.strftime('%I:%M %p'),#time.strftime('%I:%M %p'),
                                                "to_time": display_end_time.strftime('%I:%M %p'),
                                                "display_name": display_start_time.strftime('%I:%M %p')+' - '+display_end_time.strftime('%I:%M %p')+' - SOLD OUT',#time.strftime('%I:%M %p')+' - '+_time.strftime('%I:%M %p')+' - SOLD OUT',
                                                "fee": store.one_hour_delivery_fee,
                                                "urgent":False,
                                                "available": False
                                            }                      
                                    })
                        display_start_time = display_end_time
                        display_end_time = (datetime.combine(date(1,1,1),display_end_time) + timedelta(hours=1)).time()
                        continue

                    options.append({
                                "time": {
                                            "from_time": display_start_time.strftime('%I:%M %p'),#time.strftime('%I:%M %p'),
                                            "to_time": display_end_time.strftime('%I:%M %p'),
                                            "display_name": display_start_time.strftime('%I:%M %p')+' - '+display_end_time.strftime('%I:%M %p')+' - $' + str(store.three_hour_delivery_fee),#time.strftime('%I:%M %p')+' - $' + str(store.three_hour_delivery_fee),
                                            "fee": store.three_hour_delivery_fee,
                                            "urgent":False,
                                            "is_one_hour":False,
                                            "is_two_hour":False,
                                            "is_three_hour":True,
                                            "available": True
                                        }                         
                                })
                    display_start_time = display_end_time
                    display_end_time = (datetime.combine(date(1,1,1),display_end_time) + timedelta(hours=1)).time()

                    print "not today not blocked "+ str(display_end_time) +" - "+str(store.delivery_end_time)
                    print "not today not blocked type "+ str(type(display_end_time)) +" - "+str(type(store.delivery_end_time))
                data = [{"success" : True, 'delivery_options':options}]
            else:
                data = [{"success" : False, 'message': 'No delivery time available'}]
            data_string = json.dumps(data, cls=DjangoJSONEncoder)
            return HttpResponse(data_string, mimetype='application/json')