# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Order', fields ['gst_hst_number']
        db.delete_unique(u'order_order', ['gst_hst_number'])


        # Changing field 'Order.gst_hst_number'
        db.alter_column(u'order_order', 'gst_hst_number', self.gf('django.db.models.fields.CharField')(max_length=16))

    def backwards(self, orm):

        # Changing field 'Order.gst_hst_number'
        db.alter_column(u'order_order', 'gst_hst_number', self.gf('django.db.models.fields.CharField')(max_length=10, unique=True))
        # Adding unique constraint on 'Order', fields ['gst_hst_number']
        db.create_unique(u'order_order', ['gst_hst_number'])


    models = {
        u'cart.cart': {
            'Meta': {'object_name': 'Cart'},
            'coupon': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coupon.Coupon']", 'null': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'delivery_charge': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '5', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_check_out': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modification_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usermanagement.User']"})
        },
        u'coupon.coupon': {
            'Meta': {'object_name': 'Coupon'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'created_by'", 'null': 'True', 'to': u"orm['usermanagement.User']"}),
            'discount_per_store': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'dollar_amount': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'expiry_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_checked_out': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'random_hash': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'redeemed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'redeemed_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'redeemed_by'", 'null': 'True', 'to': u"orm['usermanagement.User']"}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        },
        u'order.order': {
            'Meta': {'object_name': 'Order'},
            'assigned_driver': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'driver_assigned'", 'null': 'True', 'to': u"orm['usermanagement.User']"}),
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cart'", 'to': u"orm['cart.Cart']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'delivery_charge': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '6', 'decimal_places': '2'}),
            'delivery_date': ('django.db.models.fields.DateField', [], {}),
            'delivery_instructions': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'discount': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '6', 'decimal_places': '2'}),
            'discount_tax_deduction': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '6', 'decimal_places': '2'}),
            'from_delivery_time': ('django.db.models.fields.TimeField', [], {}),
            'gst_hst_number': ('django.db.models.fields.CharField', [], {'default': "'830627378 RT0001'", 'max_length': '16'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order_confirmation_code': ('django.db.models.fields.CharField', [], {'default': "'6991'", 'max_length': '4'}),
            'order_number': ('django.db.models.fields.CharField', [], {'default': "'1504152350233696'", 'unique': 'True', 'max_length': '20'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'price_paid': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '6', 'decimal_places': '2'}),
            'shipping_address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['order.ShippingAddress']", 'null': 'True', 'on_delete': 'models.SET_NULL'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PENDING'", 'max_length': '20'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']"}),
            'tax': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '6', 'decimal_places': '2'}),
            'tax_paid': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '6', 'decimal_places': '2'}),
            'tip': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '6', 'decimal_places': '2'}),
            'to_delivery_time': ('django.db.models.fields.TimeField', [], {}),
            'total_price': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '6', 'decimal_places': '2'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usermanagement.User']"})
        },
        u'order.orderitem': {
            'Meta': {'object_name': 'OrderItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'items'", 'to': u"orm['order.Order']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '6', 'decimal_places': '2'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Product']"}),
            'quantity': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PENDING'", 'max_length': '10'}),
            'tax': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '6', 'decimal_places': '2'})
        },
        u'order.postalcodevalidator': {
            'Meta': {'object_name': 'PostalCodeValidator'},
            'area_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        u'order.shippingaddress': {
            'Meta': {'object_name': 'ShippingAddress'},
            'apt_number': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'street_address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usermanagement.User']"})
        },
        u'product.product': {
            'Meta': {'object_name': 'Product'},
            'aisle': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'to': u"orm['store.Aisle']"}),
            'alcohol_content': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2'}),
            'brand_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'clearance_sale_savings_in_cents': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cost_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Department']"}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'has_clearance_sale': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_limited_time_offer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_value_added_promotion': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'is_kosher': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_seasonal': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_vqa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'large_image_url': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'mark_up_category': ('django.db.models.fields.SmallIntegerField', [], {}),
            'modified_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'package_unit_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'package_unit_volume_in_milliliters': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'position_on_aisle': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2'}),
            'price_change': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price_per_liter_in_cents': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'price_per_liter_of_alcohol_in_cents': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'product_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'size': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'store'", 'to': u"orm['store.Store']"}),
            'store_product_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sugar_content': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'sugar_in_grams_per_liter': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tasting_note': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'taxable': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'total_package_units': ('django.db.models.fields.SmallIntegerField', [], {}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'store.aisle': {
            'Meta': {'object_name': 'Aisle'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'aisle_icon_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'aisle_name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'department': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'aisles'", 'to': u"orm['store.Department']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'store.department': {
            'Meta': {'object_name': 'Department'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'department_name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'departments'", 'to': u"orm['store.Store']"})
        },
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'delivery_end_time': ('django.db.models.fields.TimeField', [], {}),
            'delivery_start_time': ('django.db.models.fields.TimeField', [], {}),
            'has_alcohol': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_free_delivery_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'min_order_for_free_delivery': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'minimum_order': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'one_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'phone': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'store_logo_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'three_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'three_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'time_zone': ('django.db.models.fields.CharField', [], {'default': "'America/Toronto'", 'max_length': '50'}),
            'two_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'two_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        },
        u'usermanagement.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'email_subscription': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'expiry': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 4, 15, 0, 0)'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'forgot_password_hash': ('django.db.models.fields.CharField', [], {'default': "'tfLb6xnKGHCYMzFQERDYtwxtyRQ2BLMR95SbEGsG'", 'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_driver': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_first_order': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'lastOrderID': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'last_store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']", 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        }
    }

    complete_apps = ['order']