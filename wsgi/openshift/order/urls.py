'''
Created on Nov 09, 2013

@author: Hisham
'''
from django.conf.urls import patterns, url, include
from order.api import OrderResource, ShippingAddressResource, PostalCodeResource, OrderItemResource, OrderItemRefundResource, ManagementResource


order_resource = OrderResource()
shipping_address_resource = ShippingAddressResource()
management_resource = ManagementResource()
post_code_resource = PostalCodeResource()
order_item_resource = OrderItemResource()
order_item_refund_resource = OrderItemRefundResource()
# block_devlivery_time_resource = DevliveryTimeManagerResource()
# devlivery_time_resource = DevliveryTimeResource()

urlpatterns = patterns('',
    #url(r'^asa/$', 'order.views.add_shipping', None, 'add_shipping'),
    url(r'^api/', include(order_resource.urls)),    
    url(r'^api/', include(shipping_address_resource.urls)),  
    url(r'^api/', include(management_resource.urls)),
    url(r'^api/', include(order_item_resource.urls)),
    url(r'^api/', include(post_code_resource.urls)),
    url(r'^api/', include(order_item_refund_resource.urls)),
    #url(r'^api/', include(block_devlivery_time_resource.urls)),
    #url(r'^api/', include(devlivery_time_resource.urls)),    
);