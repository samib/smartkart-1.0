from django import template

register = template.Library()

@register.filter(name='get_order_total')
def get_order_total(value, order): 
   	return value + order.tip + order.tax + order.delivery_charge - order.discount - order.discount_tax_deduction