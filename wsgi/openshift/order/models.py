import random

from decimal import Decimal
from datetime import datetime
from django.db import models
from django.db.models import Sum
from django.template.loader import get_template
from django.template import Context

import openshift.settings
from cart.models import Cart
from payment.models import Payment
from product.models import Product
from usermanagement.models import User
from store.models import Store
from usermanagement import utils
from order.tasks import send_mass_html_mail

def get_unique_order_number():
    current_date_time = datetime.today().strftime('%y%m%d%H%M%S')
    random_num = random.randint(1, 9999)
    return '%s%s' % (current_date_time, random_num)

def get_confirmation_code():
    random_num = random.randint(1000, 9999)
    return '%s'% (random_num)


def get_random_aplha_numeric_string():
    current_time = datetime.today().strftime('%H%M%S')
    random_string = ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ') for i in range(4))
    return '%s%s' % (current_time, random_string)    
    
class AddressManger(models.Manager):
    def create_shipping_address(self, user, label, street_address, apt_number, postal_code, note):
        if not user:
            raise ValueError('shipping address must have a user')
        
        if not label:
            raise ValueError('shipping address must have a label')
        
        if not street_address:
            raise ValueError('shipping address must have a street_address')
                
        if not postal_code:
            raise ValueError('shipping address must have a postalcode')
        
        shipping_address = self.create(user=user, label=label, street_address=street_address, 
                                       apt_number=apt_number, postal_code=postal_code, note=note)
        return shipping_address
        
class ShippingAddress(models.Model):
    objects = AddressManger()
    user = models.ForeignKey(User)
    label = models.CharField(max_length=100)
    street_address = models.CharField(max_length=100)
    apt_number = models.CharField(max_length=100, blank=True, null=True)
    postal_code = models.CharField(max_length=6)
    note = models.TextField(blank=True, null=True)
    
class OrderManager(models.Manager):
    def create_order(self, user, delivery_date, from_delivery_time, to_delivery_time, store_id, cart, shipping_address_id, tip, delivery_charge, 
        phone, delivery_instructions, discount, discount_tax_deduction):
        if not user:
            raise ValueError('Order must have a user')
        
        if not delivery_date:
            raise ValueError('Order must have a delivery_date')
        
        if not from_delivery_time:
            raise ValueError('Order must have a from_delivery_time')
        
        if not to_delivery_time:
            raise ValueError('Order must have a to_delivery_time')

        if not store_id:
            raise ValueError('Order must have a store_id')
                
        if not cart:
            raise ValueError('Order must have a cart')
        
        if not shipping_address_id:
            raise ValueError('Order must have a shipping_address_id')
        
        shipping_address = ShippingAddress.objects.get(pk=shipping_address_id)
        try:
            PostalCodeValidator.objects.get(postal_code = shipping_address.postal_code.strip().lower()[0:3])
        except PostalCodeValidator.DoesNotExist:
            raise ValueError('shipping address not currently served')
            return None
#         cart = Cart.objects.select_related().get(pk=cart_id, user=user)        
#         if not cart:
#             raise ValueError('Card does not belong to the current user')
#             return None
        
        charge = Payment.objects.get(cart=cart)
        if not charge == None:
            order = self.create(user=user, delivery_date=delivery_date, from_delivery_time=from_delivery_time, to_delivery_time=to_delivery_time, 
                                store_id=store_id, cart=cart, shipping_address=shipping_address, tip=tip, delivery_charge=delivery_charge,
                                phone=phone, delivery_instructions=delivery_instructions, discount=discount, discount_tax_deduction=discount_tax_deduction)
            return order
        else:
            raise ValueError('A successful payment was not found for this order')
            return None
    
    def change_order_status(self, order_id, status):
        if not order_id:
            raise ValueError('change_order_status must have a order_id')
        
        if not status:
            raise ValueError('change_order_status must have a status')
        
        return Order.objects.filter(pk = order_id).update(status = status)
    
    def assign_to_driver(self, order_id, driver_id): 
        if not order_id:
            raise ValueError('assign_to_driver must have a order_id')

        if not driver_id:
            raise ValueError('assign_to_driver must have a driver_id')

        Order.objects.filter(pk = order_id).update(assigned_driver = User.objects.get(pk=driver_id))
        return self.change_order_status(order_id, 'ASNTD')
                 
class Order(models.Model):
    objects = OrderManager()    
    order_number = models.CharField(max_length=20, editable=False, unique=True, default=get_unique_order_number)
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)
    delivery_date = models.DateField()
    from_delivery_time = models.TimeField()
    to_delivery_time = models.TimeField()
    cart = models.ForeignKey(Cart, related_name="cart")
    shipping_address = models.ForeignKey(ShippingAddress, null=True, on_delete=models.SET_NULL)
    store = models.ForeignKey(Store)
    assigned_driver = models.ForeignKey(User, related_name="driver_assigned", blank=True, null=True)
    tip = models.DecimalField(max_digits=6, default=0.00, decimal_places=2)  
    total_price = models.DecimalField (max_digits=6, default=0.00, decimal_places=2)
    tax = models.DecimalField (max_digits=6, default=0.00, decimal_places=2)
    delivery_charge = models.DecimalField (max_digits=6, default=0.00, decimal_places=2)
    order_confirmation_code = models.CharField(max_length=4, editable=False, default=get_confirmation_code)
    phone = models.CharField(max_length=25)
    delivery_instructions = models.CharField(max_length=500, blank=True, null=True)
    discount = models.DecimalField(max_digits=6, default=0.00, decimal_places=2)
    discount_tax_deduction = models.DecimalField(max_digits=6, default=0.00, decimal_places=2)
    ORDER_STATUS = (
        ('PENDING', 'Pending'),
        ('ASSIGNED TO SHOPPER', 'AssignedToShopper'),
        ('SHOPPING', 'Shopping'),
        ('OUT FOR DELIVERY', 'OutForDelivery'),
        ('DELIVERED', 'Delivered'),
        ('CANCELED', 'Canceled'),
    )
    status = models.CharField(max_length=20, choices=ORDER_STATUS, default="PENDING")
    price_paid = models.DecimalField(max_digits=6, default=0.00, decimal_places=2)
    tax_paid = models.DecimalField(max_digits=6, default=0.00, decimal_places=2)
    gst_hst_number = models.CharField(max_length=16, editable=False, default="830627378 RT0001")
    drivers_pay = models.DecimalField(max_digits=6, default=0.00, decimal_places=2)
    
    def setTotalPrice(self):
        data = OrderItem.objects.filter(order=self).aggregate(Sum('price'))
        self.total_price = data['price__sum']
        
    def setTax(self):
        data = OrderItem.objects.filter(order=self).aggregate(Sum('tax'))
        self.tax = data['tax__sum'] + Decimal(str(round(self.delivery_charge * Decimal(openshift.settings.TAX_RATE),2))).quantize(openshift.settings.TWO_PLACES)
        
    def updatePhone(self, phone):
        self.phone = phone

    def set_expense(self, tax_paid, price_paid):
        self.tax_paid = tax_paid 
        self.price_paid = price_paid
        conditionalPricePaid = self.total_price if (price_paid == 0.00 or (price_paid > self.total_price)) else price_paid
        self.drivers_pay = Decimal(openshift.settings.DRIVERS_BASE_PAY) + self.tip + Decimal(str(round(conditionalPricePaid/openshift.settings.DRIVERS_COMMISION_BASE,2))).quantize(openshift.settings.TWO_PLACES)
        self.save()   
        
    def assign_driver(self, driver, status):
        self.assigned_driver=driver
        self.status='ASSIGNED TO SHOPPER'
    
    def send_order_create_email(self): #TODO: Make one generic email function
        context = Context({
            'order': self,
        })
        
        customer_subject = 'Smartkart Confirmation of your order from '+self.store.name+' - '+self.order_number
        admin_email_subject = 'New Order: DD: '+str(self.delivery_date)+' from '+self.store.name+' - '+self.order_number
        customer_plaintext_template = get_template('emails/order.txt')
        admin_plaintext_template = get_template('emails/admin_notification.txt')
        customer_plaintext_content = customer_plaintext_template.render(context)
        admin_plaintext_content = admin_plaintext_template.render(context)
        
        customer_html_template = get_template('emails/order.html')
        customer_html_content = customer_html_template.render(context)
        admin_html_template = get_template('emails/admin_notification.html')
        admin_html_content = admin_html_template.render(context)
        
        admins = User.objects.get_all_admins()
        admin_emails = [a.email for a in admins]

        if openshift.settings.ON_OPENSHIFT:
            datatuple = (
                (customer_subject, customer_plaintext_content, customer_html_content, 'auto-confirm@smartkart.ca', [self.user.email]),
                (admin_email_subject, admin_plaintext_content, admin_html_content, 'auto-confirm@smartkart.ca', admin_emails),
            )
            print 'sending email using celery and redis'
            send_mass_html_mail(datatuple, fail_silently=False, user=None, password=None, 
                            connection=None)
        else: 
            print 'sync sending email'      
            utils.send_email(subject=customer_subject, plaintext_content=customer_plaintext_content, to_email=[self.user.email], html_content=customer_html_content)
            utils.send_email(subject=admin_email_subject, plaintext_content=admin_plaintext_content, to_email=admin_emails, html_content=admin_html_content)

    def send_order_notification_email_to_driver(self): #TODO: Make one generic email function
        print 'sending email to driver'
        context = Context({
            'order': self,
        })

        plaintext_template = get_template('emails/driver_notification.txt')
        plaintext_content = plaintext_template.render(context)

        html_template = get_template('emails/driver_notification.html')
        html_content = html_template.render(context)

        subject = 'SmartKart you have an order to deliver from '+self.store.name+'-'+self.order_number
        utils.send_email(subject=subject, plaintext_content=plaintext_content, to_email=[self.assigned_driver.email], html_content=html_content)
        
    def send_order_cancel_email(self): #TODO: Make one generic email function
        context = Context({
            'order': self,
        })

        plaintext_template = get_template('emails/cancel.txt')
        plaintext_content = plaintext_template.render(context)

        html_template = get_template('emails/cancel.html')
        html_content = html_template.render(context)

        subject = 'Your order from '+self.store.name+' is canceled'+' - '+self.order_number
        utils.send_email(subject=subject, plaintext_content=plaintext_content, to_email=[self.user.email], html_content=html_content)  

class OrderItemManager(models.Manager):
    def add_order_item(self, order, product_id, quantity, note):
        if not order:
            raise ValueError('OrderItem must have a order')
        
        if not product_id:
            raise ValueError('CartItem must have a product')
        
        if not quantity:
            raise ValueError('CartItem must have a quantity')
        
        #order_item = self.create(order=order, product=Product.objects.get(pk=product_id), quantity=quantity,
        #                          note=note)
        order_item = self.create(order=order, product_id=product_id, quantity=quantity,
                                  note=note)
        order_item.set_price()
        order_item.set_tax()
        order_item.save()
        return order_item
    
    #def change_item_status(self, status): #item_id
        #if not item_id:
            #raise ValueError('change_item_status must have a item_id')
        
        #if not status:
            #raise ValueError('change_item_status must have a status')
        
        #item = OrderItem.objects.get(pk = item_id)
        #self.update_status(status = status)
        #item.save()
        #return item
    
class OrderItem(models.Model):
    objects = OrderItemManager()
    order = models.ForeignKey(Order, related_name="items")
    product = models.ForeignKey(Product)
    quantity = models.PositiveSmallIntegerField()
    price = models.DecimalField(max_digits=6, decimal_places=2, default=0.00)
    tax = models.DecimalField(max_digits=6, decimal_places=2, default=0.00)
    note = models.TextField(blank=True, null=True)
    ITEM_STATUS = (
        ('PENDING', 'Pending'),
        ('FOUND', 'Found'),
        ('SUBSTITUTE', 'Substitute'),
        ('REFUND', 'Refund'),
        ('CANCELED', 'Cancel'),
    )
    status = models.CharField(max_length=10, choices=ITEM_STATUS, default="PENDING")
    
    def set_tax(self):
        if self.product.taxable:
            self.tax = Decimal(self.product.price) * Decimal(self.quantity) * Decimal(openshift.settings.TAX_RATE)
            
    def set_price(self):
        self.price = Decimal(self.product.price) * Decimal(self.quantity)
        
    def update_status(self, status):
        self.status = status
            
class PostalCodeValidator(models.Model):
    postal_code = models.CharField(max_length=3)
    area_name = models.CharField(max_length=50)