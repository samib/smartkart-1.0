'''
Created on Nov 10, 2013

@author: Hisham
'''
import json

from datetime import datetime
from decimal import Decimal
from django.conf.urls import url
from django import forms
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db.models import Sum

from tastypie import fields
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import Authorization
from tastypie.exceptions import Unauthorized, BadRequest
from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from tastypie.throttle import CacheThrottle
from tastypie.utils import trailing_slash
from tastypie.cache import SimpleCache
from tastypie.validation import FormValidation

import openshift.settings
from cart.models import Cart, CartItem, DeliveryCharge
from store.models import Department, Aisle
from store.api import AdminAuthorization
from order.models import Order, OrderItem, ShippingAddress, PostalCodeValidator
from payment.api import PaymentResource
from payment.models import Payment
from product.models import Product
from usermanagement.models import User

'''
    Shipping Address APIs
'''
class AddressForm(forms.ModelForm):

    class Meta:
        model = ShippingAddress
        exclude = ('user',)
        
class AddressAuthorization(Authorization):

    def read_list(self, object_list, bundle):
        if bundle.request.GET.get('id'):
            return object_list.filter(pk = bundle.request.GET.get('id'), user = bundle.request.user)
        else:
            return object_list.filter(user = bundle.request.user)
        
    def read_detail(self, object_list, bundle):
        if bundle.request.GET.get('id'):
            return object_list.filter(pk = bundle.request.GET.get('id'), user = bundle.request.user)
        else:
            return object_list.filter(user = bundle.request.user)
                        
class ShippingAddressResource(ModelResource):
    class Meta:
        queryset = ShippingAddress.objects.all()
        resource_name = 'shipping'
        fields = ['id', 'label', 'street_address','apt_number','postal_code','note']
        allowed_methods = ['post','get']
        include_resource_uri = False
        authentication = SessionAuthentication()
        authorization = AddressAuthorization()
        throttle = CacheThrottle(throttle_at=200)
        validation = FormValidation(form_class=AddressForm)
        
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/add%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('add_shipping_address'), name="api_add_shipping_address"),
            url(r"^(?P<resource_name>%s)/remove%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('remove_shipping_address'), name="api_remove_shipping_address")
        ]

    def add_shipping_address(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = json.loads(request.body)        
        label = data.get('label', '')
        street_address = data.get('street_address', '')
        apt_number = data.get('apt_number', '')
        postal_code = data.get('postal_code', '').replace(" ", "")
        note = data.get('note', '')
        pc = postal_code.strip().lower()[0:3]
        try:
            PostalCodeValidator.objects.get(postal_code = pc)
        except PostalCodeValidator.DoesNotExist:
            return self.create_response(request, {
                    'success': False,
                    'message': 'Shipping address not currently served' 
            })

        shipping_address = ShippingAddress.objects.create_shipping_address(user=request.user, label=label, street_address=street_address, 
                                                        apt_number=apt_number, postal_code=postal_code, note=note)
        return self.create_response(request, {
            'success': True,
            'shipping_address_id': shipping_address.id,
            'message': 'Shipping address successfully added' 
        })
        
    def remove_shipping_address(self, request, **kwargs):
        self.method_check(request, allowed=['delete'])
        self.is_authenticated(request)
        shipping_address_id = request.GET.get('id', '')
        ShippingAddress.objects.filter(pk = shipping_address_id,user=request.user).delete()
        return self.create_response(request, {
            'success': True,
            'message': 'Shipping address successfully deleted' 
        })
        
'''
    OrderItem APIs
    
    #NOTE: Once we change an active Department to not active the assigned driver would not be able to see Department info in his view
    #NOTE: Once we change an active Aisle to not active the assigned driver would not be able to see Aisle info in his view
'''            
class DepartmentResource(ModelResource):
    class Meta:
        queryset = Department.objects.exclude(active=False)
        fields = ['department_name']
        #allowed_methods = ['post','get']
        include_resource_uri = False
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)

class AisleResource(ModelResource):
    class Meta:
        queryset = Aisle.objects.exclude(active=False)
        fields = ['aisle_name', 'store_aisle_number']
        #allowed_methods = ['post','get']
        include_resource_uri = False
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
                
class ProductResource(ModelResource):
    department = fields.ForeignKey(DepartmentResource, 'department', full=True)
    aisle = fields.ForeignKey(AisleResource, 'aisle', full=True)
     
    class Meta:
        queryset = Product.objects.all()
        allowed_methods = ['get'] 
        excludes = ['product_available', 'price_change', 'mark_up_category', 'large_image_url', 'package_unit_volume_in_milliliters', 
        'limited_time_offer_savings_in_cents', 'price_per_liter_in_cents', 'sugar_content', 'has_value_added_promotion', 'has_limited_time_offer', 
        'is_seasonal','is_vqa',  'is_kosher', 'sugar_in_grams_per_liter', 'clearance_sale_savings_in_cents', 'has_clearance_sale']
        
        include_resource_uri = False       
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
        
    def dehydrate(self, bundle): 
        bundle = super(ProductResource, self).dehydrate(bundle)
        assigned_driver = bundle.request.GET.get('d', '')        
        #print 'dehydrate product order item '+assigned_driver
        if(bundle.request.user.is_admin):
            #Do not hydrate
            return bundle            
        #elif bundle.request.user.is_driver and assigned_driver == 't':
        #TODO:Discuss with team
        #elif (assigned_driver == 't'):
        #    del bundle.data['price']
        #elif not (bundle.request.user.is_admin or assigned_driver == 't'):
        elif not (assigned_driver == 't'):
            del bundle.data['cost_price'] 
        return bundle

class OrderItemResource(ModelResource): 
    product = fields.ForeignKey(ProductResource, 'product',full=True)
    class Meta:
        queryset = OrderItem.objects.all()
        allowed_methods = ['get']
        resource_name = 'item'
        #excludes = ['is_check_out', 'session', 'creation_date', 'modification_date']
        include_resource_uri = True       
        authentication = SessionAuthentication()
        throttle = CacheThrottle(throttle_at=200)
    
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/change%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('change_item_status'), name="api_change_item_status"),
        ]
    
    #TODO:Discuss with team
    # def dehydrate(self, bundle): 
    #     bundle = super(OrderItemResource, self).dehydrate(bundle) 
    #     assigned_driver = bundle.request.GET.get('d', '')
    #     if bundle.request.user.is_driver and assigned_driver == 't': 
    #         del bundle.data['price']
    #         del bundle.data['tax']
    #     return bundle
          
    def change_item_status(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        item_id = data.get('item_id', '')
        status = data.get('status', '')
        item = OrderItem.objects.select_related().get(pk = item_id)
        if (request.user.is_admin) or (request.user == item.order.assigned_driver):
            if (item.status != 'CANCELED'): 
                item.update_status(status=status)
                item.save()
                return self.create_response(request, {
                    'success': True,
                    'message': 'Successfully changed the status of order item' 
                })
            else:
                return self.create_response(request, {
                    'success': False,
                    'message': 'You cannot change the status of a canceled product' 
                })
        else:
            return self.create_response(request, {
                'success': False,
                'message': 'You do not have the permission to perform this action' 
            })

'''
    Order APIs
'''
class OrderForm(forms.ModelForm):

    class Meta:
        model = Order
        exclude = ('user','assigned_driver',)

class UserNameResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        fields = ['id']
        excludes = ['password', 'phone', 'is_superuser', 'is_admin', 'is_driver']
        include_resource_uri = False
        allowed_methods = ['get']
        authentication = SessionAuthentication()
        
    def dehydrate(self, bundle):
        bundle.data['name'] = "%s %s" % (bundle.obj.first_name, bundle.obj.last_name)
        return bundle
        
class OrderAuthorization(Authorization):
    def authorize_user(self, bundle):
        print 'Authorize User'

    def read_list(self, object_list, bundle):
        if not (bundle.request.user.is_admin or bundle.request.user.is_driver):
            return object_list.filter(user=bundle.request.user)
        elif bundle.request.user.is_admin:
            delivery_date = bundle.request.GET.get('date', '')
            admin_user_order = bundle.request.GET.get('a', '')
            assigned_driver = bundle.request.GET.get('d', '')
            from_date = bundle.request.GET.get('from', '')
            to_date = bundle.request.GET.get('to', '')
            if(from_date and to_date):
                return object_list.filter(delivery_date__range=[from_date, to_date])               
            elif(delivery_date):
                _delivery_date = datetime.strptime(delivery_date , '%Y-%m-%d')
                #bundle.data['assigned_driver'] = bundle.obj.assigned_driver #TODO: CHECK IF THIS IS NEEDED?????
                return object_list.filter(delivery_date__exact=_delivery_date)
            elif(admin_user_order == 'f'):# If admin is the customer
                print 'admin is a customer '+bundle.request.user.first_name
                return object_list.filter(user=bundle.request.user)
            elif(assigned_driver == 't'):# If admin is the driver
                print 'admin is a driver '+bundle.request.user.first_name
                return object_list.filter(assigned_driver=bundle.request.user).exclude(status="CANCELED")
            else:# If admin
                return object_list.all()
        elif bundle.request.user.is_driver:
            assigned_driver = bundle.request.GET.get('d', '')
            if(assigned_driver == 't'):# If driver
                print 'drivar is '+bundle.request.user.first_name
                return object_list.filter(assigned_driver=bundle.request.user).exclude(status="CANCELED")
            else: # If driver is the customer
                print 'driver is a customer '+bundle.request.user.first_name
                return object_list.filter(user=bundle.request.user)
    
    def read_detail(self, object_list, bundle):        
        if(bundle.request.user.is_admin):  
            return True      
        else:
            return bundle.obj.user == bundle.request.user

    def delete_list(self, object_list, bundle):
        print 'WARN : Trying to hack into to sever - Order List'
        raise Unauthorized("Sorry, no deletes.")

    def delete_detail(self, object_list, bundle):
        print 'WARN : Trying to hack into to sever - Order Detail'
        raise Unauthorized("Sorry, no deletes.")
                           
class OrderResource(ModelResource):
    items = fields.ToManyField(OrderItemResource, 'items', full=True)
    store = fields.ForeignKey('store.api.SearchStoreResource', 'store', full=True) 
    shipping_address = fields.ForeignKey(ShippingAddressResource, 'shipping_address', full=True, null=True)
    assigned_driver = fields.ForeignKey(UserNameResource, 'assigned_driver', full=True, null=True)
    user = fields.ForeignKey(UserNameResource, 'user', full=True, null=True)
    #delivery_time = fields.ForeignKey('order.api.DevliveryTimeResource', 'delivery_time', full=True)
    
    class Meta:
        queryset = Order.objects.all()
        resource_name = 'order'
        include_resource_uri = False
        allowed_methods = ['get']
        authentication = SessionAuthentication()
        authorization = OrderAuthorization()
        throttle = CacheThrottle(throttle_at=200)
        limit = 0
        max_limit = 0
        validation = FormValidation(form_class=OrderForm)
        #excludes = ['tax_paid', 'price_paid']
        filtering = {
            "status": ['exact']
        }
        cache = SimpleCache(timeout=10)

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/create%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_order'), name="api_create_order"),
            url(r"^(?P<resource_name>%s)/update%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('modify_order_status'), name="api_modify_order_status"),
            url(r"^(?P<resource_name>%s)/status%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_order_status'), name="api_get_order_status"),
            url(r"^(?P<resource_name>%s)/phone%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('edit_phone_number'), name="api_edit_phone_number"),
            url(r"^(?P<resource_name>%s)/assign%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('assign_order_to_driver'), name="api_assign_order_to_driver"),
        ]
    
    def dehydrate(self, bundle): 
        bundle = super(OrderResource, self).dehydrate(bundle) 
        is_user_assigned_driver = bundle.request.GET.get('d', '')
        if bundle.request.user.is_driver and is_user_assigned_driver == 't': 
            del bundle.data['total_price']
            del bundle.data['tax']
        elif not bundle.request.user.is_driver and not bundle.request.user.is_admin: #TODO: handle the case of driver being the customer
            del bundle.data['tax_paid']
            del bundle.data['price_paid']
            del bundle.data['drivers_pay']
        return bundle
    
    #def hydrate(self, bundle):
        #bundle.data['profile'] = bundle.obj.profile
        #return bundle
    
    def create_order(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        store_id = data.get('store_id', '')
        cart_id = data.get('cart_id', '')
        delivery_date = data.get('delivery_date', '')#TODO
        from_delivery_time = data.get('from_delivery_time', '')
        to_delivery_time = data.get('to_delivery_time', '')
        shipping_address_id = data.get('shipping_address_id', '')
        tip = float(data.get('tip', ''))
        tip = Decimal(str(round(tip,2))).quantize(openshift.settings.TWO_PLACES)
        print "tip is "+str(tip)
        phone = data.get('phone', '')
        delivery_instructions = data.get('delivery_instructions', '')
        items = data.get('items', '')
        
        user = request.user 
        cart = Cart.objects.get(pk=cart_id, user=user)
        delivery_charge = Decimal(get_delivery_charge(cart_id, store_id))
        print "delivery charge " +str(delivery_charge)
        discount = Decimal(0.00)
        discount_tax_deduction = Decimal(0.00)
        if(cart.coupon and cart.coupon.type == 'free_delivery'):
            discount = delivery_charge 
            discount_tax_deduction = Decimal(round(delivery_charge * Decimal(openshift.settings.TAX_RATE),2)).quantize(openshift.settings.TWO_PLACES)
        else:
            if(cart.coupon):
                discount = cart.discount_per_store            
            if(delivery_charge <= 0):
                return self.create_response(request, {
                    'success': False,
                    'message' : "Invalid Delivery Charge"
                })

        if(tip < 0 or delivery_charge < 0): #TODO: Fix Duplicate check
            return self.create_response(request, {
                'success': False,
                'message' : "Invalid tip or delivery charge amount"
            })

        if len(items) >0 :
            try:
                payment = Payment.objects.get(cart__id=cart_id)
                if(payment):             
                    from_delivery_time = datetime.strptime(from_delivery_time,'%I:%M %p').time()
                    to_delivery_time = datetime.strptime(to_delivery_time,'%I:%M %p').time()
                  
                    order = Order.objects.create_order(user=request.user, delivery_date=datetime.strptime(delivery_date , '%Y-%m-%d'),
                                                        from_delivery_time=from_delivery_time, to_delivery_time=to_delivery_time, store_id=store_id, cart=cart,
                                                        shipping_address_id=shipping_address_id, tip=tip, delivery_charge=delivery_charge, phone=phone,
                                                        delivery_instructions=delivery_instructions,discount=discount, discount_tax_deduction=discount_tax_deduction)
                    for item in items:            
                        product_id = item['product']['id']
                        quantity = item['quantity']
                        note = item['note']
                        OrderItem.objects.add_order_item(order=order, product_id=product_id, quantity=quantity,note=note)
                        CartItem.objects.filter(pk=item['id']).delete()
                    
                    order.setTotalPrice()
                    order.setTax()
                    order.save()
            
                    items_left_in_cart = CartItem.objects.filter(cart__pk=cart_id).count()
                    if items_left_in_cart==0:
                        Cart.objects.filter(pk=cart_id).update(is_check_out=True)
                    
                    order.send_order_create_email()                
                    if(user.is_first_order):
                        user.is_first_order = False
                        user.save()
                        
                    return self.create_response(request, {
                        'success': True,
                        'message' : "Successfully created order"
                    })
            except ObjectDoesNotExist:
                return self.create_response(request, {
                    'success': False,
                    'message' : "Invalid payment or cart"
                }) 
        else:
            return self.create_response(request, {
                'success': False,
                'message' : "There are no items to create order"
            })
            
    def edit_phone_number(self, request, **kwargs):
        try:
            self.method_check(request, allowed=['patch'])
            self.is_authenticated(request)
            data = json.loads(request.body)
            phone = data.get('phone', '')
            if len(phone) < 10:
                return self.create_response(request, {
                    'success': False,
                    'message': 'Phone number should be at least 10 digits'
                })
            order_id = data.get('order_id', '')
            order = Order.objects.select_related().get(pk=order_id)
            if (request.user == order.user) or (request.user.is_admin):
                order.updatePhone(phone=phone)
                order.save()
            else:
                return self.create_response(request, {
                    'success': False,
                    'message': 'You don not have the priviledge to modify this phone number'
                })
        except Exception as ex:
            return self.create_response(request, {
                    'success': False,
                    'message': 'There was some problem while modifying your phone number, Please contact customer service. '+str(ex)
            })
        return self.create_response(request, {
                'success': True,
                'message': 'Your phone number was successfully modified'
        })
    
    def get_order_status(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        order_id = request.GET.get('order_id', '')
        order = Order.objects.get(pk=order_id)
        if order.user == request.user:
            return self.create_response(request, {
                    'success': True,
                    'order_status': order.status
            })
        else:
            return self.create_response(request, {
                    'success': False,
                    'message': 'You don not have the priviledge to access this information'
            })
            
    def modify_order_status(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        order_status = data.get('order_status', '')
        order_id = data.get('order_id', '')
        order = Order.objects.get(pk=order_id)
        if  order.user == request.user or order.assigned_driver == request.user or request.user.is_admin:
            if order.status == 'DELIVERED':
                return self.create_response(request, {
                        'success': False,
                        'message': 'You cannot change the status of an order which is already delivered'
                    })
            elif order_status == 'CANCELED':
                if  order.user == request.user or request.user.is_admin:
                    if order.status == 'PENDING':
                        paymentResource = PaymentResource();
                        refund_amount = order.total_price + order.tax + order.tip + order.delivery_charge - order.discount - order.discount_tax_deduction
                        print "refund_amount " + str(refund_amount)
                        refunded, instance = paymentResource.process_refund(request=request, cart=order.cart, refund_amount=int(refund_amount*100))
        
                        if refunded:
                            order.status=order_status
                            order.save()
                            order.send_order_cancel_email()
                            return self.create_response(request, {
                                'success': True,
                                'message': 'Sucessfully cancelled your order',
                                'order_status': order_status
                            })
                        else:
                            return self.create_response(request, {
                            'success': False,
                            'message': instance.message,
                            'order_status': order.status
                        })
                    else:
                        return self.create_response(request, {
                            'success': False,
                            'message': 'You can only cancel order before the order is assigned to shopper',
                            'order_status': order.status
                        })
                else:
                    return self.create_response(request, {
                        'success': False,
                        'message': 'Access denied'
                    })
            else:
                print 'new order_status '+order_status
                order.status = order_status
                order.save()
                
                #if order_status == 'DELIVERED':
                #    order.send_order_delivery_email()                

                return self.create_response(request, {
                    'success': True,
                    'message': 'Sucessfully changed your order status to '+order_status,
                    'order_status': order_status
                })
        else:
            return self.create_response(request, {
                'success': False,
                'message': 'You do not have the permission make any changes to this order'
            })
        
    def assign_order_to_driver(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        order_id = data.get('order_id', '')
        driver_id = data.get('driver_id', '')
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Only Admin has assign order to driver'
            })
        else:
            driver = User.objects.get(pk=driver_id)
            if driver.is_driver:
                print 'Assigning to driver'
                order = Order.objects.get(pk=order_id)
                if(order.status != "CANCELED" and order.status != "DELIVERED"):
                    order.assign_driver(driver=driver, status='ASSIGNED TO SHOPPER')
                    order.save()
                    order.send_order_notification_email_to_driver()
                    return self.create_response(request, {
                        'success': True,
                        'message': 'Order successfully assigned to driver'
                    })
                else:
                    return self.create_response(request, {
                        'success': False,
                        'message': 'Order is cancelled or already delivered, cannot assign to driver'
                    })
            else:
                return self.create_response(request, {
                    'success': False,
                    'message': 'User assigned to is not a driver'
                })

    # def get_last_order(self, request, **kwargs):
    #     self.method_check(request, allowed=['get'])
    #     self.is_authenticated(request)
    #     order = Order.objects.get(user=request.user)
    #     if order.user == request.user:
    #         return self.create_response(request, {
    #                 'success': True,
    #                 'order_status': order.status
    #         })
    #     else:
    #         return self.create_response(request, {
    #                 'success': False,
    #                 'message': 'You don not have the priviledge to access this information'
    #         })

'''
    Postal Code APIs
'''            
class PostalCodeResource(ModelResource):
    class Meta:
        queryset = PostalCodeValidator.objects.all()
        resource_name = 'postalcode'
        include_resource_uri = True
        allowed_methods = ['get']
        authentication = SessionAuthentication()
        
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/add%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('add_postal_code'), name="api_add_postal_code"),
            url(r"^(?P<resource_name>%s)/check%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('validate_postal_code'), name="api_validate_postal_code"),
        ]
          
    def add_postal_code(self, request, **kwargs):
        self.method_check(request, allowed=['post'])   
        self.is_authenticated(request)#TODO: Check?
        if not request.user.is_admin:
            return self.create_response(request, {
                'success': False,
                'message': 'Sorry! Only admin can add postal code.'
            })        
        data = self.deserialize(request, request.body, format=request.META.get('CONTENT_TYPE', 'application/json'))
        postal_code = data.get('postal_code', '')
        area_name = data.get('area_name', '')
        if len(postal_code) < 3:
            return self.create_response(request, {
                'success': False,
                'message': 'Sorry! Postal code should be at-least of 3 characters.'
            })
        pc = postal_code.strip().lower()[0:3]
        PostalCodeValidator.objects.create(postal_code=pc,area_name=area_name)
        return self.create_response(request, {
            'success': True
        })
        
    def validate_postal_code(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        postal_code = request.GET.get('postal_code', '').replace(" ", "")
        pc = postal_code.strip().lower()[0:3]
        try:
            PostalCodeValidator.objects.get(postal_code=pc)
        except PostalCodeValidator.DoesNotExist:
            return self.create_response(request, {
                'success': False,
                'message': 'We are not in ' + postal_code + ' yet. Send us an email to get notified when we start delivering to your area.'
            })
        return self.create_response(request, {
                'success': True
            })

'''
    Item Refund APIs
'''
class RefundItemAuthorization(Authorization):
    def read_list(self, object_list, bundle):
        print 'Read List RefundItemAuthorization'        
        if bundle.request.user.is_admin:
            return object_list.all()
        else:
            raise Unauthorized("Only admin user can access this.")
  
class RefundProductResource(ModelResource):
     
    class Meta:
        queryset = Product.objects.all()
        allowed_methods = ['get'] 
        excludes = ['id','taxable', 'search_tags', 'product_available', 'price_change', 'position_on_aisle', 'cost_price', 'description']
        include_resource_uri = False       
        authentication = SessionAuthentication()
        #throttle = CacheThrottle(throttle_at=200)
        
class OrderItemRefundResource(ModelResource):            
    product = fields.ForeignKey(RefundProductResource, 'product', full=True)
    
    class Meta:
        queryset = OrderItem.objects.filter(status='REFUND').exclude(order__status='CANCELED')
        resource_name = 'refund'
        include_resource_uri = False
        allowed_methods = ['get']
        limit = 0
        max_limit = 0
        authentication = SessionAuthentication()
        authorization = RefundItemAuthorization()
    
    def prepend_urls(self):
        return [
            url(r"^admin/(?P<resource_name>%s)/items%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('refund_items'), name="api_refund_items"),
        ]
    
    def dehydrate(self, bundle): 
        bundle = super(OrderItemRefundResource, self).dehydrate(bundle)
        if bundle.request.user.is_admin: 
            bundle.data['store'] = bundle.obj.order.store.name
            bundle.data['order'] = bundle.obj.order.order_number
            bundle.data['cutomer_id'] = bundle.obj.order.user.pk
            bundle.data['cutomer_name'] = bundle.obj.order.user
            bundle.data['assigned_driver'] = bundle.obj.order.assigned_driver
            bundle.data['cart_id'] = bundle.obj.order.cart.id
            return bundle
        
    def refund_items(self, request, **kwargs):        
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        cart_id = data.get('cart_id', '')
        if request.user.is_admin:
            items = OrderItem.objects.filter(order__cart__id=cart_id, status='REFUND')
            total_price = items.aggregate(Sum('price'))
            total_tax = items.aggregate(Sum('tax'))
            gross_amount_to_refund = total_price['price__sum'] + total_tax['tax__sum']
            payment = Payment.objects.get(cart__id=cart_id)
            success, instance = payment.refund(charge_id=payment.charge_id, refund_amount=int(gross_amount_to_refund*100))
            if success:
                items.update(status='CANCELED')
                message = 'Successfully refunded'
            else:
                message = instance.message
            return self.create_response(request, {
                'success': success,
                'message': message
            })
        else:
            raise Unauthorized("Only admin user can access this.")

class ManagementResource(ModelResource):
    store = fields.ForeignKey('store.api.SearchStoreResource', 'store', full=True) 
    #shipping_address = fields.ForeignKey(ShippingAddressResource, 'shipping_address', full=True, null=True)
    #assigned_driver = fields.ForeignKey(UserNameResource, 'assigned_driver', full=True, null=True)
    
    class Meta:
        queryset = Order.objects.all()
        resource_name = 'management/order'
        include_resource_uri = False
        allowed_methods = ['patch']
        authentication = SessionAuthentication()
        #authorization = AdminAuthorization()
        throttle = CacheThrottle(throttle_at=200)
        limit = 0
        max_limit = 0
        filtering = {
           "delivery_date": ['gte', 'lte'],
           "store":ALL_WITH_RELATIONS,
        }

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/expense%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('set_expense'), name="api_set_expense"),
        ]

    def set_expense(self, request, **kwargs):        
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        order_id = data.get('order_id', '')
        tax_paid = Decimal(data.get('tax_paid', ''))
        price_paid = Decimal(data.get('price_paid', ''))

        if request.user.is_admin or request.user.is_driver:
            try:
                if price_paid < 0:
                    return self.create_response(request, {
                        'success': False,
                        'message': 'Price paid cannot be less than zero.'
                    }) 
                order = Order.objects.get(id=order_id)
                order.set_expense(tax_paid, price_paid)
                #order.save()
                return self.create_response(request, {
                    'success': True,
                    'drivers_pay': order.drivers_pay,
                    'message': 'Successfully updated the expense'
                })
            except ObjectDoesNotExist, MultipleObjectsReturned:
                return self.create_response(request, {
                    'success': False,
                    'message': 'Selected order is either NOT VALID or the status is NOT DELIVERED'
                })
        else:
            raise Unauthorized("Only admin/driver user can access this.")

def get_delivery_charge(cart_id, store_id):
    delivery_slot = DeliveryCharge.objects.select_related('store').get(cart__id=cart_id, store__id=store_id)
    if(delivery_slot.is_one_hour):
        delivery_charge = Decimal(delivery_slot.store.one_hour_delivery_fee)
    elif(delivery_slot.is_two_hour):
        delivery_charge = Decimal(delivery_slot.store.two_hour_delivery_fee)
    elif(delivery_slot.is_three_hour):
        delivery_charge = Decimal(delivery_slot.store.three_hour_delivery_fee)
    return delivery_charge
        