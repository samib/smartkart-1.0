from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
#from django.contrib import admin
#admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'openshift.views.login', name='login'),
	url(r'^', include('usermanagement.urls')),
    url(r'^', include('store.urls')),
    url(r'^', include('cart.urls')),
    url(r'^', include('product.urls')),
    url(r'^', include('order.urls')),
    url(r'^', include('payment.urls')),
    url(r'^', include('coupon.urls')),
    # Examples:
    #url(r'^$', 'openshift.views.login', name='login'),
    # url(r'^openshift/', include('openshift.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^admin/', include(admin.site.urls)),
)
