'''
Created on Dec 23, 2013

@author: Hisham
'''
import json
from decimal import Decimal

from django.http import HttpResponse
from django.conf.urls import url

from tastypie.authentication import SessionAuthentication
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

import openshift.settings
from payment.models import Payment
from cart.models import Cart, DeliveryCharge

def get_delivery_charge(delivery_charges):
        total_delivery_charge = Decimal(0.00)
        for charge in delivery_charges:
            if(charge.is_one_hour):
                total_delivery_charge += Decimal(charge.store.one_hour_delivery_fee)
            elif(charge.is_two_hour):
                total_delivery_charge += Decimal(charge.store.two_hour_delivery_fee)
            elif(charge.is_three_hour):
                total_delivery_charge += Decimal(charge.store.three_hour_delivery_fee)
        return total_delivery_charge


class PaymentResource(ModelResource):
    class Meta:
        queryset = Payment.objects.all()
        resource_name = 'payment'
        allowed_methods = ['post']
        authentication = SessionAuthentication()
        
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/pay%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('process_payment'), name="api_process_payment"),
            
            url(r"^(?P<resource_name>%s)/cancel%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('process_refund'), name="api_process_refund"),
            
            url(r"^(?P<resource_name>%s)/cards%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_payment_info'), name="api_get_payment_info"),
             
             url(r"^(?P<resource_name>%s)/card/pay%s$" %
                 (self._meta.resource_name, trailing_slash()),
                 self.wrap_view('charge_card'), name="api_charge_card"),
            
            url(r"^(?P<resource_name>%s)/card/delete%s$" %
                 (self._meta.resource_name, trailing_slash()),
                 self.wrap_view('delete_card'), name="api_delete_card"),
        ]        
    
    def process_payment(self, request, **kwargs):
        self.is_authenticated(request)
        self.method_check(request, allowed=['post'])
        data = json.loads(request.body)
        tip = float(data.get('tip', ''))
        tip = Decimal(str(round(tip,2))).quantize(openshift.settings.TWO_PLACES)
        cart_id = data.get('cart_id', '')
        token = data.get('token', '')
        save_card = data.get('save_card', '')
        random_hash = data.get('random_hash', '')

        tip = Decimal(tip)
        if(tip<0):
            return self.create_response(request, {
                    'success': False,
                    'message': 'Invalid tip, Please try again'
            })        

        if not cart_id:
            return self.create_response(request, {
                    'success': False,
                    'message': 'payment must have a cart'
            })

        try:    
            cart = Cart.objects.select_related('coupon').get(pk=cart_id, user=request.user)
        except ObjectDoesNotExist:
            return self.create_response(request, {
                        'success': False,
                        'message': 'Invalid Cart'
                })

        if(cart.delivery_charge == 0.00):
            delivery_charges_for_cart = DeliveryCharge.objects.filter(cart__id=cart_id, cart__user=request.user).prefetch_related('store')
            if delivery_charges_for_cart.exists():
                total_delivery_charge = get_delivery_charge(delivery_charges_for_cart)
                cart.delivery_charge = total_delivery_charge
                cart.save()
            else:
                return self.create_response(request, {
                        'success': False,
                        'message': 'Invalid Cart'
                })
        
        if(cart.delivery_charge<0):
            return self.create_response(request, {
                    'success': False,
                    'message': 'Invalid delivery fee, Please try again'
            })

        if cart.coupon:
            if(cart.coupon.type != 'free_delivery' and cart.coupon.type != 'multiple_monetary' and cart.coupon.type != 'multiple_percentage' and (cart.coupon.random_hash != random_hash)):
                return self.create_response(request, {
                        'success': False,
                        'message': 'Invalid security code for discount'
                })
        print "charing cc"
        payment = Payment()
        success, instance = payment.charge(tip, token, cart, save_card, request.user)
 
        if not success:
            return self.create_response(request, {
                'success': success,
                'message': instance
            })
        else:
            payment.cart_id = cart_id#Cart.objects.get(pk=cart_id)
            payment.save()
            
            # if cart.coupon:
            #     cart.coupon.is_checked_out = True
            #     cart.coupon.save()

            return self.create_response(request, {
                    'success': success,
                    'message': 'successfully charged card'
            })
        
    def process_refund(self, request, cart, refund_amount):
        self.is_authenticated(request)
        try:
            payment = Payment.objects.get(cart=cart)
        except ObjectDoesNotExist, MultipleObjectsReturned:
            return self.create_response(request, {
                'success': success,
                'message': 'Something went wrong in accessing your payment info. Please contact customer care.'
            })
        success, instance = payment.refund(payment.charge_id, refund_amount)
        if success:
            payment.refund_status = True
            payment.save()
        return success, instance
    
    def get_payment_info(self, request, **kwargs):
        self.is_authenticated(request)
        self.method_check(request, allowed=['get'])
        payment = Payment()
        success, instance = payment.get_cards(request.user)
        if success:
            data = json.dumps(instance)#serializers.serialize('json', instance)
            return HttpResponse(data, mimetype='application/json')
        else:
            if instance == None:
                message = "The user do not have any saved cards"
            else:
                message = instance.message
            return self.create_response(request, {
                'success': success,
                'error': message
            })
    
    def charge_card(self, request, **kwargs):
        self.is_authenticated(request)
        self.method_check(request, allowed=['post'])
        payment = Payment()
        data = json.loads(request.body)
        tip = float(data.get('tip', ''))
        tip = Decimal(str(round(tip,2))).quantize(openshift.settings.TWO_PLACES)
        cart_id = data.get('cart_id', '')
        card_id = data.get('card_id', '')
        random_hash = data.get('random_hash', '')
        tip = Decimal(tip)
        if(tip<0):
            return self.create_response(request, {
                    'success': False,
                    'message': 'Invalid tip, Please try again'
            })        
        
        if not cart_id:
            return self.create_response(request, {
                    'success': False,
                    'message': 'charge_card must have a cart'
            })
            raise ValueError()
        if not card_id:
            return self.create_response(request, {
                    'success': False,
                    'message': 'charge_card must have a card'
            })
            raise ValueError()

        cart = Cart.objects.select_related().get(pk=cart_id, user=request.user)

        if(cart.delivery_charge == 0.00):
            delivery_charges_for_cart = DeliveryCharge.objects.filter(cart__id=cart_id, cart__user=request.user).prefetch_related('store')
            if delivery_charges_for_cart.exists():
                total_delivery_charge = get_delivery_charge(delivery_charges_for_cart)
                cart.delivery_charge = total_delivery_charge
                cart.save()
            else:
                return self.create_response(request, {
                        'success': False,
                        'message': 'Invalid Cart'
                })
        
        if(cart.delivery_charge<0):
            return self.create_response(request, {
                    'success': False,
                    'message': 'Invalid delivery fee, Please try again'
            })

        if cart.coupon:
            if(cart.coupon.type != 'free_delivery' and cart.coupon.type != 'multiple_monetary' and cart.coupon.type != 'multiple_percentage' and (cart.coupon.random_hash != random_hash)):
                return self.create_response(request, {
                        'success': False,
                        'message': 'Invalid security code for discount'
                })
        print "charing saved cc"
        success, instance = payment.charge_card(tip, cart, card_id, request.user)
 
        if not success:
            return self.create_response(request, {
                    'success': success,
                    'message': instance
            })
        else:
            payment.cart_id = cart_id#Cart.objects.get(pk=cart_id)
            payment.save()
            return self.create_response(request, {
                    'success': success,
                    'message': 'successfully charged card'
            })
      
    def delete_card(self, request, **kwargs):
        self.is_authenticated(request)
        self.method_check(request, allowed=['post'])
        data = json.loads(request.body)
        card_id = data.get('card_id', '')
        payment = Payment()
        success, instance = payment.remove_card(card_id,request.user)
        if success:
            return self.create_response(request, {
                'success': instance.deleted,
                'error': 'Successfully deleted the card'
            })
        else:
            return self.create_response(request, {
                'success': success,
                'error': 'Error while deleting the card'
            })
