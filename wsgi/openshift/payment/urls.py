from django.conf.urls import patterns, url, include
from payment.api import PaymentResource

product_resource = PaymentResource()

urlpatterns = patterns('',
    url(r'^api/', include(product_resource.urls)),
);


