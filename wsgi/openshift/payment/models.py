from decimal import Decimal, ROUND_UP

from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum

import openshift.settings
from cart.models import Cart, CartItem
from coupon.models import Coupon
from usermanagement.models import User
from usermanagement.utils import Error

class Payment(models.Model):
    def __init__(self, *args, **kwargs):
        super(Payment, self).__init__(*args, **kwargs)
 
        import stripe
        stripe.api_key = openshift.settings.STRIPE_API_KEY
        print stripe.api_key
        self.stripe = stripe
 
    charge_id = models.CharField(max_length=50)
    cart = models.ForeignKey(Cart)
    refund_status = models.BooleanField(default = False)
 
    def charge(self, tip, token, cart, save_card, user):
        """
        Takes a the price and credit card details: number, exp_month,
        exp_year, cvc.
 
        Returns a tuple: (Boolean, Class) where the boolean is if
        the charge was successful, and the class is response (or error)
        instance.
        """
 
        if self.charge_id:
            return False, Exception(message="Already charged.")
 
        try:
            Payment.objects.get(cart=cart)
            return False, Exception(message="Payment already processed for this cart.")
        except ObjectDoesNotExist:
            items = CartItem.objects.filter(cart=cart, cart__user__exact=user)
            if len(items) > 0:           
                net_price = items.aggregate(Sum('price'))['price__sum']
                tax = items.aggregate(Sum('tax'))['tax__sum']
                delivery_charge = cart.delivery_charge
                print 'tip '+str(tip)
                print 'delivery_charge '+str(delivery_charge)
                delivery_tax = Decimal(round(cart.delivery_charge * Decimal(openshift.settings.TAX_RATE),2)).quantize(openshift.settings.TWO_PLACES)
                print 'delivery_tax '+str(delivery_tax)
                delivery_charge += delivery_tax
                print 'total_delivery_charge '+str(delivery_charge)
                chargable_amount = net_price+tax+tip+delivery_charge     
                print 'total_chargable_amount '+str(chargable_amount)
                chargable_amount = Decimal(round(chargable_amount,2)).quantize(openshift.settings.TWO_PLACES)
                print 'CA ' + str(chargable_amount)
                if cart.coupon:
                    if(cart.coupon.type == 'free_delivery'):
                        discount = delivery_charge 
                    else:
                        discount = cart.dollar_amount
                    gross_amount_in_cents = int(chargable_amount*100) - int(discount*100)                                   
                else:
                    gross_amount_in_cents = int(chargable_amount*100)
                print "amount charging " + str(gross_amount_in_cents)
                if True:
                    try:
                        customer = PaymentInfo.objects.get(user=user)
                        _customer = self.stripe.Customer.retrieve(customer.customer_id)
                        try:
                            _card = _customer.cards.create(card=token)
                        except self.stripe.error.CardError, ce:
                            return False, ce   
                    except ObjectDoesNotExist:
                        _response = self.stripe.Customer.create(
                            description='Smartkart.ca',
                            card=token)
                        customer = PaymentInfo.objects.create(user=user, customer_id=_response.id)  
                    customer_id = customer.customer_id     
                    try:           
                        response = self.stripe.Charge.create(
                            amount=gross_amount_in_cents,
                            currency="cad",
                            customer=customer_id,
                            description='Smartkart.ca'
                        )
                    except self.stripe.error.CardError, ce:
                        return False, ce                
                else:
                    print "hello"
                    print self.stripe.api_key
                    try:
                        response = self.stripe.Charge.create(
                            amount = gross_amount_in_cents,
                            currency = "cad",
                            source = token,
                            description='Smartkart.ca')
                    except self.stripe.error.CardError, ce:
                        return False, ce
                self.charge_id = response.id
            else:
                err = Error()
                err.set_message("There are no items to create order.")
                return False, err
        except self.stripe.error.CardError, ce:
                # charge failed
                return False, ce
        if cart.coupon and (cart.coupon.type != 'free_delivery' or cart.coupon.type != 'multiple'):
            cart.coupon.is_checked_out = True#Coupon.objects.get(cart_id).update(is_checked_out=True)
        cart.save()
        return True, response
    
    def refund(self, charge_id, refund_amount):
        try:
            print "refund_amount "+str(refund_amount)
            ch = self.stripe.Charge.retrieve(charge_id)
            ch.refund(amount=refund_amount)
        except self.stripe.CardError, ce:
            #print ce.message
            return False, ce
        return True, ch
    
    def charge_card(self, tip, cart, card_id, user):
        if self.charge_id:
            return False, Exception(message="Already charged.")
        try:
            Payment.objects.get(cart=cart)
            return False, Exception(message="Payment already processed for this cart. Please contact the customer service.")#TODO: CHANGE TO ERROR
        except ObjectDoesNotExist:                 
            _customer = PaymentInfo.objects.get(user=user)
            customer = self.stripe.Customer.retrieve(_customer.customer_id)
            customer.default_card = card_id
            customer.save()
            items = CartItem.objects.filter(cart=cart, cart__user__exact=user)
            if len(items) > 0:
                net_price = items.aggregate(Sum('price'))['price__sum']
                tax = items.aggregate(Sum('tax'))['tax__sum']
                delivery_charge = cart.delivery_charge
                print 'tip '+str(tip)
                print 'delivery_charge '+str(delivery_charge)
                delivery_tax = Decimal(round(cart.delivery_charge * Decimal(openshift.settings.TAX_RATE),2)).quantize(openshift.settings.TWO_PLACES)
                print 'delivery_tax '+str(delivery_tax)
                delivery_charge += delivery_tax
                print 'total_delivery_charge '+str(delivery_charge)
                chargable_amount = net_price+tax+tip+delivery_charge     
                print 'total_chargable_amount '+str(chargable_amount)
                chargable_amount = Decimal(round(chargable_amount,2)).quantize(openshift.settings.TWO_PLACES)
                if cart.coupon:
                    if(cart.coupon.type == 'free_delivery'):
                        discount = delivery_charge 
                    else:
                        discount = cart.dollar_amount
                    gross_amount_in_cents = int(chargable_amount*100) - int(discount*100)
                else:             
                    gross_amount_in_cents = int(chargable_amount*100)
                print "amount charging " + str(gross_amount_in_cents)
                try:
                    response = self.stripe.Charge.create(
                            amount = gross_amount_in_cents,
                            currency = "cad",
                            customer=customer.id,
                            description='Smartkart.ca')
                except self.stripe.error.CardError, ce:
                        return False, ce
                self.charge_id = response.id
            else:
                err = Error()
                err.set_message("There are no items to create order.")
                return False, err
        except self.stripe.error.CardError, ce: #TODO: CardError depreciated 
            # charge failed
            return False, ce
        if cart.coupon and (cart.coupon.type != 'free_delivery' or cart.coupon.type != 'multiple'):
            cart.coupon.is_checked_out=True
        cart.save() 
        return True, response
    
    def get_cards(self, user):
        try:
            customer = PaymentInfo.objects.get(user=user)
            cards = self.stripe.Customer.retrieve(customer.customer_id).cards.all()
        except PaymentInfo.DoesNotExist:
            return False, None
        except self.stripe.CardError, ce:
            # charge failed
            return False, ce
        return True, cards
    
    def remove_card(self, card_id, user):
        try:
            customer = PaymentInfo.objects.get(user=user)
            _customer = self.stripe.Customer.retrieve(customer.customer_id)
            deleted_card = _customer.cards.retrieve(card_id).delete()
        except PaymentInfo.DoesNotExist:
            return False, None
        except self.stripe.InvalidRequestError, e:
            return False, e
        return True, deleted_card
    
# def get_discount_amount(random_code, cart_id):
#     try:
#         coupon = Coupon.objects.get(cart_id = cart_id, random_code = random_code)
#     except ObjectDoesNotExist:
#         print 'No coupon fount for '+cart_id + 'and security code: '+ random_code
#         return 0
#     return coupon.dollar_amount
    
class PaymentInfo(models.Model):
    user = models.ForeignKey(User)
    customer_id = models.CharField(max_length=50)