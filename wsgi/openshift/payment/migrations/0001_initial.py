# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Payment'
        db.create_table(u'payment_payment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('charge_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('cart', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cart.Cart'])),
            ('refund_status', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'payment', ['Payment'])

        # Adding model 'PaymentInfo'
        db.create_table(u'payment_paymentinfo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['usermanagement.User'])),
            ('customer_id', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'payment', ['PaymentInfo'])


    def backwards(self, orm):
        # Deleting model 'Payment'
        db.delete_table(u'payment_payment')

        # Deleting model 'PaymentInfo'
        db.delete_table(u'payment_paymentinfo')


    models = {
        u'cart.cart': {
            'Meta': {'object_name': 'Cart'},
            'coupon': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['coupon.Coupon']", 'null': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'delivery_charge': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '5', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_check_out': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modification_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usermanagement.User']"})
        },
        u'coupon.coupon': {
            'Meta': {'object_name': 'Coupon'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'created_by'", 'null': 'True', 'to': u"orm['usermanagement.User']"}),
            'discount_per_store': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'dollar_amount': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'expiry_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_checked_out': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'random_hash': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'redeemed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'redeemed_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'redeemed_by'", 'null': 'True', 'to': u"orm['usermanagement.User']"}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        },
        u'payment.payment': {
            'Meta': {'object_name': 'Payment'},
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cart.Cart']"}),
            'charge_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'refund_status': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'payment.paymentinfo': {
            'Meta': {'object_name': 'PaymentInfo'},
            'customer_id': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['usermanagement.User']"})
        },
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'delivery_end_time': ('django.db.models.fields.TimeField', [], {}),
            'delivery_start_time': ('django.db.models.fields.TimeField', [], {}),
            'has_alcohol': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_free_delivery_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'min_order_for_free_delivery': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'minimum_order': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'one_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'phone': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'store_logo_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'three_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'three_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'time_zone': ('django.db.models.fields.CharField', [], {'default': "'America/Toronto'", 'max_length': '50'}),
            'two_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'two_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        },
        u'usermanagement.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'email_subscription': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'expiry': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 12, 13, 0, 0)'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'forgot_password_hash': ('django.db.models.fields.CharField', [], {'default': "'2eEXhNDqDazazpedQDUjdKqRuZgPcgn9UX7KPbUU'", 'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_driver': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_first_order': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'lastOrderID': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'last_store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']", 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        }
    }

    complete_apps = ['payment']