'''
Created on Nov 10, 2013

@author: Hisham
'''
from django.conf.urls import patterns, url, include
from cart.api import CartResource, CartItemResource, DeliveryChargeResource

cart_resource = CartResource()
cart_item_resource = CartItemResource()
delivery_charge_resource = DeliveryChargeResource()

urlpatterns = patterns('',
    url(r'^api/', include(cart_resource.urls)),
    url(r'^api/', include(cart_item_resource.urls)),
    url(r'^api/', include(delivery_charge_resource.urls)),
);