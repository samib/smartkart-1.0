from decimal import Decimal

from django.db import models
from django.utils.translation import ugettext_lazy as _

import openshift.settings

from coupon.models import Coupon
from product.models import Product
from store.models import Store
from usermanagement.models import User

class CartItemManager(models.Manager):
    def add_to_cart(self, cart, product_id, quantity, note):
        if not cart:
            raise ValueError('CartItem must have a cart')
        
        if not product_id:
            raise ValueError('CartItem must have a product')
        
        if not quantity:
            raise ValueError('CartItem must have a quantity')
        
        try:
            cart_item = self.create(cart = cart, product_id = product_id, quantity=quantity, note=note)
            cart_item.set_price()
            cart_item.set_tax()
            cart_item.save()
        except Exception as e:
            return None
        return cart_item
    
    def update_cart(self):
        pass

class Cart(models.Model):
    user = models.ForeignKey(User)
    creation_date = models.DateTimeField(auto_now_add=True)
    modification_date = models.DateTimeField(auto_now=True, auto_now_add=True)
    is_check_out = models.BooleanField(default=False)
    coupon = models.ForeignKey(Coupon, blank=True, null=True)
    delivery_charge = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    dollar_amount = models.DecimalField(_("Dollar Amount"), max_digits=6, decimal_places=2, blank=True, null=True)
    discount_per_store = models.DecimalField(_("Discount Per Store"), max_digits=6, decimal_places=2, blank=True, null=True)
    
class CartItem(models.Model):
    objects = CartItemManager()
    cart = models.ForeignKey(Cart)
    product = models.ForeignKey(Product)
    quantity = models.PositiveSmallIntegerField(default=1)
    creation_date = models.DateTimeField(auto_now_add=True)
    modification_date = models.DateTimeField(auto_now=True, auto_now_add=True)
    price = models.DecimalField(max_digits=6, decimal_places=2, default=0.00)
    tax = models.DecimalField(max_digits=6, decimal_places=2, default=0.00)
    note = models.TextField(blank=True, null=True)
    
    def set_tax(self):
        if self.product.taxable:
            self.tax = Decimal(self.product.price) * Decimal(self.quantity) * Decimal(openshift.settings.TAX_RATE)
            
    def set_price(self):
        self.price = Decimal(self.product.price) * Decimal(self.quantity)
    
    def get_price(self):
        return self.price
    
    def get_tax(self):
        return self.tax
    
    def update_quantity(self, quantity):
        self.quantity = quantity

class DeliveryChargeManager(models.Manager):
    def pick_delivery_slot(self, cart, store_id, is_one_hour, is_two_hour, is_three_hour):
        if not cart:
            raise ValueError('DeliveryCharge must have a cart')
        
        if not store_id:
            raise ValueError('DeliveryCharge must have a store_id')
        
        try:
            delivery_charge = self.create(cart=cart, store_id=store_id, is_one_hour=is_one_hour, is_two_hour=is_two_hour, is_three_hour=is_three_hour)
        except Exception as e:
            return None
        return delivery_charge

class DeliveryCharge(models.Model):
    objects = DeliveryChargeManager()
    cart = models.ForeignKey(to=Cart, related_name="delivery_slot", null=True, blank=True)
    store = models.ForeignKey(Store, null=True, blank=True)
    is_one_hour = models.BooleanField(default=False)
    is_two_hour = models.BooleanField(default=False)
    is_three_hour = models.BooleanField(default=False)
