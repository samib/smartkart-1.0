'''
Created on Nov 10, 2013

@author: Hisham
'''
import json

from decimal import Decimal

from django.conf.urls import url
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers
from django.http import HttpResponse

from tastypie import fields
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import Authorization
from tastypie.exceptions import Unauthorized, BadRequest
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash
from tastypie.validation import Validation

from cart.models import Cart, CartItem, DeliveryCharge
from store.api import SearchProductResource
from usermanagement.models import User

'''
    CART APIs
'''

class CartResource(ModelResource):
    class Meta:
        queryset = Cart.objects.all()
        resource_name = 'cart'
        excludes = ['creation_date', 'session']
        validation = Validation()
        #allowed_methods = ['post']
        authentication = SessionAuthentication()
        
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/create%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('create_cart'), name="api_add_cart"),
            url(r"^(?P<resource_name>%s)/show%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_user_cart'), name="api_get_user_cart"),
        ]
            
    def create_cart(self, request, **kwargs):
        self.is_authenticated(request)
        self.method_check(request, allowed=['post'])
        new_cart = Cart.objects.create(user=request.user)
        data = serializers.serialize('json', [new_cart])
        return HttpResponse(data, mimetype='application/json')

    def get_user_cart(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        try : 
            cart = Cart.objects.get(user__exact=request.user, is_check_out=False)
        except ObjectDoesNotExist:
            cart = Cart.objects.create(user=request.user)
            data = [ { 'cart_id':cart.id, 'items':[]} ]
            data_string = json.dumps(data, cls=DjangoJSONEncoder)
            return HttpResponse(data_string, mimetype='application/json')
        _items = CartItem.objects.select_related('product').filter(cart=cart)
        items = []    
        for item in _items:
            items.append({
                            "product": {
                                        "id": item.product.id,
                                        "name": item.product.name,
                                        "image_url": item.product.image_url,
                                        "store_id" : item.product.store_id,
                                        "price" : item.product.price,
                                        "description" : item.product.description,
                                        "brand_name" : item.product.brand_name,
                                        "taxable" : item.product.taxable,
                                        "size" : item.product.size
                                    },
                            'price': item.price,
                            'tax': item.tax,
                            'cart':item.cart.id,
                            'note': item.note,
                            'id':item.id,
                            'quantity':item.quantity                          
                        })
        if cart.coupon:
            data = [ { 'cart_id':cart.id, 'items':items, 'coupon':{"id": cart.coupon.id, "random_hash" : cart.coupon.random_hash, "dollar_amount" : cart.dollar_amount, "code" :cart.coupon.code}} ]
        else:
            data = [ { 'cart_id':cart.id, 'items':items, 'coupon':None} ]
        data_string = json.dumps(data, cls=DjangoJSONEncoder)
        return HttpResponse(data_string, mimetype='application/json')
    
class CartItemAuthorization(Authorization):
    # Checks that the records' owner is either None or the logged in user
    def authorize_user(self, bundle):
        print 'Authorize User'

        #if bundle.request.user.is_superuser:
            #return True
        #if bundle.request.user == bundle.obj.user:
            #return True

        #return False

    def user(self, bundle):
        if not hasattr(bundle.request, 'user'):
            print 'No user in cart item authorization'
            return None
        return User.objects.get(pk=bundle.request.user.id)

    def read_list(self, object_list, bundle):
        print 'Read List'
        return object_list.filter(cart__user = bundle.request.user, cart__id = bundle.request.GET.get('cart_id'))
        #return object_list.filter(cart__user = bundle.request.user, cart__is_check_out = False)
    
    def read_detail(self, object_list, bundle):
        print 'Read Detail'
        raise BadRequest('Request not allowed');
        #return self.authorize_user(bundle)

    def create_list(self, object_list, bundle):
        print 'Create List'
        raise BadRequest('Request not allowed');
        #return object_list

    def create_detail(self, object_list, bundle):
        print 'Create Detail'
        raise BadRequest('Request not allowed');
        #return self.authorize_user(bundle)

    def update_list(self, object_list, bundle):
        print 'Update List'
        raise BadRequest('Request not allowed');
        #allowed = []
        #for obj in object_list:
            # print "User is superuser %s"%(bundle.request.user.is_superuser)
            # print "User owns obj %s"%(bundle.request.user == bundle.obj.user)

            #if bundle.request.user.is_superuser or bundle.request.user == bundle.obj.user:
                #allowed.append(obj)

        #return allowed

'''
    CartItem APIs
'''                            
class CartItemResource(ModelResource):
    product = fields.ForeignKey(SearchProductResource, 'product', full=True)
    
    class Meta:
        queryset = CartItem.objects.all()
        resource_name = 'cart_item'
        #fields = ['cart_id','product_id','quantity']
        excludes = ['creation_date', 'modification_date']
        #allowed_methods = ['post']
        authentication = SessionAuthentication()
        authorization = CartItemAuthorization()
        
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/add%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('add_to_cart'), name="api_add_to_cart"),
            url(r"^(?P<resource_name>%s)/remove%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('remove_from_cart'), name="api_delete_from_cart"),
            url(r"^(?P<resource_name>%s)/update%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('modify_cart_item_quantity'), name="api_modify_cart_item_qty"),
            url(r"^(?P<resource_name>%s)/note%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('cart_item_note'), name="api_cart_item-note"),
        ]
            
    def add_to_cart(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        cart_id = data.get('cart_id', '')
        product_id = data.get('product_id', '')
        #TODO: Check if the product exist
        try : 
            cart = Cart.objects.get(user__exact=request.user, pk=cart_id)
        except ObjectDoesNotExist:
            return self.create_response(request, {
                'success': False,
                'message': 'Invalid cart'
            })

        cart_item = CartItem.objects.add_to_cart(cart=cart, product_id=product_id, quantity=1, note=None)
        
        if cart_item:
            return self.create_response(request, {
                'success': True,
                'cart_item_id': cart_item.id
            })
        else:
            return self.create_response(request, {
                'success': False,
                'message': 'Failed to add item to the cart'
            })
       
    def modify_cart_item_quantity(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        data = self.deserialize(request, request.body, format=request.META.get('CONTENT_TYPE', 'application/json'))
        cart_item_id = data.get('cart_item_id', '')
        quantity = data.get('quantity', '')
        
        if(quantity<0 or (Decimal(quantity) % 1 != 0)):
            return self.create_response(request, {
                'success': False,
                'message': 'Invalid quantity'
            })
            
        try:
            cart_item = CartItem.objects.get(pk=cart_item_id, cart__user__exact=request.user)
            cart_item.update_quantity(quantity=quantity)
            cart_item.set_price()
            cart_item.set_tax()
            cart_item.save()
            return self.create_response(request, {
                'success': True
            })
        except Exception as e:
            return self.create_response(request, {
                'success': False,
                'message': 'Failed to modify the quantity of the item'
            })
        
    def cart_item_note(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        cart_item_id = data.get('cart_item_id', '')
        note = data.get('note', '')
        
        try:
            CartItem.objects.filter(pk=cart_item_id, cart__user__exact=request.user).update(note=note)
            return self.create_response(request, {
                'success': True
            })        
        except Exception as e:
            return self.create_response(request, {
                'success': False,
                'message': 'Failed to add note to the item'
            })
        
    def remove_from_cart(self, request, **kwargs):
        self.method_check(request, allowed=['delete'])
        self.is_authenticated(request)
        cart_item_id = request.GET.get('id', '')
        
        try:
            CartItem.objects.filter(pk=cart_item_id, cart__user__exact=request.user).delete()
            return self.create_response(request, {
                'success': True
            })
        except Exception as e:
            return self.create_response(request, {
                'success': False,
                'message': 'Failed to remove the item from the cart'
            })

'''
    DeliveryCharge APIs
'''                            
class DeliveryChargeResource(ModelResource):
    
    class Meta:
        queryset = DeliveryCharge.objects.all()
        resource_name = 'delivery_slot'
        authentication = SessionAuthentication()
        authorization = CartItemAuthorization()

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/pick%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('pick_delivery_slot'), name="api_pick_delivery_slot"),
            url(r"^(?P<resource_name>%s)/remove%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('remove_delivery_slot'), name="api_remove_delivery_slot"),
        ]

    def pick_delivery_slot(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        cart_id = data.get('cart_id', '')
        store_id = data.get('store_id', '')
        is_one_hour = data.get('is_one_hour', '')
        is_two_hour = data.get('is_two_hour', '')
        is_three_hour = data.get('is_three_hour', '')

        try:
            delivery_charge = DeliveryCharge.objects.get(cart__id=cart_id, cart__user__exact=request.user, store__id=store_id)
            delivery_charge.is_one_hour = is_one_hour
            delivery_charge.is_two_hour = is_two_hour
            delivery_charge.is_three_hour = is_three_hour 
            delivery_charge.save()

            if(not is_one_hour and not is_two_hour and not is_three_hour):
                return self.create_response(request, {
                    'success': False,
                    'message': 'Failed to pick a available delivery time, Please try another delivery time'
                })
                      
            return self.create_response(request, {
                'success': True,
            })
        except ObjectDoesNotExist:
            try:
                try : 
                    cart = Cart.objects.get(user__exact=request.user, pk=cart_id)
                except ObjectDoesNotExist:
                    return self.create_response(request, {
                        'success': False,
                        'message': 'Invalid cart'
                    })
                DeliveryCharge.objects.pick_delivery_slot(cart=cart, store_id=store_id, is_one_hour=is_one_hour, is_two_hour=is_two_hour, is_three_hour = is_three_hour);
                return self.create_response(request, {
                    'success': True
                })
            except Exception as e:
                return self.create_response(request, {
                    'success': False,
                    'message': 'Failed to pick the delivery slot'
                })

    def remove_delivery_slot(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        data = json.loads(request.body)
        cart_id = data.get('cart_id', '')

        try:
            DeliveryCharge.objects.filter(cart__id=cart_id, cart__user__exact=request.user).delete()   
            return self.create_response(request, {
                'success': True,
            })       
        except Exception as e:
            return self.create_response(request, {
                'success': False,
                'message': 'Failed to remove delivery slots'
            })