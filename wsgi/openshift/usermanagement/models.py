import datetime
from datetime import timedelta

from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser)
from django.db import models
from django.template import Context
from django.template.loader import get_template
from django.utils import timezone
from django.db import IntegrityError

from usermanagement.utils import Error
from usermanagement import utils

class UserManager(BaseUserManager):
    def create_user(self, first_name, last_name, email, password):
        """Creates customer with the given name, email and password."""
        
        if not first_name:
            raise ValueError('Users must have a first name')
        
        if not last_name:
            raise ValueError('Users must have a last name')
        
        if not email:
            raise ValueError('Users must have an email address')
        
        if not password:
            raise ValueError('Users must have a password')
        
        user = self.model(
            first_name=first_name,
            last_name=last_name,
            email=UserManager.normalize_email(email).strip().lower(),
        )
        try:
            user.set_password(password)
            user.save(using=self._db)
        except IntegrityError as e:
            print e.message
            err = Error()
            err.set_message("A user with this email already exists")
            return None, err
        user.send_welcome_email()
        return user, None

    def create_superuser(self, name, email, password, phone):
        """Creates Admin with the given name, email and password."""
        
        if not name:
            raise ValueError('Users must have a name')
        
        if not email:
            raise ValueError('Users must have an email address')
        
        if not password:
            raise ValueError('Users must have a password')
        
        if not phone:
            raise ValueError('Users must have a phone')
        
        if not len(str(phone)) ==15:
            raise ValueError('Invalid phone number')

        user = self.model(
            name=name,
            email=UserManager.normalize_email(email).strip().lower(),
        )
        
        user.is_admin = True
        user.set_password(password)
        user.set_phone(phone)
        user.save(using=self._db)
        return user

    def create_driver(self, name, email, password, phone):
        """Creates driver with the given name, email and password."""
        
        if not name:
            raise ValueError('create_driver must have a name')
        
        if not email:
            raise ValueError('create_driver must have an email address')
        
        if not password:
            raise ValueError('create_driver must have a password')
        
        if not phone:
            raise ValueError('create_driver must have a phone')
        
        if not len(str(phone)) ==15:
            raise ValueError('create_driver Invalid phone number')

        user = self.model(
            name=name,
            email=UserManager.normalize_email(email).strip().lower(),
        )
        
        user.is_driver = True
        user.set_password(password)
        user.set_phone(phone)
        user.save(using=self._db)
        return user
    
    def get_all_admins(self):
        return User.objects.filter(is_admin=True)

class User(AbstractBaseUser):
    objects = UserManager()
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(
        max_length=255,
        unique=True,
    )
    phone = models.CharField(max_length=15, null=True)
    is_admin = models.BooleanField(default=False)
    is_driver = models.BooleanField(default=False)
    lastOrderID = models.CharField(max_length=25, null=True)
    expiry = models.DateTimeField(default=timezone.now())
    forgot_password_hash = models.CharField(max_length=40,
                                            default=utils.make_random_string(40))
    last_store = models.ForeignKey('store.Store', null=True, blank=True)
    email_subscription = models.BooleanField(default=True)
    is_first_order = models.BooleanField(default=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name','last_name']

    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)
    
    def set_phone(self, phone):
        self.phone = phone
        
    @property
    def is_staff(self):
        return self.is_admin
    
    def send_welcome_email(self):
        context = Context({
            'user': self,
        })

        plaintext_template = get_template('emails/register.txt')
        plaintext_content = plaintext_template.render(context)

        html_template = get_template('emails/register.html')
        html_content = html_template.render(context)

        subject = 'Welcome to Smartkart!'
        utils.send_email(subject=subject, plaintext_content=plaintext_content, to_email=[self.email], html_content=html_content)
    
    def change_password(self, new_raw_password, current_raw_password):
        if len(new_raw_password) < 6:
            return False
        if self.check_password(current_raw_password):
            self.set_password(new_raw_password)
            self.save()
            return True
        else:
            return False
    
    def reset_password(self, new_raw_password):
        if len(new_raw_password) < 6:
            return False
        self.set_password(new_raw_password)
        self.forgot_password_hash = utils.make_random_string(40)
        self.save()
        return True
        
    def forgot_password(self):        
        self.expiry = timezone.now() + timedelta(days=1)
        self.forgot_password_hash = utils.make_random_string(40)
        self.save()

        context = Context({
            'user': self,
        })

        html_template = get_template('emails/forgot_password.html')#TODO:
        html_content = html_template.render(context)
        plaintext_template = get_template('emails/forgot_password.txt')#TODO:
        plaintext_content = plaintext_template.render(context)
        subject = 'Reset your SmartKart Password'
        utils.send_email(subject=subject, plaintext_content=plaintext_content, to_email=[self.email], html_content=html_content)
    
    def forgot_password_url_check(self, url_hash):
        if not self.expiry >= timezone.now():
            return False
        
        if not self.forgot_password_hash == url_hash:
            return False
        
        self.expiry = datetime.datetime.now() + timedelta(days=1)
        self.forgot_password_hash = utils.make_random_string(40)
        self.save()
        return True
    
    def set_last_store(self, store_id):
        try:
            self.last_store_id = store_id
            self.save()
            return True
        except Exception as e :
            print '%s (%s)' % (e.message, type(e))
            return False
        
class DriverJobRequest(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=75)
    phone = models.CharField(max_length=10)
    above_18 = models.BooleanField(default = False)
    suspended_licence = models.BooleanField(default = False)
    WEEKLY_HOURS = (
        ('5', '0 - 5'),
        ('10', '5 - 10'),
        ('20', '10 - 20'),
        ('30', '20 - 30'),
        ('40', '30 - 40'),
        ('40+', '40+'),
    )
    number_of_hours_a_week = models.CharField(max_length=5, choices=WEEKLY_HOURS, default="5")
    Monday = models.BooleanField(default = False)
    Tuesday = models.BooleanField(default = False)
    Wednesday = models.BooleanField(default = False)
    Thursday = models.BooleanField(default = False)
    Friday = models.BooleanField(default = False)
    Saturday = models.BooleanField(default = False)
    Sunday = models.BooleanField(default = False)