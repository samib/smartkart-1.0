# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table(u'usermanagement_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=255)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=15, null=True)),
            ('is_admin', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_driver', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('lastOrderID', self.gf('django.db.models.fields.CharField')(max_length=25, null=True)),
            ('expiry', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 12, 13, 0, 0))),
            ('forgot_password_hash', self.gf('django.db.models.fields.CharField')(default='mbwcFxxGDsKfzF7cTeUedqD8kJa2HGDfCPagmYM2', max_length=40)),
            ('last_store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Store'], null=True, blank=True)),
            ('email_subscription', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_first_order', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'usermanagement', ['User'])

        # Adding model 'DriverJobRequest'
        db.create_table(u'usermanagement_driverjobrequest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('above_18', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('suspended_licence', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('number_of_hours_a_week', self.gf('django.db.models.fields.CharField')(default='5', max_length=5)),
            ('Monday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Tuesday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Wednesday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Thursday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Friday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Saturday', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Sunday', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'usermanagement', ['DriverJobRequest'])


    def backwards(self, orm):
        # Deleting model 'User'
        db.delete_table(u'usermanagement_user')

        # Deleting model 'DriverJobRequest'
        db.delete_table(u'usermanagement_driverjobrequest')


    models = {
        u'store.store': {
            'Meta': {'object_name': 'Store'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'delivery_end_time': ('django.db.models.fields.TimeField', [], {}),
            'delivery_start_time': ('django.db.models.fields.TimeField', [], {}),
            'has_alcohol': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_free_delivery_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'min_order_for_free_delivery': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'minimum_order': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'one_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'one_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'phone': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'store_logo_url': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'three_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'three_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'time_zone': ('django.db.models.fields.CharField', [], {'default': "'America/Toronto'", 'max_length': '50'}),
            'two_hour_delivery_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'two_hour_delivery_fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        },
        u'usermanagement.driverjobrequest': {
            'Friday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Meta': {'object_name': 'DriverJobRequest'},
            'Monday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Saturday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Sunday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Thursday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Tuesday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Wednesday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'above_18': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'number_of_hours_a_week': ('django.db.models.fields.CharField', [], {'default': "'5'", 'max_length': '5'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'suspended_licence': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'usermanagement.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255'}),
            'email_subscription': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'expiry': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 12, 13, 0, 0)'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'forgot_password_hash': ('django.db.models.fields.CharField', [], {'default': "'mbwcFxxGDsKfzF7cTeUedqD8kJa2HGDfCPagmYM2'", 'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_driver': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_first_order': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'lastOrderID': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'last_store': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['store.Store']", 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True'})
        }
    }

    complete_apps = ['usermanagement']