from django.core.mail import get_connection, EmailMultiAlternatives

def send_email(subject, plaintext_content, to_email, html_content):
    msg = EmailMultiAlternatives(subject, plaintext_content, 'auto-confirm@smartkart.ca', to_email)
    msg.attach_alternative(html_content, "text/html")
    msg.send()
    
def make_random_string(length=10,
    allowed_chars='abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789'):
    import random
    try:
        random = random.SystemRandom()
    except NotImplementedError:
        pass
    return ''.join([random.choice(allowed_chars) for i in range(length)])

class Error(object):
    def __init__(self):
        self.message = None
        
    def set_message(self, message):
        self.message = message

# def send_mass_html_mail(datatuple, fail_silently=False, user=None, password=None, 
#                         connection=None):
#     """
#     Given a datatuple of (subject, text_content, html_content, from_email,
#     recipient_list), sends each message to each recipient list. Returns the
#     number of emails sent.
# 
#     If from_email is None, the DEFAULT_FROM_EMAIL setting is used.
#     If auth_user and auth_password are set, they're used to log in.
#     If auth_user is None, the EMAIL_HOST_USER setting is used.
#     If auth_password is None, the EMAIL_HOST_PASSWORD setting is used.
# 
#     """
#     connection = connection or get_connection(
#         username=user, password=password, fail_silently=fail_silently
#     )
# 
#     messages = []
#     for subject, text, html, from_email, recipient in datatuple:
#         message = EmailMultiAlternatives(subject, text, from_email, recipient)
#         message.attach_alternative(html, 'text/html')
#         messages.append(message)
#     return connection.send_messages(messages)
