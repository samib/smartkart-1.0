'''
Created on Oct 27, 2013

@author: Hisham
'''
from django.conf.urls import patterns, url, include
from tastypie.api import Api
from usermanagement.api import UserResource, DriverResource, DriverJobRequestResource

user_resource = UserResource()
driver_resource = DriverResource()
driver_job_request_resource = DriverJobRequestResource()

urlpatterns = patterns('usermanagement.views',
    #url(r'^$', 'login', None, 'login'),
    url(r'^api/', include(user_resource.urls)),
    url(r'^api/', include(driver_resource.urls)),
    url(r'^api/', include(driver_job_request_resource.urls)),
);