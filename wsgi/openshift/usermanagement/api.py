'''
Created on Oct 28, 2013

@author: Hisham
'''
import json

from django.conf.urls import url
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf

from tastypie.authentication import SessionAuthentication
from tastypie.authorization import Authorization
from tastypie.exceptions import Unauthorized
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash
from tastypie.validation import Validation

from usermanagement.models import User, DriverJobRequest

class UserAuthorization(Authorization):

    def read_list(self, object_list, bundle):
        return object_list.filter(pk = bundle.request.user.pk)
    def read_detail(self, object_list, bundle):
        return object_list.filter(pk = bundle.request.user.pk)
    
class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'        
        fields = ['first_name','last_name','is_driver','is_admin']
        excludes = ['password', 'phone', 'is_superuser' ,'forgot_password_hash']
        validation = Validation()
        include_resource_uri = False
        allowed_methods = ['get', 'post']
        authentication = SessionAuthentication()
        authorization = UserAuthorization()
        
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="api_login"),
            url(r'^(?P<resource_name>%s)/logout%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('logout'), name='api_logout'),
#             url(r'^(?P<resource_name>%s)/active%s$' %
#                 (self._meta.resource_name, trailing_slash()),
#                 self.wrap_view('is_logged_in'), name='api_is_logged_in'),
            url(r'^(?P<resource_name>%s)/forgot%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('forgot_password'), name='api_forgot_password'),
            url(r'^(?P<resource_name>%s)/change%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('change_password'), name='api_reset_password'),
            url(r'^(?P<resource_name>%s)/reset%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('set_new_password'), name='api_set_new_password'),
            url(r'^(?P<resource_name>%s)/token%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('reset_forgot_password'), name='api_reset_forgot_password'),
            url(r'^(?P<resource_name>%s)/registration%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('registration'), name='api_registration'),
            url(r'^(?P<resource_name>%s)/admin/shopper/register%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('driver_registration'), name='api_driver_registration'),
            url(r'^(?P<resource_name>%s)/admin/register%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('admin_registration'), name='api_admin_registration'),
            url(r'^(?P<resource_name>%s)/last_store%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('set_last_store'), name='api_set_last_store'),   
        ]
           
    def registration(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        data = json.loads(request.body)
        email = data.get('email', '')
        password = data.get('password', '')
        first_name = data.get('first_name', '')
        last_name = data.get('last_name', '')
        
        if len(str(password))<6:
            return self.create_response(request, {
                'success': False,
                'message': 'Password must be of at least 6 characters!'
            })
            
        new_user, err = User.objects.create_user(first_name=first_name, last_name=last_name, email=email, password=password)
        if new_user:
            user = authenticate(username=email, password=password)
            if user:
                login(request, user)
                return self.create_response(request, {
                    'success': True,
                    'userName': user,
                    'isAdmin': user.is_admin,
                    'isDriver': user.is_driver,
                    'is_first_order': user.is_first_order,
                    'last_store': user.last_store_id,
                    'csrf': csrf(request)
                })
        else:
            return self.create_response(request, {
                    'success': False,
                    'message': err.message
                })
            
    def admin_registration(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        if request.user and request.user.is_admin:
            data = json.loads(request.body)
            email = data.get('email', '')
            password = data.get('password', '')
            first_name = data.get('first_name', '')
            last_name = data.get('last_name', '')
            phone = data.get('phone', '')
            if not len(str(password))==6:
                return self.create_response(request, {
                    'success': False,
                    'message': 'Password must be of atleast 6 characters!'
                })
            User.objects.create_superuser(first_name=first_name, last_name=last_name, email=email, password=password, phone=phone)
            return self.create_response(request, {
                'success': True
            })
        else:
            return self.create_response(request, {
                'success': False
            })

    def driver_registration(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        if request.user and request.user.is_admin:
            data = json.loads(request.body)
            email = data.get('email', '')
            password = data.get('password', '')
            first_name = data.get('first_name', '')
            last_name = data.get('last_name', '')
            phone = data.get('phone', '')
            if not len(str(password))>=6:
                return self.create_response(request, {
                    'success': False,
                    'message': 'Password must be of atleast 6 characters!'
                })
            User.objects.create_driver(first_name=first_name, last_name=last_name, email=email, password=password, phone=phone)
            return self.create_response(request, {
                'success': True
            })
        else:
            return self.create_response(request, {
                'success': False
            })

    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        data = json.loads(request.body)
        email = data.get('email', '')
        password = data.get('password', '')

        user = authenticate(username=email, password=password)
        if user:
            login(request, user)
            
            return self.create_response(request, {
                'success': True,
                'isAdmin': user.is_admin,
                'isDriver': user.is_driver,
                'is_first_order': user.is_first_order,
                'userName': user,
                'last_store': user.last_store_id,
                'csrf': csrf(request)
            })
        else:
            return self.create_response(request, {
                'success': False,
                'message': 'Incorrect login credentials',
            }, HttpUnauthorized )

    def logout(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if request.user and request.user.is_authenticated():
            logout(request)
            return self.create_response(request, { 
                'success': True 
            })
        else:
            return self.create_response(request, { 
                'success': False 
            }, HttpUnauthorized)
    
#     def is_logged_in(self, request, **kwargs):
#         self.method_check(request, allowed=['get'])
#         if request.user and request.user.is_authenticated():
#             return self.create_response(request, { 
#                 'success': True 
#             })
#         else:
#             return self.create_response(request, { 
#                 'success': False 
#             }, HttpUnauthorized)

    def change_password(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        data = json.loads(request.body)        
        current_password = data.get('cp', '')
        new_password = data.get('np', '')
        if not len(str(new_password))>=6:
            return self.create_response(request, {
                'success': False,
                'message': 'Password must be of atleast 6 characters'
            })
        response = request.user.change_password(new_password, current_password)
        if response:
            return self.create_response(request, { 
                'success': True,
                'message': 'Successfully changed your password', 
            })
        else:
            return self.create_response(request, { 
                'success': False,
                'message': 'Invalid current password', 
            })
            
    def set_new_password(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        data = json.loads(request.body)
        user_id = data.get('temp_id', '')
        token_hash = data.get('token', '')
        new_password = data.get('password', '')
        if not len(str(new_password))>=6:
            return self.create_response(request, {
                'success': False,
                'message': 'Password must be of atleast 6 characters'
            })
        user_id_with_buffer = int(user_id)
        user_id_int = user_id_with_buffer - 125
        user = User.objects.get(pk=user_id_int)
        if(user.forgot_password_hash == token_hash):
            response = user.reset_password(new_password)
            if response:
                return self.create_response(request, { 
                        'success': True,
                        'message': 'Successfully set the new password',  
                })
            else:
                return self.create_response(request, { 
                     'success': False,
                     'message': 'Error while resetting the password', 
                })
        else:
            return self.create_response(request, { 
                 'success': False,
                 'message': 'This link can only be used once', 
            })
                    
    def forgot_password(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        data = json.loads(request.body)
        email = data.get('email', '')
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            return self.create_response(request, {
                    'success': False,
                    'message': 'Invalid Email address' 
            })
        user.forgot_password()
        return self.create_response(request, {
                'success': True,
                'message': 'Successfully send user email to reset password' 
            })
    
    def reset_forgot_password(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        user_id = request.GET.get('exp')
        token = request.GET.get('t')
        user_id_with_buffer = int(user_id)
        user_id_with_buffer += 125 # Adding 125 to user_id and subtract it in set_new_password
        user = User.objects.get(pk=user_id)
        status = user.forgot_password_url_check(token)
        user_id_with_buffer_string = str(user_id_with_buffer)
        if status:
            return HttpResponseRedirect('http://'+request.get_host()+'/#/reset?temp_id='+user_id_with_buffer_string+'&token='+user.forgot_password_hash)
        else:
            return self.create_response(request, { 
                     'success': False,
                     'message':'This link has expired or was already accessed. Please request for a new link.' 
                 })
            
    def set_last_store(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        data = json.loads(request.body)
        store_id = data.get('store_id', '')
        if store_id:
            result = request.user.set_last_store(store_id)
            if result:
                return self.create_response(request, { 
                         'success': True,
                         'message':'Successfully set the last store' 
                     })
            else:
                return self.create_response(request, { 
                         'success': False,
                         'message':'Error while setting the last store' 
                     })
                
        else:
            return self.create_response(request, { 
                     'success': False,
                     'message':'No store was provided' 
                 })

    # def wrap_view(self, view):
    #     def wrapper(request, *args, **kwargs):
    #         return ModelResource.wrap_view(self, view)(request, *args, **kwargs)
    #     return wrapper
        
class DriverInfoAccessAuthorization(Authorization):
    def authorize_user(self, bundle):
        print 'Authorize User'
    
    def read_list(self, object_list, bundle):
        print 'Read List'
        if bundle.request.user and bundle.request.user.is_admin:
            return object_list.all()
        else:
            raise Unauthorized("Only admin user can access this.")
        
class DriverResource(ModelResource):
    
    class Meta:
        queryset = User.objects.filter(is_driver = True)
        resource_name = 'drivers'
        excludes = ['password', 'forgot_password_hash', 'is_admin', 'is_driver', 'expiry', 'last_login', 'lastOrderID']
        include_resource_uri = True
        validation = Validation()#check?
        allowed_methods = ['get']
        authentication = SessionAuthentication()
        authorization = DriverInfoAccessAuthorization()
                  
class DriverJobRequestResource(ModelResource):
    
    class Meta:
        queryset = DriverJobRequest.objects.all()
        resource_name = 'job'
        # excludes = ['password', 'forgot_password_hash', 'is_admin', 'is_driver', 'expiry', 'last_login', 'lastOrderID']
        include_resource_uri = False
        validation = Validation()#check?
        allowed_methods = ['get','post']
        authentication = SessionAuthentication()
        authorization = DriverInfoAccessAuthorization()
    
    def prepend_urls(self):
        return [            
            url(r'^(?P<resource_name>%s)/shopper%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('driver_job_request'), name='api_driver_job_request'),  
        ]
           
    def driver_job_request(self, request, **kwargs):
        self.method_check(request, allowed=['post'])        
        data = json.loads(request.body)
        first_name = data.get('fname', '')
        last_name = data.get('lname', '')
        email = data.get('email', '')
        phone = data.get('phone', '')
        above_18 = data.get('above_18', '')
        suspended_licence = data.get('suspended_licence', '')
        hours = data.get('hours', '')
        monday = data.get('monday', '')
        tuesday = data.get('tuesday', '')
        wednesday = data.get('wednesday', '')
        thursday = data.get('thursday', '')
        friday = data.get('friday', '')
        saturday = data.get('saturday', '')
        sunday = data.get('sunday', '')
        DriverJobRequest.objects.create(first_name = first_name, last_name= last_name, email=email, phone=phone, above_18 = above_18, suspended_licence = suspended_licence, number_of_hours_a_week=hours,
                              Monday = monday, Tuesday = tuesday, Wednesday = wednesday, Thursday = thursday, Friday = friday, Saturday = saturday, Sunday = sunday)

        return self.create_response(request, {
            'success': True
        })
        
        
#     def get_drivers(self, request, **kwargs):
#         self.method_check(request, allowed=['get'])
#         self.is_authenticated(request)
#         if request.user and request.user.is_admin:
#             self.method_check(request, allowed=['get'])
#             drivers = User.objects.filter(is_driver = True)
#             data = serializers.serialize('json', drivers)
#             return HttpResponse(data, mimetype='application/json')
#         else:
#             return self.create_response(request, {
#                 'success': False,
#                 'message':'Only admin user can access this.'
#             })
        