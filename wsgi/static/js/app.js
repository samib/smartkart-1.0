var app = angular.module('SmartKart', ['angularUtils.directives.dirPagination','angular-loading-bar','ngCookies','ngRoute','ui.bootstrap','angularPayments','ngAnimate','toaster','ui.router','LocalStorageModule', 'angularSpinner', 'ngResource','truncate', 'perfect_scrollbar']);

//magic values
app.value('FREE_DELIVERY_COUPON_CODE', 'FREE');
app.value('defaultStoreId', 1);
app.value('defaultAisleId', 1);
app.value('defaultDptId', 1);
app.value('defaultStoreIndex', 0);
app.value('TAX_RATE', 0.13);
app.value('apiPrefix','api/');
app.value('defaultHomePath','/home/store/1/department/1/aisle/1');

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                var cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

app.config(['$routeProvider','$httpProvider','$urlRouterProvider', '$stateProvider', 'cfpLoadingBarProvider', function($routeProvider, $httpProvider,$urlRouterProvider,$stateProvider,cfpLoadingBarProvider) {


      cfpLoadingBarProvider.latencyThreshold = 750;
       $httpProvider.defaults.useXDomain = true;
       delete $httpProvider.defaults.headers.common['X-Requested-With'];
       $httpProvider.defaults.withCredentials = true;
       $httpProvider.defaults.headers.patch = {
            'Content-Type': 'application/json;charset=utf-8'
          }

       $urlRouterProvider.otherwise('/');

       $stateProvider.state('landing', {
            url: '/',
            templateUrl: 'static/partials/landingPage.html' 
        })
       .state('login', {
            url: '/login',
            templateUrl: 'static/partials/landingPage.html' 
        })
       .state('register', {
            url: '/register',
            templateUrl: 'static/partials/landingPage.html' 
        })
       .state('forgot', {
            url: '/forgot',
            templateUrl: 'static/partials/landingPage.html' 
        })
        .state('resetPasswd', {
            url: '/reset',
            templateUrl: 'static/partials/resetPassword.html' 
        })
        .state('driverdashboard', {
            url: '/driverdashboard',
            templateUrl: 'static/partials/driverDashboard.html',
            controller: 'DriverDashboardCtrl'
        })
        .state('driverOrder', {
            url: '/driver/order/:dOrderId', //driver order ID
            templateUrl: 'static/partials/driverOrder.html',
            controller: 'DriverOrderCtrl',
            resolve: {

                assignedOrders: function(DriverOrders){
                  return DriverOrders.fetch().$promise;
                },

                driverOrder: function(assignedOrders,$stateParams){

                  // Helper function to premark items
                  function premarkItemStatuses(order){

                    order.items.forEach(function(item){
                      item.isFound = item.status == 'FOUND';
                      item.didSubstitute = item.status == 'SUBSTITUTE';
                      item.didRefund = item.status == 'REFUND';
                    });

                    return order;

                  }

                  var thisOrder = {};
                  var orderNumber = $stateParams.dOrderId;
                  assignedOrders.objects.forEach(function(order){
                    if (order.order_number == orderNumber){
                      thisOrder = order; 
                    }
                  });

                  return premarkItemStatuses(thisOrder);

                }

            }

        })

       .state('info', {
          url: '/info',
          templateUrl : 'static/partials/page.html',
          abstract:true,
          controller: 'pageCtrl',
          resolve : {
                userInfo: function(UserResource,$q, $cookieStore){
                  if($cookieStore.get('userToken') == "undefined"){
                    var deferred = $q.defer();
                    UserResource.get(function(user){
                          deferred.resolve(user.objects[0]);
                      });
                    return deferred.promise;
                  }
                    
                },
           }            
         })
      .state('info.profile' , {
          url: '/profile',
          views: {
            'processview' : {
              templateUrl: 'static/partials/profilePage.html',
              controller: 'profilePageCtrl',
              resolve: {

                cards: function(CardsResource){
                    return CardsResource.get().$promise;
                },

                ownAddresses: function(AddressResource){
                    return AddressResource.get().$promise;
                }


              }
              
            }
        }})
      .state('info.tos' , {
          url: '/tos',
          views: {
            'processview' : {
              templateUrl: 'static/partials/tos.html',
              controller: 'backCtrl'
            }
        }})
      .state('info.privacy' , {
          url: '/privacy',
          views: {
            'processview' : {
              templateUrl: 'static/partials/privacy.html',
              controller: 'backCtrl'
            }
        }})
      .state('info.faq' , {
          url: '/faq',
          views: {
            'processview' : {
              templateUrl: 'static/partials/faq.html',
              controller: 'backCtrl'
            }
        }})
      .state('admin', {
          url: '',
          templateUrl : 'static/partials/page.html',
          abstract:true,
          controller: 'pageCtrl',
          resolve : {

                userInfo: function(UserResource,$q, $cookieStore){
                  if($cookieStore.get('userToken') == "undefined"){
                    var deferred = $q.defer();
                    UserResource.get(function(user){
                          deferred.resolve(user.objects[0]);
                      });
                    return deferred.promise;
                  }
                    

                },
           }            
         })
      .state('admin.manageConfiguration' , {
          url: '/admin',
          views: {
              'processview' : {
                  templateUrl: 'static/partials/admin.html',
                  controller: 'AdminCtrl',
                  resolve:{
                    recentOrders: function(Order,$q,$filter){
                      var deferred = $q.defer();

                      
                      var toDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                      var fromDate = new Date();
                      fromDate.setDate(fromDate.getDate() - 7); // bout a week ago
                      fromDate = $filter('date')(fromDate, 'yyyy-MM-dd');

                      Order.getOrdersForRange(/*fromDate*/toDate, toDate,function(response){
                        deferred.resolve(response.objects);

                      });

                      return deferred.promise;

                    },
                    priceChangeList: function(PriceChangeRequests){
                        return PriceChangeRequests.get().$promise;
                    }
                  }
                }
            }
        })
      .state('home', {
          abstract: true,
          url : '/home',
          views : {
            '' : { 
              templateUrl: 'static/partials/main.html'
              }, 
            'cart@home' : { 
              templateUrl: 'static/partials/cart.html',
              controller: 'CartCtrl',
              resolve : {

                cart: function(CartResource){ // Injecting Cart resource

                      return CartResource.retrieveCart().$promise;
                      // $scope.cartItems in CartCtrl binds to this promise and updates ui when the data is resolved

                },
                storeInfo: function(StoreResource){
                    return StoreResource.getAllStores().$promise;
                }

              }
            },
            'navigation@home' : { 

              templateUrl: 'static/partials/navigation.html',
              controller: 'SideNavCtrl',
              resolve : {

                storeInfo: function(StoreResource){

                    return StoreResource.getAllStores().$promise;

                }

              }
              
              },
          }
       })

       .state('home.products' , {

          url: '/store/:storeId/department/:departmentId/aisle/:aisleId',
          views: {
            'mainview' : {
              templateUrl: 'static/partials/productList.html',
              controller: 'ProductListCtrl',
              resolve: {

                  productData: function(ProductsInAisle,$stateParams){ 
                  
                    return ProductsInAisle.get({
                                                aisle_id : $stateParams.aisleId,
                                                store_id : $stateParams.storeId
                                              }).$promise;                  
            
                  },
                  selectedStore : function(StoreHelper,$stateParams){
                    //Resolve selected aisle info
                    return StoreHelper.getStoreById($stateParams.storeId);
                  },

                  selectedDpt : function(StoreHelper,$stateParams){
                    return StoreHelper.getDepartmentById($stateParams.departmentId);
                  },

                  selectedAisle : function(StoreHelper,$stateParams){
                    //Resolve selected aisle info
                    return StoreHelper.getAisleById($stateParams.aisleId);
                  },

                  sortedProducts : function(productData){

                    var products = productData.objects;

                    products.sort(function(firstProduct,secondProduct){

                      if (firstProduct.image_url.indexOf('smartkart_logo.JPG') != -1){
                        return 1;
                      } else return 0;

                    });

                    return products;

                  },
                  /*  
                      Do not remove this or Angular throws an injector error. 
                      This controller is shared with home.search state, so we have
                      to inject *something* albeit null to satisfy dependencies
                  */
                  searchResults : function(){
                      return null;
                  }
              }
            }
        }})

        .state('home.search' , {

          url: '/search/:queryString',
          views: {

            'mainview' : {
              templateUrl: 'static/partials/searchResults.html',
              controller: 'ProductListCtrl', //re-using this controller since 90% of the functionality is identical
              resolve: {

                  sortedProducts : function(){
                    return null;
                  },
                  productData : function(){
                      return null;
                  },
                  searchResults: function($stateParams,Search,SearchHelper,$q,$rootScope){ 
                    
                    var deferred = $q.defer();
                    var queryString = $stateParams.queryString;
                    Search.get({queryString : queryString},function(response){

                        var searchResults = SearchHelper.harmonizeSearchResults(response.objects,queryString,$rootScope.allCartItems);
                        deferred.resolve(searchResults);

                      }, function(error){

                        //console.log('Error searching : ' + JSON.stringify(error));

                      });
                      return deferred.promise;
                                    
                  },
                  /*  
                    Because we share a controller with the product list, nullify these 
                    values or Angular throws an injection error when the controller is 
                    instantiated. These two resolves save us ~300 loc
                  */
                  productsInAisle : function(){
                        return null;
                  },
                  selectedStore : function($rootScope,CacheManager){

                        if ($rootScope.selectedStore) return $rootScope.selectedStore;
                        return CacheManager.get('selectedStore');

                  },

                  selectedDpt : function($rootScope,CacheManager){

                        if ($rootScope.selectedDepartment) return $rootScope.selectedDepartment;
                        return CacheManager.get('selectedDepartment');                   
                  },

                  selectedAisle : function($rootScope,CacheManager){
                        //Resolve selected aisle info
                        if ($rootScope.selectedAisle) return $rootScope.selectedAisle;
                        return CacheManager.get('selectedAisle'); 
                  }

              }
            }
        }})
        .state('process', {
          url: '/process',
          templateUrl : 'static/partials/page.html',
          abstract:true,
          controller: 'pageCtrl',
          resolve : {}            
         })

        .state('process.checkout' , {
          url: '/checkout',
          views: {
            'processview' : {
              templateUrl: 'static/partials/checkout.html',
              controller: 'checkoutCtrl',
              resolve : {
                cart: function(CartResource){ //injecting Cart resource

                    var cartData = CartResource.retrieveCart();
                    // $scope.cartItems in CartCtrl binds to this promise and updates ui when the data is resolved

                    return cartData.$promise;
                },
                ownAddresses: function(AddressResource){
                    return AddressResource.get().$promise;
                },
                storeInfo: function(StoreResource){
                    var storeData = StoreResource.getAllStores();
                    return storeData.$promise;
                },
                cartItemsByStore: function(cart,storeInfo,Sorting){
                    return Sorting.groupCartItemsByStore(cart[0].items,storeInfo);
                },
                discountData: function(cart,CouponService,Sorting,storeInfo,$q){

                    var coupon = cart[0].coupon;
                    if (!coupon){
                      return null;
                    }
                    /*couponCode,cart_id,num_of_stores,callback*/

                    var cartItemsByStore = Sorting.groupCartItemsByStore(cart[0].items,storeInfo);
                    var deferred = $q.defer();
                    CouponService.reApplyCoupon(coupon.code,cart[0].cart_id,cartItemsByStore.length,function(response){
                          deferred.resolve(response);
                      });

                    return deferred.promise;                    
                },
                cards: function(CardsResource){
                    return CardsResource.get().$promise;
                },

                totalTax: function(cartItemsByStore,TAX_RATE){
                    var totalTax = 0;
                    cartItemsByStore.forEach(function(store){

                            var taxForThisStore = 0;
                            store.cartItems.forEach(function(cartItem){

                                if (cartItem.product.taxable)
                                {
                                    taxForThisStore += parseFloat(cartItem.product.price * cartItem.quantity) * TAX_RATE ;
                                }
                                
                            });

                            totalTax += taxForThisStore;
                        });

                    // Delivery fee is also taxable
                    // totalTax += (getTotalDeliveryFee() * TAX_RATE);

                    return totalTax;

                },
                totalDeliveryFee: function(cart,Sorting,storeInfo){

                   var cartItemsByStore = Sorting.groupCartItemsByStore(cart[0].items,storeInfo);
                   var totalDeliveryFee = 0;
                   //console.log('Total Delivery Fee : ' + JSON.stringify(cartItemsByStore));
                   cartItemsByStore.forEach(function(store){

                      if (store.totalForStore <= parseFloat(store.min_order_for_free_delivery))
                      {
                          totalDeliveryFee += parseFloat(store.delivery_fee);
                      }

                   });

                   return totalDeliveryFee;

                },
                subtotal: function(cart,Sorting,storeInfo,cartItemsByStore){
                  var subtotal = 0;
                  //debugger;
                  cartItemsByStore.forEach(function(store){
                      store.cartItems.forEach(function(cartItem){
                            subtotal +=  parseFloat(cartItem.product.price) * cartItem.quantity;
                        });
                    });
                  return subtotal;
                },
                total: function(cart,Sorting,storeInfo){
                  var total = 0;
                  var subtotal = 0;
                  var totalDeliveryFee = 0;
                  var totalTax = 0;
                  var cartItemsByStore = Sorting.groupCartItemsByStore(cart[0].items,storeInfo);
                  //debugger
                  cartItemsByStore.forEach(function(store){

                      var taxForThisStore = 0;

                      store.cartItems.forEach(function(cartItem){
                            subtotal +=  parseFloat(cartItem.product.price) * cartItem.quantity;
                            if (cartItem.product.taxable)
                            {
                                taxForThisStore += parseFloat(cartItem.tax);
                            }


                        });

                      /*
                        if (!store.isDeliveryFree){
                            totalDeliveryFee += parseFloat(store.delivery_fee);
                        }
                      */

                      totalTax += taxForThisStore;

                      });
                      total = subtotal + totalTax;
                      return total;

                }


              }
              
            }
        }})
        .state('process.order' , {
          url: '/order',
          views: {
            'processview' : {
              templateUrl: 'static/partials/orderList.html',
              controller: 'UserOrderListCtrl',
              resolve: {
                  //  there should be a resolve here...

                  myOrders: function(MyOrdersResource,DateHelper,$q){

                    var deferred = $q.defer();

                    MyOrdersResource.get(function(response){

                        var orders = response.objects;
                        orders.forEach(function(order){

                          order.status = order.status.replace(/%20/g,' ');
                          order.deliveryTimeDisplayName = DateHelper.convert24hrTime(order.from_delivery_time) + ' - ' + DateHelper.convert24hrTime(order.to_delivery_time);

                          order.deliveryTimeDisplayName = order.deliveryTimeDisplayName.replace(/:00/g, ' ');

                          if (Number(order.discount_tax_deduction) > 0){
                            order.tax = Number(order.tax) - Number(order.discount_tax_deduction);
                          }

                          // Add tax, tip and delivery charge since they are not included in the total_price, which is actually the subtotal
                          order.total_price = Number(order.total_price) + Number(order.tip) + Number(order.delivery_charge) + Number(order.tax);

                          if (Number(order.discount) > 0){
                            order.total_price -= Number(order.discount);
                          } 

                      
                        });

                        orders.sort(function(firstOrder,secondOrder){
                          return secondOrder.order_number - firstOrder.order_number;
                        });

                        deferred.resolve(orders);

                      });
                    return deferred.promise;

                  }

              }
              
            }
        }})
        .state('process.orderDetails' , {
          url: '/orderdetails/:orderId',
          views: {
            'processview' : {
              templateUrl: 'static/partials/orderDetails.html',
              controller: 'UserOrderCtrl',
              resolve: {

                  thisOrder: function($stateParams,OrderResource,$q){

                    var deferred = $q.defer();

                      OrderResource.get({orderId:$stateParams.orderId},function(response){

                        deferred.resolve(response);

                      });

                    return deferred.promise;


                  }
              }
              
            }
        }})
        .state('process.admbrkdwn' , {
          url: '/admbrkdwn/:orderId',
          views: {
            'processview' : {
              templateUrl: 'static/partials/adminorderbrkdwn.html',
              controller: 'AdminOrderCtrl',
              resolve: {

                  orderInfo: function($stateParams,OrderResource,$q){
                    
                      var deferred = $q.defer();

                      OrderResource.get({orderId:$stateParams.orderId},function(response){

                        var orderInfo = response;
                        orderInfo.items.forEach(function(item){

                          item.profit = item.quantity*(Number(item.product.price) - Number(item.product.cost_price));
                          item.price = Number(item.quantity) * Number(item.product.price);

                        });

                        var totalMarkupProfit = 0;
                        orderInfo.items.forEach(function(item){
                          totalMarkupProfit += item.profit;

                        });

                        orderInfo.totalMarkupProfit = totalMarkupProfit;

                        deferred.resolve(orderInfo);

                      });

                      return deferred.promise;


                    }

                  }
              }
              
            }
        });
      
      }]);

app.service('anchorSmoothScroll', function(){
    
    this.scrollTo = function(eID) {

        // This scrolling function 
        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
        
        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }
        
        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }

        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 10) speed = 10;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for ( var i=startY; i<stopY; i+=step ) {
                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( var i=startY; i>stopY; i-=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }
      
    };
    
});

app.run(['$window', '$rootScope', '$location' ,'$cookieStore', '$state', '$timeout', '$anchorScroll', 'NotificationCenter', 'defaultHomePath','$cookies', 'anchorSmoothScroll', function($window, $rootScope, $location, $cookieStore, $state, $timeout, $anchorScroll, NotificationCenter, defaultHomePath,$cookies,anchorSmoothScroll){


    $rootScope.$on("$viewContentLoaded", function(){



          if ($state.current.name == 'landing'){

              anchorSmoothScroll.scrollTo('landingTop');
              return;

          }

          // Exclude all landing page states, no need to anchor scroll when animating the form in and out
          if ($state.current.name == 'register' || $state.current.name == 'login' || $state.current.name == 'forgot' || $state.current.name == 'resetPasswd'){

            return;

          }

          $anchorScroll();
      });

      $rootScope.$on("$viewContentLoading", function(){
        NotificationCenter.post('didStartLoading',null);
      });

      $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){

        debugger;
        NotificationCenter.post('doneLoading',null);

        //        debugger;
        $location.path('/login');

       console.log(' \n State change error: ' + error.message + '\n');
       console.log(' \n From state : ' + JSON.stringify(fromState) + '\n');
       console.log(' \n From Params : ' + JSON.stringify(fromParams) + '\n');
       console.log(' \n To state : ' + JSON.stringify(toState) + '\n');
       console.log(' \n To params : ' + JSON.stringify(toParams) + '\n');


      })

      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){ 
          
          NotificationCenter.post('didStartLoading',null);
          $.unblockUI();
           var urlCheck1 = $location.path() != '/forgot' && $location.path() != '/register' && $location.path() != '/postal' && $location.path() != '/';
           var urlCheck2 = $location.path() != '/jobrequest' && $location.path() != '/info/faq' && $location.path() != '/reset' && $location.path() != '/info/privacy' && $location.path() != '/info/tos';
            if(urlCheck1 && urlCheck2){
              if($cookieStore.get('userToken') == undefined){
		          $location.path('/login');
              } 
            } else if($cookieStore.get('userToken') != undefined && ($location.path() == '/login' || $location.path() == '/')){
                  $location.path(defaultHomePath);
                  
            }
      });

      $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){ 

            NotificationCenter.post('doneLoading',null);

       });
}]);



app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $location) {
        return {
            'response': function (response) {
                //Will only be called for HTTP up to 300
               return response;
            },
            'responseError': function (rejection) {
                if(rejection.status === 401) {
                  $location.path('/login');
                    
                }
                return $q.reject(rejection);
            }
        };
    });
}]);

