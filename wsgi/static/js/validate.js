$(document).ready(function() {

	//add postal code validation 
	$.validator.addMethod("PostalCode", function(postal, element) {
		return this.optional(element) ||
			postal.match(/[a-zA-Z][0-9][a-zA-Z](-| |)[0-9][a-zA-Z][0-9]/);
	}, "Please enter a valid postal code.");

	//override defaults
	$.validator.setDefaults({
		errorElement: "span"
	});

	//postal Form
	postalValidator = $("#postalForm").validate({
		//debug:true,
		rules: {
			inputPostal: {
				PostalCode: true,
				required: true,

				/*The serverside resource is called via jQuery.ajax (XMLHttpRequest) and gets a key/value pair corresponding to the 
				name of the validated element and its value as a GET parameter. 
				The response is evaluated as JSON and must be true for valid elements, 
				and can be any false, undefined or null for invalid elements, using the 
				default message; or a string, eg. "That name is already taken, try peter123 instead" to display as the error message.*/
				//remote: //Integrate with backend here. TODO
			}


		},
		submitHandler: function(form) {
			window.location.href = "#register";
		}

	});
	//Register Form
	registerValidator = $("#registerForm").validate({
		rules: {

			inputFirstName: {
				required: true
			},
			inputLastName: {
				required: true
			},
			inputEmail: {
				required: true,
				email: true,
				//remote: //Check if email already exists. TODO
			},
			inputPassword: {
				required: true
			}


		},
		submitHandler: function(form) {
			//Redirect to store
			//window.location.location ="" //TODO

		}

	});

	//login Form
	loginValidator = $("#loginForm").validate({
		rules: {

			inputEmail: {
				required: true,
				email: true
			},
			inputPassword: {
				required: true
			}

		},
		submitHandler: function(form) {
			//Redirect to store
			//window.location.location ="" //TODO

		}

	});
	//forgot password Form
	forgotValidator = $("#forgotForm").validate({
		rules: {

			inputEmail: {
				required: true,
				email: true
			}

		},
		submitHandler: function(form) {
			//Integrate with backend. 
			//TODO

		}

	});

});