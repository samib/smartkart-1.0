app.controller('DriverDashboardCtrl', function($rootScope, Driver, $scope, defaultHomePath) {
	
	Driver.retrieveDriverOrders(function(data){
		$scope.ordersAssigned2me = data.objects;
	});

	$scope.defaultHomePath = defaultHomePath;

	//TODO: make this a factory or part of a service
	$scope.getTotalCostPrice = function(order){
		var totalPrice = 0;
		order.items.forEach(function(item){

			var itemPrice = item.product.taxable ? Number(item.product.cost_price * 1.13) : Number(item.product.cost_price);
			totalPrice += Number(itemPrice * item.quantity);

		});
		
		return totalPrice;

	};

});

//As a driver, for viewing an Order assigned to me and its products/order items
app.controller('DriverOrderCtrl', function($scope, $stateParams, $rootScope, HttpService, $modal, Order, driverOrder, defaultHomePath){

	var orderNumber = $stateParams.dOrderId;

	// Initialize the model for the view
	$scope.thisOrder = driverOrder;
	$scope.defaultHomePath = defaultHomePath;
	// Actions
	$scope.toggleFound = function toggleFound(item){

		var newStatus = item.isFound ?'FOUND':'PENDING';
		HttpService.patchRequest('api/item/change/', {'item_id': item.id, 'status': newStatus}, function(data){
			item.status = newStatus;
			item.isFound = newStatus == 'FOUND';
			if (newStatus == 'PENDING'){
				item.didSubstitute = false;
			}            
          });
	};
	$scope.toggleSubstitute = function toggleSubstitute(item){

		//this only gets hit once we've already found the item, so PENDING is not an option, hence the ternary 
		var newStatus = item.didSubstitute ?'SUBSTITUTE':'PENDING';
		console.log('newStatus : ' + newStatus);
		HttpService.patchRequest('api/item/change/', {'item_id': item.id, 'status': newStatus}, function(data){
			item.status = newStatus;     
			item.didSubstitute = newStatus == 'SUBSTITUTE';       
          });

	};

	$scope.confirmRefund = function (item) {

			    var modalInstance = $modal.open({
			      templateUrl: 'static/partials/modal-confirmRefund.html',
			      controller: 'RefundConfirmationInstanceCtrl',
			      resolve: {
			        item: function () {
			          return item;
			        }
			      }
			    });

			    modalInstance.result.then(function (item) {
			    	$scope.item = item;
			    }, function () {
			    });
	};

	$scope.orderStatusIcons = Order.getStatusIcons();

});

app.controller('RefundConfirmationInstanceCtrl', function ($scope, $modalInstance, item, HttpService, toaster) {

	$scope.item = item;

	$scope.buttonLabel = item.status == 'REFUND'? 'Yes' : 'Refund';
	$scope.message = item.status == 'REFUND'?  'Are you sure you want to cancel this refund?': 'Are you sure you want to refund this item?';


	function showConfirmation(newStatus){
	  		var didRefund = newStatus == 'REFUND'
	        toaster.pop(didRefund?'success':'info', didRefund?"Item successfully refunded":"Cancelled refund");
	    };

	$scope.toggleRefund = function(item){

	   		var newStatus = !item.didRefund ?'REFUND':'PENDING';
			HttpService.patchRequest('api/item/change/', {'item_id': item.id, 'status': newStatus}, function(data){
				
				if (data.success){
					item.status = newStatus;     
					item.didRefund = newStatus == 'REFUND';  
					showConfirmation(newStatus);
				} else {
					var errorMessage = "An error occurred in refunding this item: " + data.message;
					toaster.pop('warning', errorMessage);

				}
				$modalInstance.close();
				
	          });

   };

  $scope.close = function () {
    $modalInstance.close('cancel');
  };

});
