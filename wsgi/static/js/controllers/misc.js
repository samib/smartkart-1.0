'use strict';

app.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            case 11: // +CPPP####### -> CCC (PP) ###-####
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;

            case 12: // +CCCPP####### -> CCC (PP) ###-####
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;

            default:
                return tel;
        }

        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (country + " (" + city + ") " + number).trim();
    };
});

app.controller('driverRegisterCtrl', function($scope, $location, $http){
	$scope.above_18 = false;
	$scope.suspended_licence = false;
	$scope.monday = false;
	$scope.tuesday = false;
	$scope.wednesday = false;
	$scope.thursday = false;
	$scope.friday = false;
	$scope.saturday = false;
	$scope.sunday = false;

	$scope.register = function(){
		var driverDetails = new Object();
		driverDetails.fname = $scope.fname;
		driverDetails.lname = $scope.lname;
		driverDetails.email = $scope.email;
		driverDetails.phone = $scope.phone;
		driverDetails.above_18 = $scope.above_18;
		driverDetails.suspended_license = $scope.suspended_licence;
		driverDetails.hours = $scope.hours;
		driverDetails.monday = $scope.monday;
		driverDetails.tuesday = $scope.tuesday;
		driverDetails.wednesday = $scope.wednesday;
		driverDetails.thursday = $scope.thursday;
		driverDetails.friday = $scope.friday;
		driverDetails.saturday = $scope.saturday;
		driverDetails.sunday = $scope.sunday;

		//alert(JSON.stringify(driverDetails));
		$http.post('api/job/shopper/', JSON.stringify(driverDetails)).success(function(data){
			alert(JSON.stringify(data));
		}).error(function(data){
			alert("driver registration failed");
		});
		// alert("firstname= "+$scope.fname);
		// alert("lastname= "+$scope.lname);
		// alert("email= "+$scope.email);
		// alert("phone="+ $scope.phone);
		// alert("above_18= "+ $scope.above_18);
		// alert("suspended_licence ="+ $scope.suspended_licence);
		// alert("working hours= "+ $scope.workingHours);
		// alert("monday = "+ $scope.monday);
		// alert("tuesday = "+ $scope.tuesday);
		// alert("wednesday = "+ $scope.wednesday);
		// alert("thursday = "+ $scope.thursday);
		// alert("friday = "+ $scope.friday);
		// alert("saturday= "+ $scope.saturday);
		// alert("sunday= "+ $scope.sunday);
	};


});

app.controller('resetPasswdCtrl', function(HttpService, $scope, $location, toaster,NotificationCenter,$http){

	$scope.$on('didStartLoading', function(event) {
      
          $scope.isLoading = true;
    });

    $scope.$on('doneLoading', function(event) {
          $scope.isLoading = false;
    });

	$scope.reset = function(){

		if ($scope.confPasswd != $scope.newPasswd){
			toaster.pop('error', 'Invalid information', 'Your passwords do not match', 4000, 'trustedHtml');
			return;
		}

		if ($scope.confPasswd.length < 6){
			toaster.pop('error', 'Invalid information', 'Password must be at least 6 characters', 4000, 'trustedHtml');
			return;
		}

		NotificationCenter.post('didStartLoading');

		var temp_id = $location.search()['temp_id'];
		var token = $location.search()['token'];

		HttpService.patchRequest('api/user/reset/', 

			{'password': $scope.newPasswd, 'temp_id': temp_id, 'token':token}, 

			function(response){

				toaster.pop('success', 'Reset password', 'Your password has been changed', 4000, 'trustedHtml');

				$location.path('/login');
				
				NotificationCenter.post('doneLoading');
		});
	};

	$scope.change = function(){

		NotificationCenter.post('didStartLoading');

		$http({ method: 'PATCH', url: 'api/user/change/', data: {'cp': $scope.currentPassword, 'np': $scope.newPasswd}}).success(function(data){
			NotificationCenter.post('doneLoading',null);

			var toastMessage = data.success ? 'Changed password' : 'Error changing password';
			var toastType = data.success ? 'success' : 'error';

			toaster.pop(toastType, toastMessage, data.message, 3000, 'trustedHtml');
			
			['newPasswd', 'confPasswd','currentPassword'].forEach(function(field){
				delete $scope[field];
			});

			$scope.changePasswordForm.$setPristine();

		});

	};
	
});

app.controller('backCtrl', function($scope){
	$scope.back = function() {
		window.history.back();
	}
});