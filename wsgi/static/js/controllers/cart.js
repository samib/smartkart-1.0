app.controller('CartCtrl', function (NotificationCenter,$scope,Store,$rootScope,AddItemNote,Sorting,cart,storeInfo,TAX_RATE,$stateParams,Cart,$modal,$location,$http,$cookies,CacheManager) //Product,Store and Cart are services
{   
    $scope.initializeJQuery = function(){
        totalsSlider();
    }

    // just in case...
    delete $rootScope.cards;

    // Pointer to the cart
    var _cart = cart[0];

    // Notify other controllers who need cart info
    NotificationCenter.post('cartLoaded', cart);

    $rootScope.cart_id = _cart.cart_id;

    // Util method to get a cart item for a given product
    function getCartItem(productId){
        var cartItem;
        $scope.cartItemsByStore.forEach(function(store){

            store.cartItems.forEach(function(item){

                if (item.product.id == productId) {
                    cartItem = item;
                }

            });

        });
        return cartItem;
    }

    // Util method to get the total number of cart items
    function getCartItemCount(){
        var itemCount = 0
        $scope.cartItemsByStore.forEach(function(store){
            itemCount += store.cartItems.length;
        });
        return itemCount;
    }

    // Util method to update and sort/group the cart
    function sortCartItemsByStore(){

        var allCartItems = [];
        $scope.cartItems.forEach(function(item){
            allCartItems.push(item);
        });
        $rootScope.allCartItems = allCartItems;

        $scope.cartItemsByStore = Sorting.groupCartItemsByStore($scope.cartItems,storeInfo);
        
    };

    // Subtotal
    function getSubtotal(){
        var subtotal = 0;
        $scope.cartItemsByStore.forEach(function(store){

            store.cartItems.forEach(function(cartItem){
                subtotal +=  (parseFloat(cartItem.product.price) * cartItem.quantity);
            });
        });
        return subtotal;
    }

    // Total delivery fee
    function getTotalDeliveryFee() {

        var totalDeliveryFee = 0;
        $scope.cartItemsByStore.forEach(function(store){
            if (store.totalForStore < parseFloat(store.min_order_for_free_delivery)){
                totalDeliveryFee += parseFloat(store.delivery_fee);

            }
        });
        return totalDeliveryFee;
    }

    // Total tax
    function getTotalTax(){
      
        var totalTax = 0;
        $scope.cartItemsByStore.forEach(function(store){

                var taxForThisStore = 0;
                store.cartItems.forEach(function(cartItem){

                    if (cartItem.product.taxable)
                    {
                        taxForThisStore += parseFloat(cartItem.product.price * cartItem.quantity) * TAX_RATE ;
                    }
                    
                });

                totalTax += taxForThisStore;
            });

        // Delivery fee is also taxable
        // totalTax += (getTotalDeliveryFee() * TAX_RATE);

        return totalTax;

    }

    // Total
    function getTotal(){
        // add subtotal and tax
        return parseFloat(getSubtotal() /*+ getTotalDeliveryFee()*/ + getTotalTax());
    }

    // Used to manage showing the Free! label
    function hasStoreInCartUnderMinimumForFreeDelivery(){

        var isStoreGoodForFreeDelivery = function(store){
          return store.totalForStore > store.min_order_for_free_delivery;
        };
        
        var cartItemsByStore = $scope.cartItemsByStore;
        return cartItemsByStore ? !cartItemsByStore.every(isStoreGoodForFreeDelivery) : false;
    }

    // Initialize the cart
    $scope.cartItems = _cart.items; // unsorted version

    // Group and sort the cart items
    sortCartItemsByStore();

    // Listener delegate for adding new items
    $scope.$on('didAddCartItem', function(event, newItem) {
        
        $scope.cartItems.push(newItem);
        sortCartItemsByStore();

    });

    // Observer for cart totals
    $scope.$watch('cartItemsByStore',function(cartItemsByStore){

       //console.log('CartItemsByStore : ' + JSON.stringify(cartItemsByStore));

        // update per-store totals, difference amounts and other meta/helper properties
        $scope.cartItemsByStore.forEach(function(store){

                // update total for this store
                var totalForStore = 0;
                store.cartItems.forEach(function(cartItem){

                    totalForStore += (parseFloat(cartItem.product.price) * cartItem.quantity);

                });
                
                store.totalForStore = totalForStore;

                store.differenceAmountForFreeDelivery = store.min_order_for_free_delivery - totalForStore;
                store.isDeliveryFree = totalForStore >= store.min_order_for_free_delivery && store.is_free_delivery_active;
                store.hasReachedMinOrderForStore = totalForStore >= parseFloat(store.minimum_order);
                
            });

        for(var store in $scope.cartItemsByStore){
        	var totalForStore = 0;
        	$scope.cartItemsByStore[store].cartItems.forEach(function(cartItem){
				totalForStore += (parseFloat(cartItem.product.price) * cartItem.quantity);
            });

            $scope.cartItemsByStore[store].totalForStore = totalForStore;
            $scope.cartItemsByStore[store].hasReachedMinOrderForStore = totalForStore >= parseFloat($scope.cartItemsByStore[store].minimum_order);
            $scope.cartConditionsSatisfied = false;
            if($scope.cartItemsByStore[store].hasReachedMinOrderForStore){
            	$scope.cartConditionsSatisfied = $scope.cartItemsByStore[store].hasReachedMinOrderForStore;
            	break;
            }
        }

        var numberOfCartItems = getCartItemCount();

        // Update subtotal, delivery fees
        $scope.subtotal = numberOfCartItems ? getSubtotal() : 0;
        $scope.totalDeliveryFee = numberOfCartItems ? getTotalDeliveryFee() : 0;
        $scope.totalTax = numberOfCartItems ? getTotalTax() : 0;

        // Update total and notify observer (SidenavCtrl is a listener)
        $scope.total = numberOfCartItems ? $scope.subtotal /*+ $scope.totalDeliveryFee*/ + $scope.totalTax : 0;
        
        // notify listener(s)
        NotificationCenter.post('didUpdateCartTotal', { total : $scope.total, numberOfCartItems : numberOfCartItems});
        
        // Update total tax + helper property (deprecated property?)
        $scope.hasStoreInCartUnderMinimumForFreeDelivery = hasStoreInCartUnderMinimumForFreeDelivery();



    });        
    
    $scope.cancelAddNote = function(cartItem){
        cartItem.addingNote = false;
    };

    $scope.saveAddNote = function(cartItem){
        AddItemNote.addNote(cartItem.id, cartItem.note, function(data){
            cartItem.addingNote = !cartItem.addingNote;
        });  
    };

    $scope.removeCartItem = function(cartItem){
        Cart.removeProductFromCart(cartItem);
    };

    // Delegate for removing items
    $scope.$on('didRemoveCartItem', function(event, removedItem) {

        $scope.cartItems.splice($scope.cartItems.indexOf(removedItem),1);

        sortCartItemsByStore();


    });

	$scope.increaseItemQuantity = function(cartItem){
		
 		Cart.increaseItemQuantity(cartItem, function(response){
 			 	
                cartItem.modifying = false;

                if (response.success) {
                    NotificationCenter.post('didIncreaseCartItemQuantity', cartItem);
                }

        });
	};

    // Delegate for increasing quantity
    $scope.$on('didIncreaseCartItemQuantity', function(event, cartItem) {
        var item = getCartItem(cartItem.product.id);
        item.quantity++;
        sortCartItemsByStore();
    });

    $scope.decreaseItemQuantity = function(cartItem){

        if (cartItem.quantity < 2) return;

        Cart.decreaseItemQuantity(cartItem, function(response){
               //console.log('response : ' + JSON.stringify(response));
                // TODO: put common callbacks in service 
                if (response.success){
                    NotificationCenter.post('didDecreaseCartItemQuantity', cartItem);
                }
        });
    };
    // Delegate event for decreasing quantity
    $scope.$on('didDecreaseCartItemQuantity', function(event, cartItem) {
        var item = getCartItem(cartItem.product.id);
        if (item.quantity < 2) return;
        item.quantity--;
        sortCartItemsByStore();
    });

    function confirmCheckout(incompleteStores) {

            var modalInstance = $modal.open({
              templateUrl: 'static/partials/modal-confirmCheckout.html',
              controller: 'CheckoutConfirmationInstanceCtrl',
              resolve: {

                incompleteStores: function () {
                          return incompleteStores;
                        }
                
              }
            });

            modalInstance.result.then(function (incompleteStores) {
                $scope.incompleteStores = incompleteStores;
                
            }, function () {
            });
        };
        
    $scope.checkout = function(){
            var lessThanMinimum = false; // true IFF there is at least one store in the cart where min order not reached
            var incompleteStores = []; // array of stores for which lessThanMinimum is true

            // Check for stores that are under the minimum order
            $scope.cartItemsByStore.forEach(function(store){
                if (store.totalForStore < store.minimum_order){
                    lessThanMinimum = true;
                    incompleteStores.push({ id : store.id, name : store.name});
                }
            });

            if (lessThanMinimum){
                // confirm if these items should be removed
                confirmCheckout(incompleteStores);
            } else {

                NotificationCenter.post('didStartLoading',null);

                $http.defaults.headers.post["Content-Type"] = "application/JSON";
                $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;

                // Remove previously associated timeslot
                $http.post('api/delivery_slot/remove/', 
                    { cart_id : $rootScope.cart_id}).success(function(response){

                        CacheManager.remove('checkoutStepIndex');
                        $location.path('/process/checkout');

                    });

                
            }
        }

});

app.controller('CheckoutConfirmationInstanceCtrl', function (NotificationCenter,$scope, $modalInstance, incompleteStores, $location,toaster,CartResource,Cart)
{
  var incompleteStoresString = '';

  incompleteStores.forEach(function(incompleteStore){
      if (incompleteStores.indexOf(incompleteStore) == incompleteStores.length - 1){
          incompleteStoresString += incompleteStore.name; 
      } else {
          incompleteStoresString += incompleteStore.name + ', '; 
      }
      
  });

  $scope.message = 'You have one or more stores in your cart (' + incompleteStoresString + ') with a subtotal less than the minimum order for that store. Confirm to proceed to checkout (' + incompleteStoresString + ' cart items will be discarded) or cancel to continue shopping.';

  $scope.continueCheckout = function(){

      // Get the cart and remove every item from incomplete stores
      CartResource.retrieveCart(function(response){

            var cartItems = response[0].items;
            incompleteStores.forEach(function(incompleteStore){

                cartItems.forEach(function(cartItem){
                        if (cartItem.product.store_id == incompleteStore.id)
                        {
                            Cart.removeProductFromCart(cartItem);
                        }
                    });
                toaster.pop('info', "Removed cart items for " + incompleteStore.name);
                $location.path('/process/checkout');
            });
      });

      $modalInstance.close();
      
  };

  $scope.close = function () {
    $modalInstance.close('cancel');
  };

});


