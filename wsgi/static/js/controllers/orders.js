app.controller('UserOrderListCtrl', function(CacheManager,$scope,$http, Order, $rootScope, EditPhoneNumber, toaster, myOrders){

	$scope.cancelPhoneNumber = function(orderPhone){
         orderPhone.editPhone = false;
    };

    $scope.savePhoneNumber = function(orderPhone){
    	EditPhoneNumber.EditPhone(orderPhone.id, orderPhone.phone, function(response){

            if (response.success){

                orderPhone.editPhone = !orderPhone.editPhone;
            } else {

                toaster.pop('error', 'Invalid phone number', response.message, 5000, 'trustedHtml');
            }            
        });  
    };

    // Injected through resolve
    $scope.orders = myOrders;  

    // Get locally cached store selection in case of a page refresh
    var storeSelection = ['selectedStore','selectedDepartment','selectedAisle'];
    storeSelection.forEach(function(property){
    	if (!$rootScope[property]){
    		$rootScope[property] = CacheManager.get(property);
    	}
    });
 
});

app.controller('UserOrderCtrl', function($scope, thisOrder, DateHelper, Order){

	$scope.orderStatusIcons = Order.getStatusIcons();

	$scope.thisOrder = thisOrder;

    if (thisOrder.discount_tax_deduction){
        $scope.thisOrder.tax = Number(thisOrder.tax) - Number(thisOrder.discount_tax_deduction);
    }

    $scope.thisOrder.totalWithTip = parseFloat(thisOrder.total_price) + parseFloat(thisOrder.tip) + parseFloat(thisOrder.delivery_charge) + parseFloat($scope.thisOrder.tax);

    $scope.thisOrder.totalWithDiscount = $scope.thisOrder.totalWithTip - parseFloat($scope.thisOrder.discount);

    $scope.thisOrder.deliveryTimeDisplayName = DateHelper.convert24hrTime(thisOrder.from_delivery_time) + ' - ' + DateHelper.convert24hrTime(thisOrder.to_delivery_time);

    $scope.thisOrder.deliveryTimeDisplayName = $scope.thisOrder.deliveryTimeDisplayName.replace(/:00/g,' ');

});

app.controller('AdminOrderCtrl', function($scope, orderInfo, DateHelper, Order, Product, toaster){

    $scope.cancelCostPrice = function(item){
         item.editCostPrice = false;
    };

    $scope.saveCostPrice = function(item){

      Product.editCostPrice(item.product.id, item.product.cost_price, item.product.price, function(response){

            if (response.success){

                item.editCostPrice = !item.editCostPrice;
                item.price = Number(item.product.price) * Number(item.quantity);
                toaster.pop('success', 'Success', 'Updated product price', 3000, 'trustedHtml');
            } else {

                toaster.pop('error', 'Error editing price', response.message, 3000, 'trustedHtml');
            }            
        });  
    };

    $scope.orderStatusIcons = Order.getStatusIcons();

    $scope.orderInfo = orderInfo;

    if (orderInfo.discount_tax_deduction){
        $scope.orderInfo.tax = Number(orderInfo.tax) - Number(orderInfo.discount_tax_deduction);
    }

    $scope.orderInfo.totalWithTip = parseFloat(orderInfo.total_price) + parseFloat(orderInfo.tip) + parseFloat(orderInfo.delivery_charge) + parseFloat($scope.orderInfo.tax);

    $scope.orderInfo.totalWithDiscount = $scope.orderInfo.totalWithTip - parseFloat($scope.orderInfo.discount);

    $scope.orderInfo.deliveryTimeDisplayName = DateHelper.convert24hrTime(orderInfo.from_delivery_time) + ' - ' + DateHelper.convert24hrTime(orderInfo.to_delivery_time);

    $scope.orderInfo.deliveryTimeDisplayName = $scope.orderInfo.deliveryTimeDisplayName.replace(/:00/g,' ');

});

app.controller('AdminOrdersCtrl', function($scope, $location,$http, $cookies, Order, $rootScope, DateHelper, NotificationCenter,$filter){
	
    //prototypical inheritance, needed so that the DatePicker (child) scope can set and access this property
    $scope.deliveryDate = {};

    // Get orders for today
    var today = new Date();
    $scope.deliveryDate.date = today;
    Order.retrieveOrdersByDay(DateHelper.cleanDate(today),function(data){
                $scope.ordersByDay = data.objects;
                $scope.deliveryDate.selectedDateString = 'Today';
            });

    

	$scope.getOrdersForDay = function() {
            NotificationCenter.post('didStartLoading',null);
			//get list of orders
            $scope.deliveryDate.selectedDateString = $filter('date')($scope.deliveryDate.date,'fullDate');
			Order.retrieveOrdersByDay(DateHelper.cleanDate($scope.deliveryDate.date),function(data){
                NotificationCenter.post('doneLoading',null);
				$scope.ordersByDay = data.objects;
                
			});
	};	
});
