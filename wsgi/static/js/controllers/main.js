app.directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
});

'use strict';
// TODO: clean up unused injections
var LOBLAWS_STORE_ID = 1;
var LOBLAWS_ORGANIC_DEPT_ID = 38;
var BEER_STORE_ID = 5;

app.controller('SideNavCtrl', function ($scope,Store,$stateParams,storeInfo,defaultStoreIndex,UserInfoHelper,$rootScope,defaultHomePath,Sorting,StoreHelper,CacheManager,$cacheFactory,$cookieStore,$state,$location) {    
    /*
        stateProvider in module config resolves storeInfo,userInfo, 
        cartItemCount and cartTotal etc. before this controller is instantiated
    */

    $scope.isLoadingAisle = false;

    $scope.defaultHomePath = defaultHomePath;
    // TODO: remove this
    $scope.$on('$viewContentLoaded', function(){
          $.getScript("/static/js/side-menu.js");
     });

    // Apply resolved data and set a default store selection
    $scope.userInfo =  {

      userName :  UserInfoHelper.getUserName(),
      isAdmin  :  UserInfoHelper.isAdmin(),
      isDriver :  UserInfoHelper.isDriver()

    };

    $scope.allStores = storeInfo.objects;

    $scope.$on('selectedStore', function(event, selectedStore) {
          /* 
              we cant access $stateParams.storeId because that param belongs
              to ProductListCtrl for another state. ProductList sends an event
              to set the store when the state is loaded.
          */

          $rootScope.selectedStore = selectedStore;

          var unsortedDpts = $rootScope.selectedStore.departments;
          var sortedDepartments = [];

          if (selectedStore.id == LOBLAWS_STORE_ID){
                // Special sorting to get Organic section to show as the first dpt
                var organicDept;
                unsortedDpts.forEach(function(department){

                    if (department.id == LOBLAWS_ORGANIC_DEPT_ID){
                      organicDept = department;
                    }

                });

                sortedDepartments[0] = organicDept;
                unsortedDpts.forEach(function(department){
                  if (department.id != LOBLAWS_ORGANIC_DEPT_ID){
                    sortedDepartments.push(department);
                  }

                });

                $rootScope.sortedDepartments = sortedDepartments;
                
          } else {

              $rootScope.sortedDepartments = unsortedDpts;

          }

          CacheManager.cache('selectedStore', selectedStore);

    });

    $scope.$on('cartLoaded', function(event, cart) {
          /* 
              Receive cartInfo as a notification from CartCtrl
          */
          
          // Compute cart total
          var cartItemsByStore = Sorting.groupCartItemsByStore(cart[0].items,storeInfo);
          var total = 0;
          cartItemsByStore.forEach(function(store){
              var totalForStore = 0;
              store.cartItems.forEach(function(cartItem){
                totalForStore += parseFloat(cartItem.price);
              });

              if (totalForStore <= store.min_order_for_free_delivery){
                totalForStore += parseFloat(store.delivery_fee);
              }

              total += totalForStore;
              
            });

          $scope.cartTotal = total;

          // Item count
          $scope.cartItemCount = cart[0].items.length;
    });

    $scope.$on('didStartLoading', function(event) {
      
          $scope.isLoadingAisle = true;
    });

    $scope.$on('doneLoading', function(event) {
          $scope.isLoadingAisle = false;
    });

    
    // Listen to any relevant cart changes
    $scope.$on('didUpdateCartTotal', function(event, cartInfo) {
        $scope.cartItemCount = cartInfo.numberOfCartItems;
        $scope.cartTotal = cartInfo.total;
    });
    
    $scope.switchToStore = function(store){

        // Save store selection
        var stores = $scope.allStores;
        var storeIndex = stores.indexOf(store);
        $rootScope.selectedStore = stores[storeIndex];
        CacheManager.cache('selectedStore', $rootScope.selectedStore);

        // Navigate to first department and aisle in that store
        if (store){
            var defaultDpt = store.departments[0];
            var defaultAisle = defaultDpt.aisles[0];
            $location.path('/home/store/' + store.id + '/department/' + defaultDpt.id + '/aisle/' + defaultAisle.id);
        }
        
    }

    $scope.switchToAisle = function(aisle){

        $rootScope.selectedAisle = StoreHelper.getAisleById(aisle.id);

        var eventDesc = UserInfoHelper.getUserName() + ' entered aisle ' + $rootScope.selectedAisle.aisle_name + ' in ' + $rootScope.selectedStore.name;
        ga('send', {
                    hitType: 'event',
                    eventCategory: 'Aisle navigation',
                    eventAction: UserInfoHelper.getUserName() + ' entered a new aisle',
                    eventLabel:  eventDesc
                  });
        CacheManager.cache('selectedAisle', aisle);
    }

    $scope.switchToDpt = function(dpt){

        // Implement this function if we need to highlight the department in the future. Stubbing for now
        return;
        if(dpt.id == selectedDepartment.id) return;

        $rootScope.selectedDepartment = StoreHelper.getDepartmentById(dpt.id);
        CacheManager.cache('selectedDepartment', dpt);
        
    }
    
});

app.controller('ProductListCtrl', function (anchorSmoothScroll,$rootScope,$scope,$stateParams,Cart,selectedAisle,searchResults,NotificationCenter,selectedStore,selectedDpt,ProductHelper, ProductsInAisle,productData,CacheManager,UserInfoHelper,sortedProducts,browser) {
    
    $scope.currentPage = 1;
    $scope.pageSize = 140;

    anchorSmoothScroll.scrollTo('product-list-view');

    $scope.isOnFirefox = browser() == 'firefox';

    $scope.isFirstOrder = UserInfoHelper.isFirstOrder();

    $scope.isLoadingAisle = false;

    // Load resolved data into the model
    if (productData) /* If productData is null, we are in search mode */
    {
        //$scope.productsInAisle = productData.objects;
        $scope.productsInAisle = sortedProducts;
        ProductHelper.premarkCartQuantities($scope.productsInAisle,$rootScope.allCartItems);
    }

    if ($scope.productsInAisle){
          $scope.productsInAisle.forEach(function(product){
              product.price = Number(product.price);
            });
    }

    // Aisle selection
    $rootScope.selectedStore = selectedStore;
    $rootScope.selectedAisle = selectedAisle;
    $rootScope.selectedDepartment = selectedDpt;
    CacheManager.cache('selectedDepartment',selectedDpt); // Necessary for highlighting selection
    CacheManager.cache('selectedAisle',selectedAisle);

    // Search results, if any
    $scope.searchResults = searchResults;

    // let our navigation know which store is loaded, since the storeId stateParam is restricted to this state
    NotificationCenter.post('selectedStore', selectedStore);

    // Listeners use this flag to know which model to update (scope this?). 
    var _isSearching = $scope.searchResults;

    if (_isSearching){
      $scope.queryString = $stateParams.queryString;
    }

    // Util method for getting cart item for a given product
    function getCartItem(productId){
        var cartItem;
        $rootScope.allCartItems.forEach(function(item){

          if (item.product.id == productId){
            cartItem = item;
          }
        });

        return cartItem;
    }

    function getProduct(productId){
        var product;

        if (_isSearching){

          $scope.searchResults.groupedSearchResults.forEach(function(store){
              store.resultsByAisle.forEach(function(aisle){
                  aisle.products.forEach(function(productInResults){
                      if (productInResults.id == productId){
                        product = productInResults;
                      }
                  });
              });
          });

        } else {

          $scope.productsInAisle.forEach(function(productInAisle){
            if (productInAisle.id == productId){
              product = productInAisle;
            }
          });

        }

        
        return product;
    }

    $scope.increaseQuantityForProduct = function(product){
      
      var cartItem = getCartItem(product.id);

      if (cartItem){
        Cart.increaseItemQuantity(cartItem,function(response){
          if (response.success){
              
              
              NotificationCenter.post('didIncreaseCartItemQuantity', cartItem);

          }
    
        })
        
      }
    };

    $scope.decreaseQuantityForProduct = function(product){
      
      var cartItem = getCartItem(product.id);

      if (cartItem && cartItem.quantity >= 2){

        Cart.decreaseItemQuantity(cartItem,function(response){

          if (response.success){

            //console.log('Decreased quantity for product : \n'+JSON.stringify(response));

              NotificationCenter.post('didDecreaseCartItemQuantity', cartItem);

          }
          
          
        });
      }
    };

    // Listener for increasing quantity
    $scope.$on('didIncreaseCartItemQuantity', function(event, cartItem) {
        var product = getProduct(cartItem.product.id)
        if (product){
            product.quantityInCart++;
        }
    });

    // Listener for decreasing quantity
    $scope.$on('didDecreaseCartItemQuantity', function(event, cartItem) {
        var product = getProduct(cartItem.product.id)
        if (product && product.quantityInCart >= 2){
            
            product.quantityInCart--;
        }
    });

    // Listener for removing a cart item
    $scope.$on('didRemoveCartItem', function(event, cartItem) {
        // Set quantity of product in scope to 0

        var removedProduct = getProduct(cartItem.product.id)

        if (removedProduct){
            removedProduct.quantityInCart = 0;
        } 
        
        $rootScope.allCartItems.splice($rootScope.allCartItems.indexOf(cartItem),1);
    });

    // Listener for adding a new cart item
    $scope.$on('didAddCartItem', function(event, newItem) {
        // Set quantity of product in scope to 0
        var product = getProduct(newItem.product.id)
        if (product){
            product.quantityInCart = 1;
        }
        $rootScope.allCartItems.push(newItem);

    });

    $scope.$on('didStartLoading', function(event) {
      
          $scope.isLoadingAisle = true;
    });

    $scope.$on('doneLoading', function(event) {
          $scope.isLoadingAisle = false;
    });

    $scope.$on('cartLoaded', function(event, cart) {

          $rootScope.cart_id = cart[0].cart_id;
          $rootScope.allCartItems = cart[0].items;

          ProductHelper.premarkCartQuantities($scope.productsInAisle,cart[0].items);
    });

    $scope.addProductToCart = function(product){
      product.quantityInCart = 1;
      Cart.addProductToCart(product,$rootScope.cart_id);

      // Fire GA Event
      var eventDesc = UserInfoHelper.getUserName() + ' added ' + product.name + ' (' + product.id + ')' + ' for $' + product.price + ' from ' + $rootScope.selectedStore.name;

      var aisleName = $rootScope.selectedAisle.aisle_name;
      var dptName = $rootScope.selectedDepartment.department_name;
      var storeName = $rootScope.selectedStore.name;
      
      // productFieldObject
      var productData = {
        'id': product.id,                 
        'name': product.name,
        'category': storeName + '/' + dptName + '/' + aisleName,
        'brand': product.brand_name,
        'price' : product.price,
        'list' : aisleName
      };

      ga('ec:addProduct', productData);
      ga('ec:setAction', 'add');

      ga('send', {
                  hitType: 'event',
                  eventCategory: 'Cart item',
                  eventAction: UserInfoHelper.getUserName() + ' added a cart item',
                  eventLabel:  eventDesc
                });
    }

    $scope.removeProductFromCart = function(product){
        var cartItem = getCartItem(product.id);

        if (cartItem){
            Cart.removeProductFromCart(cartItem);
        }
        
    };

});


app.controller('profilePageCtrl', function ($scope, $rootScope, $http, usSpinnerService, toaster, ownAddresses,cards,Payment,NotificationCenter,Address) {

  $scope.$on('didStartLoading', function(event) {
      
          $rootScope.isLoading = true;
    });

    $scope.$on('doneLoading', function(event) {
          $rootScope.isLoading = false;
    });

  $scope.deleteShippingAddress = function(address){

    NotificationCenter.post('didStartLoading',null);
    Address.deleteShippingAddress(address, function(data){

        //remove from our local cache of addresses
        $rootScope.ownAddresses.splice($rootScope.ownAddresses.indexOf(address),1);
        NotificationCenter.post('doneLoading',null);

     });

  };

  $scope.removeCard = function(card){
        NotificationCenter.post('didStartLoading',null);
        Payment.removeCard(card.id, function(data){
            
            if (data.success){
                $scope.cards.splice($scope.cards.indexOf(card),1);
                toaster.pop('success', 'Successfully removed card');

            }
            NotificationCenter.post('doneLoading',null);
                    
                
        }, function(error){
            NotificationCenter.post('doneLoading',null);
           //console.log('Error deleting card'+JSON.stringify(error));
        });
     };

    if (!ownAddresses){

        $rootScope.ownAddresses = [];

    } else {

        $rootScope.ownAddresses = ownAddresses.objects;

    }
    if (cards.data){
        if (cards.data.length){

        var myCards = new Array();
        cards.data.forEach(function(card){
                card.image = Payment.getImageForCardType(card.type);
                myCards.push(card);
            });
        
        $rootScope.cards = myCards;

        } else {

            $rootScope.cards = [];
        }
    } else {
      $rootScope.cards = [];
    }



});

app.controller('pageCtrl', function($scope, UserInfoHelper, $rootScope, defaultHomePath){
  
  $scope.defaultHomePath = defaultHomePath;

  $scope.userInfo =  {

      userName :  UserInfoHelper.getUserName(),
      isAdmin  :  UserInfoHelper.isAdmin(),
      isDriver :  UserInfoHelper.isDriver()

    };
  $scope.$on('didStartLoading', function(event) {
      
          $rootScope.isLoading = true;
    });

    $scope.$on('doneLoading', function(event) {
          $rootScope.isLoading = false;
    });
});
