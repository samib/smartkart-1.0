'use strict';

app.controller('loginCtrl', function($scope,$location,LoginService,defaultHomePath, usSpinnerService,$rootScope, StoreHelper,UserInfoHelper, $cookieStore, defaultStoreId) {
	
	$scope.isLoggingIn = false;

	$scope.$on('didStartLoading', function(event) {
      
          $scope.isLoggingIn = true;
    });

    $scope.$on('doneLoading', function(event) {
          $scope.isLoggingIn = false;
    });

    $rootScope.clearToken = function(){

    	$cookieStore.remove('userToken');
    }

	$scope.submit = function() {
		$scope.isLoggingIn = true;
		LoginService.login($scope.email, $scope.password).then(function(response) {

			var imGoinIn = function(){

				var lastStoreId = response.last_store?response.last_store:defaultStoreId;

				// Get last store's information
				var theStore = StoreHelper.getStoreById(lastStoreId);

				// Direct to first department and first aisle by default. Only last store is recorded server-side
				var defaultDpt = theStore.departments[0];
				var defaultAisle = defaultDpt.aisles[0];

				ga('send', {
							  hitType: 'event',
							  eventCategory: 'Login',
							  eventAction: response.userName + ' logged in',
							  eventLabel: Date()
							});

				$location.path('/home/store/' + lastStoreId + '/department/' + defaultDpt.id + '/aisle/' + defaultAisle.id);
			};
			
			response.email = $scope.email;

			// Record user info
			UserInfoHelper.recordUserInfo(response);

			// Get all store info before proceeding
			StoreHelper.recordStoreInfo(imGoinIn);

        }, function(error){
        	//this is fucked up, shouldnt be doing DOM manipulation in controllers
        	$scope.isLoggingIn = false;
        	var loginAlert = document.getElementById('login-alert');
    		loginAlert.style.display = 'block';
        });
        
	};
});

app.controller('signupCtrl', function(RegisterService,$scope, $location, defaultHomePath, UserInfoHelper,StoreHelper,toaster,CacheManager) {

	$scope.isLoggingIn = false;
	function imGoinIn(){
						$location.path(defaultHomePath);
						CacheManager.cache('checkoutStepIndex',0);
					};

	function getUserInfo(){
			var newUserInfo = {};
			var properties = ['firstName','lastName','email','password'];

			properties.forEach(function(property){
				newUserInfo[property] = $scope[property];
			});
			return newUserInfo;
		}

	$scope.signup = function(){
		$scope.isLoggingIn = true;
		var info = getUserInfo();
		RegisterService.register(info).then(function(response){
			if(response.success){
				toaster.pop('success', 'Welcome to Smartkart', 'Happy shopping!', 5000, 'trustedHtml');
				UserInfoHelper.recordUserInfo({

					isAdmin : false,
					isDriver : false,
					userName : response.userName,
					is_first_order : true,
					email:info.email

				});

				ga('send', {
							  hitType: 'event',
							  eventCategory: 'Registration',
							  eventAction: 'Signup',
							  eventLabel: response.userName + ' signed up with email: ' + info.email
							});

				// Get all store info before proceeding
				StoreHelper.recordStoreInfo(imGoinIn);

			} else {

				$scope.isLoggingIn = false;
				toaster.pop('error', 'Registration failed', response.message);


			}
		}, function(error){
			$scope.isLoggingIn = false;
			console.log('Error signing up : ' + JSON.stringify(error));
		});
	};

	$scope.$on('didStartLoading', function(event) {
      
          $scope.isLoggingIn = true;
    });

    $scope.$on('doneLoading', function(event) {
          $scope.isLoggingIn = false;
    });

});


app.controller('logoutCtrl', function($scope, $http, $location, $cookies, $rootScope, $cookieStore, $state,$cacheFactory, CacheManager,StoreHelper, NotificationCenter) {
	
	function clearInfo(){

        //Delete current state
        delete $state.current;

        // Delete user properties
        var userInfo = ['isAdmin','isDriver','userName','cards','allCartItems'];
        userInfo.forEach(function(property){

          delete $rootScope[property];

        });

        $cacheFactory.get('$http').remove('cart/show/')
        $cookieStore.remove('userToken');
        CacheManager.clearAll();

      }

    $scope.signout = function() {

	   	NotificationCenter.post('didStartLoading',null);

   		// Remember last store
   		if ($rootScope.selectedStore){

   			StoreHelper.setLastStoreVisited($rootScope.selectedStore.id);

   		}

   		// End session, clear cache
	    $http.get('api/user/logout/').success(function(data) {
	      	NotificationCenter.post('doneLoading',null);
	        clearInfo();
	        $location.path('/login');

	      });
    };

});

app.controller('onLoadCtrl', function($scope, $routeParams, $location, $element, $timeout) {

	$scope.navigateTo = function(navPath,formToHide) {
		$("#"+formToHide).hide("drop", {
			direction: "right",

		}, 600);
		 $timeout(function() {
		 	$location.path('/'+navPath)
		 }, 600);
	};

});

app.controller('postalCheckCtrl', function($scope, $http, $rootScope, $location, toaster) {
	$scope.postalValid = true;

	$scope.isValidatingPostalCode = false;

	$scope.checkPostalCode = function(){
		$scope.isValidatingPostalCode = true;
		$http.get('api/postalcode/check/?postal_code='+$scope.postalCode).success(function(response){
			$scope.isValidatingPostalCode = false;
			if(response.success){
				$location.path('/register');
			} else {
				//$scope.postalValid = false;
				//$scope.message = response.message;
				toaster.pop('note', 'Coming soon!', response.message);
			}
			
		}).error(function(response){
			console.log('Something went wrong : ' + JSON.stringify(response));
		});
	};

});

app.controller('forgotPassCtrl', function($scope, $http, $modal, NotificationCenter) {

	$scope.isResetting = false;

	$scope.$on('didStartLoading', function(event) {
      
          $scope.isResetting = true;
    });

    $scope.$on('doneLoading', function(event) {
          $scope.isResetting = false;
    });

	$scope.forgotPawd = function(){
		NotificationCenter.post('didStartLoading',null);
		var forgotPasswdAlert = document.getElementById('forgot-alert');
		forgotPasswdAlert.style.display = 'none';
		$scope.disableForgot = true;

		$http.post('api/user/forgot/', JSON.stringify({'email':$scope.email})).success(function(data){

			if(data.success){
				// add a popup message saying please check your email for further instructions
				NotificationCenter.post('doneLoading',null);
				var modalInstance = $modal.open({
			      templateUrl: 'static/partials/modal-forgotPasswd.html',
			      controller: 'forgotPasswdModalInstanceCtrl',
			      scope: $scope,
			      resolve: {
			        aisle: function () {
			          return $scope.email;
			        }
			      }
		    });
			} else {
				NotificationCenter.post('doneLoading', null);
				$scope.disableForgot = false;
				
    			forgotPasswdAlert.style.display = 'block';
			}
		}).error(function(data){
			usSpinnerService.stop('forgotPasswdSpinner');
			alert('failure');
		});
	};

});

app.controller('forgotPasswdModalInstanceCtrl', function ($scope, $modalInstance, $location) {   
	$scope.ok = function(){
		 $modalInstance.close($scope.email); 
		 $location.path('/login');
	}
          
});