'use strict';

var DELIVERY_CHARGE = 4.99;

app.controller('checkoutCtrl', function(defaultHomePath, Sorting, storeInfo, cart, $modal,$http, $scope, $q, $cookies, $rootScope, Order, Address, Cart, $location, Payment, DateHelper, toaster, Timeslot, usSpinnerService, $cacheFactory, subtotal, total, HttpService, NotificationCenter, discountData, CouponResource,ownAddresses,cards,totalTax,CacheManager,UserInfoHelper,isEmpty,StoreHelper,TAX_RATE,FREE_DELIVERY_COUPON_CODE){

    $scope.isLoading = false;

    $scope.isFirstOrder = UserInfoHelper.isFirstOrder();


    if ($scope.isFirstOrder == 'false'){
        $scope.isFirstOrder = false;
    }

    if (!ownAddresses){
        $rootScope.ownAddresses = [];

    } else {
        $rootScope.ownAddresses = ownAddresses.objects;
    }

    if (cards.data){
        if (cards.data.length){
            var myCards = new Array();
            cards.data.forEach(function(card){
                    card.image = Payment.getImageForCardType(card.brand);
                    myCards.push(card);
                });
            
            $rootScope.cards = myCards;
        } else {

            $rootScope.cards = [];
        }
    } else {

        $rootScope.cards = [];
    }

    // Update total when adding tip
    $scope.$watch('tipString', function(tip){
       var theTip = parseFloat($scope.tipString);

       if (!isNaN(theTip) && theTip > 0){
            $scope.tip = theTip;
       } else if (theTip < 0 || isNaN(theTip)) {
            $scope.tip = null;

       } else {
            $scope.total = total; // the original, injected total
       }

    });

    $scope.animateTip =  function(){
         $("#tipAmount").animate({'font-size': '200%', color:'rgb(139,175,61)', 'font-weight':'bolder'}, 1000, function(){
                $("#tipAmount").animate({'font-size': '100%', color:'#7d7d7d', 'font-weight':'bolder'}, 500);
            });
    }

    // Update total when adding tip percentage
    $scope.$watch('tipPercentage', function(tipPercentage){

        if (theTip == 'custom') return;

       var theTip = Number($scope.tipPercentage);

       if (!isNaN(theTip) && theTip > 0){
            $scope.tip = (theTip/100)*(Number($scope.subtotal) + Number($scope.totalTax) + Number($scope.totalDeliveryFee?$scope.totalDeliveryFee:0));
            


       } else if (theTip < 0 || isNaN(theTip)) {
            $scope.tip = null;

       } else {
            $scope.total = total; // the original, injected total
       }

    });

    $scope.tipPercentage = 15;

    $scope.customizeTip = function(){

        $scope.customTip = !$scope.customTip;
    }

    $scope.$on('didStartLoading', function(event) {
      
          $scope.isLoading = true;
    });

    $scope.$on('doneLoading', function(event) {
          $scope.isLoading = false;
    });

    $scope.hasChosenDeliveryDateAndTime = function(){

        var dateKeys = Object.keys($scope.deliveryDate); // keys are store ids
        var timeKeys = Object.keys($scope.deliveryTime); // ditto
        var invalidDate = false;
        var noTime = false;

        // For each store, check that date exists AND it is today or in the future
        dateKeys.forEach(function(dateKey){

            if (!$scope.deliveryDate[dateKey]){
                
                invalidDate = true;

            } else {

                var dateForThisStore = Date.parse($scope.deliveryDate[dateKey].toString());
                invalidDate = !dateForThisStore || !DateHelper.checkDateIsTodayOrLater(dateForThisStore);

            }

        });

        // Check that a delivery time is entered for all stores
        if (timeKeys.length){
            timeKeys.forEach(function(timeKey){
            if (!$scope.deliveryTime[timeKey]) 
                noTime = true;
            });
        } else {

            noTime = true;

        }
        
        var hasEnteredDateForAllStores = dateKeys.length == $scope.cartItemsByStore.length && !invalidDate;
        var hasEnteredTimeForAllStores = timeKeys.length == $scope.cartItemsByStore.length && !noTime;

        return hasEnteredTimeForAllStores && hasEnteredDateForAllStores;

    }

    $scope.total = total;
    $scope.subtotal = subtotal;
    $scope.totalTax = totalTax;

    $scope.discountData = discountData;
    if (discountData && !discountData.success){
        discountData = null;
    }

    if (discountData){
        $scope.discount = parseFloat(discountData.discount);
    }

    // Set cart
    $scope.cart = cart;
    $scope.cartId = cart[0].cart_id;
    $scope.cartItemsByStore = Sorting.groupCartItemsByStore(cart[0].items, storeInfo);

    $scope.cartItemsByStore.forEach(function(store){

                // update total for this store
                var totalForStore = 0;
                var totalTaxForStore = 0;
                store.cartItems.forEach(function(cartItem){

                    totalForStore += (parseFloat(cartItem.product.price) * cartItem.quantity);

                    if (cartItem.product.taxable){
                        totalTaxForStore += Number(cartItem.product.price) * 0.13;
                    }
                    
                });
                
                store.totalForStore = totalForStore.toFixed(2);
                store.totalTaxForStore = totalTaxForStore.toFixed(2);
                
            });

    function prepareCheckoutStepEvent(){
        // Use ec:addProduct for each product in the shopping cart
        cart[0].items.forEach(function(cartItem){
            var product = cartItem.product;
            ga('ec:addProduct', {
              'id': product.id,
              'name': product.name,
              'category': product.category,
              'brand': product.brand_name,
              'price': product.price,
              'quantity': cartItem.quantity
            });
        });
    }

    prepareCheckoutStepEvent();

    ga('ec:setAction','checkout', {
        'step': 1,            // In GA a value of 1 indicates this action is first checkout step.
    });

    ga('send', {
                  hitType: 'event',
                  eventCategory: 'Checkout',
                  eventAction: UserInfoHelper.getUserName() + ' entered checkout Step 1 (Address)',
                  eventLabel:  Date()
                });

    $scope.$on('$viewContentLoaded', function(){
        if($location.path() == '/process/checkout'){
            var checkDOMLoaded = setInterval(function(){
                if (document.readyState == "complete"){
                    $('#checkoutWizard').bootstrapWizard({
                        tabClass: 'nav nav-pills',
                        onNext: function(tab, navigation, index) {
                            
                            // Fire Step 2 event in GA
                            prepareCheckoutStepEvent();

                            ga('ec:setAction','checkout', {
                                'step': 2,    
                            });

                            ga('send', {
                              hitType: 'event',
                              eventCategory: 'Checkout',
                              eventAction: UserInfoHelper.getUserName() + ' entered checkout Step 2 (Delivery)',
                              eventLabel:  Date()
                            });

                            CacheManager.cache('checkoutStepIndex',index);
                        },
                        onPrevious: function(tab, navigation, index) {
                            CacheManager.cache('checkoutStepIndex',index);
                        }
                    });

                    $('#checkoutWizard').bootstrapWizard('show',0);
                    $('.no-click').click(function(e) { return false; });
                    $.getScript("/static/js/input.js");
                    clearInterval(checkDOMLoaded);
                }
            },200);
            
        }
        
    });

    //For children controllers - time picker and date picker will add properties to these objects
    $scope.deliveryDate = {};
    $scope.deliveryTime = {};
    $scope.timeslots = {};
    $scope.noDeliveryTimeAvailableForStore = {};

    $scope.hasNotChosenDeliveryTime = function(){

            var ret = true;
            var hasOwnProperty = Object.prototype.hasOwnProperty;
            var deliveryTimesByStore = $scope.deliveryTime;

            if (deliveryTimesByStore == null) return true;

            if (deliveryTimesByStore.length > 0)    return false;
            if (deliveryTimesByStore.length === 0)  return true;

            for (var key in deliveryTimesByStore) {
                if (hasOwnProperty.call(deliveryTimesByStore, key) && deliveryTimesByStore[key] != ""){
                    ret = false;
                }
            }

            return ret;

    }


    $scope.hasNotChosenDeliveryDate = function(){
        return isEmpty($scope.deliveryDate);
    }

    function getTotalDeliveryFee(){


        var totalDeliveryFee = 0;

        $scope.cartItemsByStore.forEach(function(store){

            var deliveryOption = $scope.deliveryTime[store.id];
            var deliveryForThisStore = deliveryOption && $scope.deliveryDate[store.id] ? parseFloat(deliveryOption.substr(deliveryOption.indexOf("$") + 1)) : 0;

            totalDeliveryFee += Number(deliveryForThisStore);
        });

        return Number(totalDeliveryFee);

    }

    $scope.updateTaxAndDeliveryFee = function(){

        $scope.totalDeliveryFee = getTotalDeliveryFee();
        $scope.totalTax = totalTax + Number($scope.totalDeliveryFee) * TAX_RATE;
        $scope.total = total + $scope.totalTax;

    }

    $scope.removeCard = function(card){
        NotificationCenter.post('didStartLoading',null);
        Payment.removeCard(card.id, function(data){
            NotificationCenter.post('doneLoading',null);
            if (data.success){
                $rootScope.cards.splice($rootScope.cards.indexOf(card),1);
                toaster.pop('success', 'Successfully removed card');
                
            }
                    
        }, function(error){
            NotificationCenter.post('doneLoading',null);
        });
     };

    $scope.applyCoupon = function(couponCode){

        NotificationCenter.post('didStartLoading',null);
        HttpService.patchRequestWithErrorCallback('api/coupon/redeem/', {
            code: couponCode,
            cart_id: $scope.cartId,
            num_of_stores: $scope.cartItemsByStore.length
        }, function(response){

            NotificationCenter.post('doneLoading',null);

            if (response.success)
            {   
                $scope.originalDeliveryFee = getTotalDeliveryFee();

                $rootScope.hasAppliedCoupon = true;
                $scope.discount = response.discount;
                $scope.randomHash = response.random_hash

                // this line updates the ui since ng-shows/hides are dependent on this scope property
                $scope.discountData = response;

                if (response.discount_tax_deduction){
                    $scope.totalTax -= Number(response.discount_tax_deduction);
                    $scope.discount_tax_deduction = response.discount_tax_deduction;
                }

                if ($scope.isFirstOrder){
                    $scope.totalDeliveryFee = 0;
                }

                toaster.pop('success', response.message);
            } else {

                toaster.pop('error', 'Error with your coupon', response.message, 5000, 'trustedHtml');

            }

        }, function(errorResponse){

            console.log('Applying coupon failed : ' + JSON.stringify(errorResponse));

        });

    };

    $scope.hasAtLeastOneAvailableTimeslot = function(storeId){

        var deliveryOptions = $scope.timeslots[storeId];
        if (!isEmpty(deliveryOptions)){

            var hasAtLeastOneAvailableTimeslot = false;
            deliveryOptions.forEach(function(option){
                if (option.time.available){
                    hasAtLeastOneAvailableTimeslot = true;
                }
            });

            return hasAtLeastOneAvailableTimeslot;

        } else {
            return false;
        }
    }

    $scope.updateAvailableTimeslots = function(storeId){
            NotificationCenter.post('didStartLoading', null);
            var cleanDate = DateHelper.cleanDate($scope.deliveryDate[storeId]);

            if (cleanDate){

                Timeslot.getAvailableSlotsForDate(cleanDate, storeId, function(response){

                    NotificationCenter.post('doneLoading',null);
                    $scope.timeslots[storeId] = response[0].delivery_options;
                    $scope.deliveryTime[storeId] = "";

                    $scope.noDeliveryTimeAvailableForStore[storeId] = !response[0].success;
                    
                }); 

            } else {

                NotificationCenter.post('doneLoading', null);
            }
    }



    //prototypical inheritance again - need this to get its id
    $rootScope.chosenAddress = {};

    //We initialize to 0, which means the customer enters a new credit card by default
    $scope.selectedCard = { id : 0 };

    var defer = $q.defer();

    Stripe.setPublishableKey('pk_live_Fh15UAHTLP2r5pSEYvmesVR2');

    function getTimeFromName(deliveryTimeDisplayName,storeId){
        var time;
        $scope.timeslots[storeId].forEach(function(timeslot){

            if (timeslot.time.display_name == deliveryTimeDisplayName.trim())
                time = timeslot.time;

        });

        return time;

    }

    $scope.didPickTimeslotForStore = function(timeslotName,storeId){

        $scope.updateTaxAndDeliveryFee();
        

        var selectedTimeInfo;
        $scope.timeslots[storeId].forEach(function(timeslot){

            if (timeslot.time.display_name == timeslotName.trim()){
                selectedTimeInfo = timeslot.time;
            }

        });

        $http.defaults.headers.post["Content-Type"] = "application/JSON";
        $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
        $http.post('api/delivery_slot/pick/', 
                        {   cart_id : $scope.cartId,
                            store_id : storeId,
                            is_one_hour: selectedTimeInfo.is_one_hour,
                            is_two_hour: selectedTimeInfo.is_two_hour,
                            is_three_hour: selectedTimeInfo.is_three_hour
                        }
          ).success(function(response){

          });


    }

    // Load previous checkout information

    $rootScope.chosenAddress.id = ownAddresses.objects.length?ownAddresses.objects[0].id:CacheManager.get('selectedAddress');

    $scope.phone = CacheManager.get('phoneNumber');
    //$scope.tip = CacheManager.get('tip');
    $scope.deliveryInstructions = CacheManager.get('deliveryInstructions');

    $scope.saveAddress = function(){
        CacheManager.cache('selectedAddress', $rootScope.chosenAddress.id);
    }

    $scope.savePhoneNumber = function(){
        CacheManager.cache('phoneNumber', $scope.phone);
    }

    $scope.saveTip = function(){
        CacheManager.cache('tip', $scope.tip);
    }

    $scope.saveInstructions = function(){
        CacheManager.cache('deliveryInstructions', $scope.deliveryInstructions);
    }

    //callback for when payment is successfully processed
    function createOrder(){
        //for every store in cartItemsByStore, create an order
        angular.forEach($scope.cartItemsByStore, function(store){
        
                var numberOfStores = $scope.cartItemsByStore.length;
                var deliveryTime = getTimeFromName($scope.deliveryTime[store.id],store.id);

                var orderInfo = new Object();
                orderInfo.cart_id = $scope.cartId;
                orderInfo.delivery_date = DateHelper.cleanDate($scope.deliveryDate[store.id]);
                orderInfo.from_delivery_time = deliveryTime.from_time;
                orderInfo.to_delivery_time = deliveryTime.to_time;
                orderInfo.shipping_address_id = $rootScope.chosenAddress.id;
                orderInfo.tip = $scope.tip?parseFloat($scope.tip)/numberOfStores:0;
                orderInfo.phone = $scope.phone;
                orderInfo.delivery_instructions = $scope.deliveryInstructions;
                orderInfo.store_id = store.id;
                orderInfo.items = store.cartItems;

                // x.99
                var deliveryOption = $scope.deliveryTime[store.id];
                var delivery_charge = parseFloat(deliveryOption.substr(deliveryOption.indexOf("$") + 1));


                /*
                if (discountData) {   

                    // For the case of having entered checkout with an already redeemed coupon
                    orderInfo.discount = discountData.dollar_amount?parseFloat(discountData.dollar_amount)/numberOfStores
                                        :parseFloat(discountData.discount)/numberOfStores;

                    orderInfo.discount = orderInfo.discount.toFixed(2);

                } else if ($scope.discount) {

                    // In this case, end user has just redeemed a coupon so take discount from the scope
                    orderInfo.discount = ($scope.discount/numberOfStores).toFixed(2);

                } else {

                    orderInfo.discount = 0;
                }*/


                NotificationCenter.post('didStartLoading',null);
                Order.createOrder(orderInfo, function(data){

                            NotificationCenter.post('doneLoading',null);

                            $.unblockUI();

                            var storeName = StoreHelper.getStoreById(orderInfo.store_id).name;
                                
                            var customerName = UserInfoHelper.getUserName();

                            var purchaseEventLabel = customerName + ' | ' + storeName + ' | ' + 'Subtotal: $' + subtotal + ' | Delivery date: ' + orderInfo.delivery_date + ' between ' + orderInfo.from_delivery_time + ' and ' + orderInfo.to_delivery_time;

                            if (data.success){
                                $cacheFactory.get('$http').remove('cart/show/');

                                $scope.orderCompleted = true;
                                
                                delete $rootScope.newCard;
                                delete $rootScope.saveCard;
                                delete $rootScope.allCartItems;
                                delete $rootScope.hasAppliedCoupon;

                                // Fire custom purchase event
                                ga('send', {
                                  hitType: 'event',
                                  eventCategory: 'Purchase',
                                  eventAction: 'Order placed',
                                  eventLabel: purchaseEventLabel
                                });

                                toaster.pop('info', 'Thank you!', 'Your ' + storeName + ' order will be delivered on ' + orderInfo.delivery_date + ' between ' + orderInfo.from_delivery_time + ' and ' + orderInfo.to_delivery_time, 17000, 'trustedHtml');
                                toaster.pop('success', storeName, 'Order received!', 7000, 'trustedHtml');
                                
                                if (UserInfoHelper.isFirstOrder()){
                                    UserInfoHelper.didCompleteFirstOrder();
                                    ga('send', {
                                              hitType: 'event',
                                              eventCategory: 'Customer acquisition',
                                              eventAction: 'First time purchase',
                                              eventLabel: customerName + ' | ' + UserInfoHelper.getEmail()
                                            });
                                }

                                // Fire Step 5 event in GA e-commerce
                                prepareCheckoutStepEvent();

                                ga('ec:setAction','checkout', {
                                    'step': 5,    
                                });

                                var transactionRevenue = Number(store.totalForStore) + Number($scope.totalDeliveryFee/numberOfStores) + Number(orderInfo.tip);

                                ga('ec:setAction', 'purchase', {
                                  'id': data.order_number,
                                  'affiliation': storeName,
                                  'revenue': transactionRevenue.toFixed(2),
                                  'tax': store.totalTaxForStore,
                                  'shipping': delivery_charge,
                                  'coupon': $scope.couponCode    // User added a coupon at checkout.
                                });

                                ga('send', {
                                  hitType: 'event',
                                  eventCategory: 'Checkout',
                                  eventAction: UserInfoHelper.getUserName() + ' completed checkout Step 5 (Order placed)',
                                  eventLabel:  Date()
                                });

                                $location.path('/process/order');

                                CacheManager.cache('checkoutStepIndex',1);

                            } else {
                                toaster.pop('info', 'Failed to create order', error.error_message, 11000, 'trustedHtml');
                                ga('send', {
                                  hitType: 'event',
                                  eventCategory: 'Order failure',
                                  eventAction: 'Order creation failed',
                                  eventLabel: 'Error: ' + error.error_message + (purchaseEventLabel) 
                                });
                            }
                        }, function(error){

                            NotificationCenter.post('doneLoading',null);
                            ga('send', {
                                  hitType: 'event',
                                  eventCategory: 'Order failure',
                                  eventAction: 'Order creation failed',
                                  eventLabel: 'Error: ' + JSON.stringify(error) + (purchaseEventLabel) 
                                });
                            l.stop();
                            $.unblockUI();
                                toaster.pop('info', 'Failed to complete your order', 'Could not process your order at this time. Please contact customer service.', 7000, 'trustedHtml');

                        });
            
        });

    };

    $scope.today = function(storeId){
        $scope.deliveryDate[storeId] = new Date();
        $scope.updateAvailableTimeslots(storeId);
    }

    var handler = StripeCheckout.configure({
        key: 'pk_live_Fh15UAHTLP2r5pSEYvmesVR2',
        image: 'https://s3.amazonaws.com/stripe-uploads/acct_158lmSIFKON3UWH0merchant-icon-1447025742441-small_logo.png',
        locale: 'auto',
        token: function(token) {

            // Fire Step 4 event in GA
            prepareCheckoutStepEvent();

            ga('ec:setAction','checkout', {
                'step': 4,    
            });

            ga('send', {
              hitType: 'event',
              eventCategory: 'Checkout',
              eventAction: UserInfoHelper.getUserName() + ' started checkout Step 4 (Payment attempt)',
              eventLabel:  Date()
            });

            // True IFF the user is not using a saved credit card
            var _isNewCard = typeof $rootScope.newCard != 'undefined';

            // Build common payment info
            var paymentInfo =  {
                    tip:            $scope.tip?$scope.tip:0, 
                    cart_id:        $scope.cartId, 
                    save_card:      true,
                };

            // Add discount info
            if (discountData) {

                paymentInfo.random_hash = discountData.random_hash;

            } else if ($scope.randomHash) {

                paymentInfo.random_hash = $scope.randomHash;

            }

            paymentInfo.token = token.id;
            // Animate and attempt payment
            $.blockUI({ message: 'Creating your order...' });

            NotificationCenter.post('didStartLoading',null);

            Payment.pay(paymentInfo, $scope.selectedCard.id, _isNewCard, function(response){
                    if(response.success){
                        createOrder();
                    } else {

                        $.unblockUI();

                        var errorMsg = response.error_message?response.error_message:response.message;

                        toaster.pop('info', 'Failed to process payment', errorMsg,11000, 'trustedHtml');    

                    }
                    NotificationCenter.post('doneLoading',null); 
                    
                }, function(error){
                    NotificationCenter.post('doneLoading',null);

                    $.unblockUI();   

                    toaster.pop('info', 'Failed to process payment', 'Could not process your payment at this time.', 11000, 'trustedHtml');

                });
        }
      });

        $scope.didClickComplete = function(){

                    if ($scope.hasNotChosenDeliveryDate() || $scope.hasNotChosenDeliveryTime()){

                    toaster.pop('info', 'No delivery time selected', 'Please choose a valid delivery time and date', 7000, 'trustedHtml');
                    $('#checkoutWizard').bootstrapWizard('show',1);
                    return;
                    }

                    if (!$rootScope.chosenAddress.id){

                        toaster.pop('info', 'No address selected', 'Please select an address', 7000, 'trustedHtml');
                        $('#checkoutWizard').bootstrapWizard('show',0);
                        return;
                    }

                    if (!$scope.phone){
                        toaster.pop('info', 'No phone number provided', 'Please enter a valid phone number', 7000, 'trustedHtml');
                        $('#checkoutWizard').bootstrapWizard('show',1);
                        return;
                    }

                    var theTotal = $scope.subtotal + $scope.totalTax + $scope.tip - ($scope.discountData?$scope.discountData.dollar_amount:$scope.discount) + $scope.originalDeliveryFee;

                    // Open Checkout with further options
                    handler.open({
                      name: 'Smartkart',
                      description: 'Live. Love. Food.',
                      zipCode: true,
                      currency: "cad",
                      email: UserInfoHelper.getEmail(),
                      amount: theTotal * 100
                    });

                    // Fire Step 3 event in GA
                    prepareCheckoutStepEvent();

                    ga('ec:setAction','checkout', {
                        'step': 3,    
                    });

                    ga('send', {
                      hitType: 'event',
                      eventCategory: 'Checkout',
                      eventAction: UserInfoHelper.getUserName() + ' entered checkout Step 3 (Payment info)',
                      eventLabel:  Date()
                    });

        }

      // Close Checkout on page navigation
      $(window).on('popstate', function() {
        handler.close();
      });

});

app.controller('PaymentFailureModalInstanceCtrl', function ($scope, $modalInstance) {
    $scope.ok = function(){
        $.unblockUI();
        $modalInstance.close($scope.message);
    }   
});

