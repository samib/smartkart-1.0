app.controller('DepartmentListCtrl', function ($scope,$q, Department,Aisle,$routeParams,$rootScope) //Department is the service
{
	$scope.storeId = typeof $routeParams.storeId != 'undefined' ? $routeParams.storeId : '1';
		
	var defer = $q.defer();

	//fetch departments
	Department.get($scope.storeId,
		function(data){
			var departments = new Array();
			var counter = 0;
			var ncount = 0;

			data.forEach(function(department){
				department.fields.id = department.pk;
				departments.push(department.fields);
			});
			
			counter = departments.length;
			departments.forEach(function(department){

				//fetch and inject aisles for every department
				Aisle.getAislesForDepartment(department.id,
				function(aisleData){

					var aisles = new Array();
					aisleData.forEach(function(aisle){
						aisle.fields.id = aisle.pk;
						aisles.push(aisle.fields);
					});

					department.aisles = aisles;
					ncount++;
					if(counter == ncount){
						defer.resolve();
					}
				})


		});
								
				defer.promise.then(function(){
					$rootScope.departments = departments;
				})
				

		});

	;});

