'use strict';
app.controller('AdminCtrl', function($scope, $rootScope, Store, Driver, Refund, Sorting, Timeslot, toaster, DateHelper, usSpinnerService, CouponService, CouponResource, NotificationCenter, CacheManager, Order, OrderResource, recentOrders, $filter, priceChangeList,Product,ProductsInAisle) {

  $scope.priceChangeList = priceChangeList;
  $scope.approvePriceChange = function(product){

    Product.approvePriceChange(product.id, function(response){
        console.log(JSON.stringify(response));
        if (response.success){
            toaster.pop('success', 'Success', 'Updated product cost_price', 2000, 'trustedHtml');
            var index = $scope.priceChangeList.objects.indexOf(product);
            $scope.priceChangeList.objects.splice(index, 1);

        } else {
            toaster.pop('error', 'Error editing cost_price', response.message, 3000, 'trustedHtml');
        }
        
    });

  }

  //todo : check if user is Admin otherwise boot him out to the login page. done with routeChange mechanism?
  $scope.addingTimeslot = false;
  $scope.fullReport = false;
  $scope.timeslotDate = {};
  $scope.couponInfo = {};
  $scope.cleanDate = DateHelper.cleanDate;
  $scope.couponExpiryDate = {};
  $scope.orderDateRange = {};

  $scope.predicate = '-delivery_date';

  Driver.getAllDrivers(function(data){
    if (data.objects){
        $rootScope.drivers = data.objects;
        generateReport(recentOrders);
    } 

  });

  function generateReport(orders){
      var drivers = $rootScope.drivers;

      $scope.totalPayByDriverId = {};
      $scope.totalTipByDriverId = {};
      $scope.totalWageByDriverId = {};

      drivers.forEach(function(driver){
          $scope.totalPayByDriverId[driver.first_name] = 0;
          $scope.totalTipByDriverId[driver.first_name] = 0;
          $scope.totalWageByDriverId[driver.first_name] = 0;
      });

      $scope.grandTotalOrderPrice = 0;
      $scope.grandTotalMarkupProfit = 0;
      $scope.grandTotalExpectedProfit = 0;
      $scope.grandTotalCostPrice = 0;
      $scope.grandTotalRefunds = 0;

      $rootScope.allOrders = orders;

      $rootScope.allOrders.forEach(function(order){

            var cost_price = 0;
            var totalMarkupProfit = 0;
            var refund = 0; // customer-cost refund

            order.items.forEach(function(item){

              if (item.status != 'CANCELED' && item.status != 'REFUND'){
                item.profit = (Number(item.product.price) - Number(item.product.cost_price))*item.quantity;
                totalMarkupProfit += item.profit;
                cost_price += (Number(item.product.cost_price)*item.quantity);
              } else {
                refund += (Number(item.product.price) * item.quantity)
              }

            });

            order.refund = refund;

            if (order.discount_tax_deduction){
                order.tax = Number(order.tax) - Number(order.discount_tax_deduction);
            }

            order.totalWithTip = parseFloat(order.total_price) + parseFloat(order.tip) + parseFloat(order.delivery_charge) + parseFloat(order.tax);

            if (!(order.discount > 0)){
              order.totalWithDiscount = order.totalWithTip - order.refund;

            } else {
              order.totalWithDiscount = order.totalWithTip - parseFloat(order.discount) - order.refund;
            }

            order.cost_price = cost_price;
            order.totalPricePaid = Number(order.price_paid) + Number(order.tax_paid);
            
            order.stripeCharge = (0.029 * Number(order.totalWithDiscount) + 0.30)

            if (order.refund > 0){
              order.drivers_pay = Number(order.drivers_pay) - (0.05*order.refund)
            }

            order.totalMarkupProfit = order.totalPricePaid ? order.totalWithDiscount - order.totalPricePaid - Number(order.drivers_pay) - order.stripeCharge : order.totalWithDiscount - order.cost_price - Number(order.drivers_pay) - order.stripeCharge;

            order.expectedProfit = order.totalWithDiscount - order.cost_price - Number(order.drivers_pay) - order.stripeCharge;

            if (order.status != 'CANCELED'){
              $scope.grandTotalCostPrice += order.cost_price;
              $scope.grandTotalMarkupProfit += order.totalMarkupProfit;
              $scope.grandTotalExpectedProfit += order.expectedProfit;
              $scope.grandTotalOrderPrice += order.totalWithDiscount;
              $scope.grandTotalRefunds += order.refund;

              if (drivers){
                  drivers.forEach(function(driver){

                      if (order.assigned_driver && driver){
                          if (order.assigned_driver.id == driver.id){
                            $scope.totalPayByDriverId[driver.first_name] += Number(order.drivers_pay);
                            $scope.totalTipByDriverId[driver.first_name] += Number(order.tip);   
                            $scope.totalWageByDriverId[driver.first_name] += (Number(order.drivers_pay)-Number(order.tip)); 
                          }
                      }
                      
                  });
              }


            }
      });

      var numberOfOrders = orders.length;

      orders.forEach(function(order){

        if (order.status == 'CANCELED'){
          numberOfOrders--;
        }

      });

      $scope.orderCount = numberOfOrders;
      $scope.averageCostPrice = $scope.grandTotalCostPrice/numberOfOrders;
      $scope.averageTotalPrice = $scope.grandTotalOrderPrice/numberOfOrders;
      $scope.netProfitPercentage = (($scope.grandTotalMarkupProfit/$scope.grandTotalOrderPrice)*100).toFixed(2);
      $scope.expectedNetProfitPercentage = (($scope.grandTotalExpectedProfit/$scope.grandTotalOrderPrice)*100).toFixed(2);

  }

  $scope.getFullOrderReport = function(){
    NotificationCenter.post('didStartLoading',null);
    $scope.fullReport = true;
    OrderResource.get(function(response){
      generateReport(response.objects);
      NotificationCenter.post('doneLoading',null);
    });
  }

  $scope.refreshOrders = function(){
      $scope.fullReport = false;
      NotificationCenter.post('didStartLoading',null);
      var from = DateHelper.cleanDate($scope.orderDateRange.fromDate);
      var to = DateHelper.cleanDate($scope.orderDateRange.toDate);

      Order.getOrdersForRange(from,to,function(response){

          $scope.currentFromDate = $filter('date')($scope.orderDateRange.fromDate, 'fullDate');
          $scope.currentToDate = $filter('date')($scope.orderDateRange.toDate, 'fullDate');

          generateReport(response.objects);
          NotificationCenter.post('doneLoading',null);
      });
  }

  $scope.getFullOrderReport = function(){

    NotificationCenter.post('didStartLoading',null);
    OrderResource.get(function(response){

      generateReport(response.objects);
        
      NotificationCenter.post('doneLoading',null);

    });

  }

  $scope.cancelPricePaid = function(order){
         order.editPricePaid = false;
    };

  $scope.savePricePaid = function(order){
      Order.editPricePaid(order.id, order.price_paid, order.tax_paid, function(response){

            if (response.success){

                order.editPricePaid = !order.editPricePaid;
                order.totalPricePaid = Number(order.tax_paid) + Number(order.price_paid);
                order.drivers_pay = Number(response.drivers_pay);

                toaster.pop('success', 'Success', 'Updated price paid', 3000, 'trustedHtml');
            } else {

                toaster.pop('error', 'Error editing price paid', response.message, 3000, 'trustedHtml');
            }            
        });  
    };

  function retrieveCouponList(){
      NotificationCenter.post('didStartLoading',null);
      CouponResource.get(function(response){

        $scope.couponList = response.objects;
        NotificationCenter.post('doneLoading',null);

      });
    }

  retrieveCouponList();

  $scope.couponTypes = ['percentage', 'monetary', 'free_delivery', 'multiple_monetary', 'multiple_percentage'];

  $scope.generateCoupons = function(){
      NotificationCenter.post('didStartLoading',null);

      if (!$scope.couponInfo){
        toaster.pop('error', 'Error with coupons', 'Invalid coupon information', 3000, 'trustedHtml');
        NotificationCenter.post('doneLoading',null);
        return;
      }

      //add a coupon
     //console.log('coupon quantity: ' + $scope.quantity);
      CouponService.generateCoupons({

        quantity: Number($scope.couponInfo.quantity),
        value : Number($scope.couponInfo.value),
        type : $scope.couponInfo.type,
        expiry_date : DateHelper.cleanDate($scope.couponExpiryDate.date)

      }, function(response){

            NotificationCenter.post('doneLoading',null);

            if (response.success){
             //console.log('Success : ' + JSON.stringify(response));
              toaster.pop('success', 'Success', response.message, 5000, 'trustedHtml');
              retrieveCouponList();
            } else {

              toaster.pop('error', 'Error with coupons', response.message, 5000, 'trustedHtml');

            }
      }, function(response){
          NotificationCenter.post('doneLoading',null);

      });

    };

  $scope.addTimeslot = function(timeslot){
    //TODO BACKEND: should return id of newly created timeslot
    Timeslot.addTimeslot(timeslot,function(data){
        if (data.success){
          var newTimeslot = { display_name: timeslot, id : data.id }
          if ($scope.timeslots){
            $scope.timeslots.push(newTimeslot);
          } else {
            var timeslots = new Array();
            timeslots.push(newTimeslot);
            $scope.timeslots = timeslots;
          }

          toaster.pop('success', 'Success', data.message, 5000, 'trustedHtml');

        }
    });

  }

  $scope.orderBy = function(propertyName){
    $scope.predicate = propertyName;
  }

  $scope.globalStoreInfo = CacheManager.get('globalStoreInfo');

  //storeId,deliveryDate,timeToBlock,callback
  $scope.blockTimeslot = function(storeId,timeslot){

    var halalDate = DateHelper.cleanDate($scope.timeslotDate.date);
    
   //console.log('Attempting to block timeslot : ' + JSON.stringify(timeslot));

    Timeslot.blockTimeslot(storeId,halalDate, timeslot.time.from_time, timeslot.time.to_time, function(response){

      if (response.success){
        timeslot.time.available = false;
        toaster.pop('error', 'Disabled', 'Blocked timeslot ' + timeslot.time.display_name, 5000, 'trustedHtml');

      } else {
        toaster.pop('error', 'Error', response.message, 5000, 'trustedHtml');
      }

      
    }); 

  }

  $scope.unblockTimeslot = function(storeId,timeslot){

    var halalDate = DateHelper.cleanDate($scope.timeslotDate.date);

   //console.log(timeslot);

    Timeslot.enableTimeslot(timeslot.time.from_time,timeslot.time.to_time,halalDate,storeId, function(response){

      if (response.success){
        timeslot.time.available = true;
        toaster.pop('success', 'Enabled', 'Activated timeslot ' + timeslot.time.display_name, 5000, 'trustedHtml');
      } else {
        toaster.pop('error', 'Error', response.message, 5000, 'trustedHtml');
      }

      
    }); 

  }

  Timeslot.getTimeslots(function(data){

    var timeslots = data.objects;
    timeslots.forEach(function(timeslot){

    });
    $scope.timeslots = data.objects;

  });

  $scope.showAvailableSlotsForDate = function(storeId){

    if ($scope.timeslotDate.date){
        NotificationCenter.post('didStartLoading',null);
        Timeslot.getAvailableSlotsForDate(DateHelper.cleanDate($scope.timeslotDate.date), storeId, 

          function(response){
             //console.log('available timeslots : ' + JSON.stringify(response));
              $scope.timeslotsForDate = response[0].delivery_options;
              NotificationCenter.post('doneLoading',null);

        });
    }
  };

  $scope.oneAtATime = false;

  NotificationCenter.post('didStartLoading',null);

  $scope.getProductsInAisle = function(aisle,store){
    NotificationCenter.post('didStartLoading',null);
    ProductsInAisle.get({ aisle_id : aisle.id,
                          store_id : store.id}
                          , function(response){

                              aisle.products = response.objects;
                              NotificationCenter.post('doneLoading',null);
                          });

  }

  NotificationCenter.post('didStartLoading',null);
    Store.getAllStores(function(data){
        NotificationCenter.post('doneLoading',null);
        $rootScope.storeDetails = data.objects;
    });

  Refund.getAllRefunds(function(data){
    $scope.refundsGroupedByCart = Sorting.groupRefundsByCart(data.objects);
  });

  $scope.approveRefund = function(cartId,orderNumber){

          Refund.approveRefund(cartId, function(data){
            if (data.success){
              var dIndex = -1; //delete index
              $scope.refundsGroupedByCart.forEach(function(cart){

              cart.refunds.forEach(function(refund){
                    if (refund.orderNumber == orderNumber){
                      dIndex = cart.refunds.indexOf(refund);
                    }
                    if (dIndex != -1){

                      cart.refunds.splice(dIndex,1)
              
                    }
                  });
            });
            }
          });
        };
});

app.controller('AdminAislesCtrl', function($scope, $modal, $rootScope) {
	$scope.isCollapsed = true;
	$scope.addAisle = function (department) {

    $scope.department = department; //departments that this aisle can be added to (Restricted by store)
    var modalInstance = $modal.open({
      templateUrl: 'static/partials/modal-addAisle.html',
      controller: 'AddAisleModalInstanceCtrl',
      scope: $scope,
      resolve: {
        aisle: function () {
          return $scope.aisle;
        }
      }
    });

    modalInstance.result.then(function (aisle) {
      $scope.aisle = aisle;
    }, function () {
      //$log.info('Modal dismissed at: ' + new Date());
    });
  };
});

//this actually gets called in admin.js. TODO: works but kind of backwards, revisit this
app.controller('AddAisleModalInstanceCtrl', function ($scope, $rootScope, $modalInstance, aisle, toaster, Aisle, Store) {

  $scope.aisle = {};
  $scope.storeDetails = $rootScope.storeDetails;

  //fetch the departments
  angular.forEach($rootScope.storeDetails, function(store){
            if (store.id == $scope.aisle.store_id) {
              $scope.departments = store.departments;
            }
          });

  $scope.pop = function(){
        toaster.pop('success', "Aisle successfully created");
    };

  $scope.save = function () {

    //yes we have to recreate the object here because of JS protypical inheritance
    var aisle = {} ;
    aisle.aisle_name = $scope.aisle.aisle_name;
    aisle.store_aisle_number = $scope.aisle.store_aisle_number;
    aisle.department_id = $scope.department.id;

    Aisle.addAisle(aisle, function(data){
      if (data.success){
        $scope.pop();
          Store.getFullStoreDetails(function(data){
            $rootScope.storeDetails = data.objects;
            
        });
      }
      $modalInstance.close($scope.aisle);
    });    
  };

  $scope.close = function () {
    $modalInstance.close('cancel');
  };
});

app.controller('AdminDepartmentsCtrl', function($scope, $modal, $rootScope) {
  $scope.isCollapsed = true;

  //fixme: child scope for some reason does not work here. check ng-click on the button
  $rootScope.addDepartment = function (store) {

    $scope.store = store; //store that this department will be added to

    var modalInstance = $modal.open({
      templateUrl: 'static/partials/modal-addDepartment.html',
      controller: 'AddDepartmentModalInstanceCtrl',
      scope: $scope,
      resolve: {
        department: function () {
          //$scope.department.store_id = store.id;
          return $scope.department;
        }
      }
    });

    modalInstance.result.then(function (department) {
      $scope.department = department;
    }, function () {
      //$log.info('Modal dismissed at: ' + new Date());
    });
  };
});

//this actually gets called in admin.js. TODO: works but kind of backwards, revisit this
app.controller('AddDepartmentModalInstanceCtrl', function ($scope, $rootScope, $modalInstance, department, toaster, Department, Store) {

  $scope.department = {};

  $scope.storeDetails = $rootScope.storeDetails; //wtf

  $scope.pop = function(){
        toaster.pop('success', "Department successfully created");
    };

  $scope.save = function () {

  Department.addDepartment($scope.department.department_name, $scope.store.id, function(data){
      if (data.success){
        $scope.pop();
        Store.getAllStores(function(data){
            $rootScope.storeDetails = data.objects;
            
        });
      }
      $modalInstance.close($scope.department);
    });    
  };

  $scope.close = function () {
    $modalInstance.close('cancel');
  };

});