$('#wrapper').on('click','a.accordion-toggle',function(e) {
 //console.log("clicked");
  

        e.preventDefault();

      
      
      // If the sidebar is collapsed or in mobile mode(same thing) disable the ability to
      // collapse menu items as the entire menu has been converted for on hover use
      if ($('body').hasClass('mobile-viewport') || $('body').hasClass('sidebar-collapsed') ) {
        if ($('body').hasClass('sidebar-persist')) {}
        else if ($(this).parents('ul.sub-nav').hasClass('sub-nav')) { }
        else {return}         
      };
      
      // Check to see if target menu is a third level nav menu. If so don't collapse parent menus
      if ($(this).parents('ul.sub-nav').hasClass('sub-nav')) {
        $(this).next('.sub-nav').slideUp('fast', 'swing', function() {
          $(this).attr('style','').prev().removeClass('menu-open');
        });
      }
      // If not a third level nav menu collapse all open menus, remove open-menu class and any animation attributes
      else {
        $('a.accordion-toggle.menu-open').next('.sub-nav').slideUp('fast', 'swing', function() {
          $(this).attr('style','').prev().removeClass('menu-open');
        });
      }
      
      // Expand targeted menu item, add open-menu class and remove 
      // left over animation attributes
      if (!$(this).hasClass('menu-open')) {
        $(this).next('.sub-nav').slideToggle('fast', 'swing', function() {
          $(this).attr('style','').prev().toggleClass('menu-open');
        });
      }


    });