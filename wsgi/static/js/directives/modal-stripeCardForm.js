app.controller('StripeFormModalCtrl', function ($scope, $modal, $log,$rootScope, NotificationCenter) {

  $scope.open = function () {

    var modalInstance = $modal.open({
      templateUrl: 'static/partials/stripeCardForm.html',
      controller: 'StripeFormModalInstanceCtrl',
      resolve: {
        card: function () {
          return $scope.card;
        },
        saveTheCard: function(){
          return true;
        }
      }
    });

    modalInstance.result.then(function (card) {
      $scope.card = card;
    }, function () {

      NotificationCenter.post('doneLoading',null);
      //$log.info('Modal dismissed at: ' + new Date());
    });
  };
});

app.controller('StripeFormModalInstanceCtrl', function (NotificationCenter,toaster,$scope, $rootScope, $modalInstance, card, Payment, usSpinnerService, saveTheCard) {

  //reminder: this controller uses the child scope of its creator
  $scope.save = {};
  $scope.getImageForCardType = Payment.getImageForCardType;

  $scope.didSubmit = function(){
    //NotificationCenter.post('didStartLoading',null);
  }

  //$scope.save.saveTheCard = saveTheCard;

  $scope.handleStripe = function(status, response){

        if(response.error) {

          // there was an error. Fix it.

          var errorMessage;

          if (response.error){
            errorMessage = response.error;
          } else if (response.message){
            errorMessage = response.message;
          } else {
            errorMessage = 'Error';
          }

          toaster.pop('error', errorMessage, 'There was an error with your credit card information. Please try again.', 11000, 'trustedHtml');


        } else {
          //fixme: shouldnt be using rootScope for this...
            if (!$rootScope.cards){
              $rootScope.cards = [];
            }
            var newCard = response.card;
            newCard.tokenId = response.id;
            newCard.image = Payment.getImageForCardType(response.card.type?response.card.type:response.card.brand);
            newCard.id = response.card.id;
            newCard.last4 = response.card.last4;
            $rootScope.newCard = newCard;
            $rootScope.saveCard = $scope.save.saveTheCard;

            toaster.pop('success', "Successfully added credit card");
            $modalInstance.close(card);

        }

        NotificationCenter.post('doneLoading',null);

        
        
      };

  $scope.close = function () {
    $modalInstance.close('cancel');
  };
});