app.directive("storeonload", ['$timeout', 'CacheManager',
  function(timer, CacheManager) {

    return {
      link: function() {
        timer(function() {
          
          storeSwitcher();
          cartSlide(CacheManager);
          $('.scroller').perfectScrollbar('update');
      if ($("body").hasClass("active-cart")) {
        $("#page-wrapper").animate({
        paddingRight: "325px"
      }, 200)
  }            
        }, 0);

      }
    }

  }
]);

function storeSwitcher() {
  
  $("#accordian div").click(function() {

    $("#accordian").removeClass("open-store-select");

    if (!$("#stores-list").is(":visible")) {

      $("#stores-list").slideDown();
      $("#accordian").addClass("open-store-select");
    }
    else{

      $("#stores-list").slideUp();
    }
  })

   $("#accordian ul li ").click(function() {
    $("#accordian").removeClass("open-store-select");
       $("#stores-list").slideUp();

   }) 
}

function totalsSlider() {
  $("#totals-toggler").click(function() {

    $("#cart-totals").removeClass("opened-subtotals");

    if (!$("#subtotals").is(":visible")) {

      $("#subtotals").slideDown();
      $("#cart-totals").addClass("opened-subtotals");
    }
    else{

      $("#subtotals").slideUp();
    }
  })
}


function cartSlide(CacheManager) {
  
  $("#cart-togller").click(function() {
    
    $("#accordian ul").slideUp();

    if ($("body").hasClass("active-cart")) {
      CacheManager.cache('cartState','cartClosed');
      $("body").removeClass("active-cart");
        var wrapperWidth = $("#page-wrapper").width();
        var origWidth=wrapperWidth+325
      $("#page-wrapper").animate({
        paddingRight: "0px"
      }, 200)

    //$rootScope.isCartOpen=false;
    //console.log($rootScope.isCartOpen);
  } else {
      CacheManager.cache('cartState','cartOpen');
      $("body").addClass("active-cart");
      $("#page-wrapper").animate({
        paddingRight: "325px"
      }, 200)
      

    //$rootScope.isCartOpen=true;
     //console.log($rootScope.isCartOpen);
    }
  });
  
}

function showCart() {

      $("body").addClass("active-cart");
      $("#page-wrapper").animate({
        paddingRight: "325px"
      }, 200)
     // $rootScope.isCartOpen=true;
    }



