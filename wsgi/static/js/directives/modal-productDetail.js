app.controller('ProductDetailModalCtrl', function ($scope, $modal, $log, Order) {



  $scope.viewProduct = function (product) {
    var modalInstance = $modal.open({
      templateUrl: 'static/partials/productDetail.html',
      controller: 'ProductDetailModalInstanceCtrl',
      resolve: {
       product: function () {
          //may be opening from product list OR cart 
          return $scope.product?$scope.product:$scope.item.product;
        }
      }
    });

    modalInstance.result.then(function (product) {
      $scope.product = product;
    }, function () {
      //$log.info('Modal dismissed at: ' + new Date());
    });
  };
});

app.controller('ProductDetailModalInstanceCtrl', function (NotificationCenter, $scope, $rootScope, $modalInstance, product, CartResource, Cart) {

  $scope.product = product;

  // Fire GA ec event
  var aisleName = $rootScope.selectedAisle.aisle_name;
  var dptName = $rootScope.selectedDepartment.department_name;
  var storeName = $rootScope.selectedStore.name;

  // impressionFieldObject
  var productImpression = {
    'id': product.id,                 
    'name': product.name,
    'category': storeName + '/' + dptName + '/' + aisleName,
    'brand': product.brand_name,
    'price' : product.price,
    'list' : aisleName
  };

  // productFieldObject
  var productData = {
    'id': product.id,                 
    'name': product.name,
    'category': storeName + '/' + dptName + '/' + aisleName,
    'brand': product.brand_name,
    'price' : product.price,
    'list' : aisleName
  };

  // The impression from an Aisle section.
  ga('ec:addImpression', productImpression);

  // The product being viewed.
  ga('ec:addProduct', productData);

  ga('ec:setAction', 'click'); // Register the click

  ga('send', {
                hitType: 'event',
                eventCategory: 'Product detail',
                eventAction: storeName + '/' + dptName + '/' + aisleName,
                eventLabel:  product.name
            });

  function getCartItem(productId){
    var cartItem;
    /* 
      Since ProductModal is a child controller of ProductList,
      We use $rootScope NOT to share between sibling controllers, but to share between parent-child controllers
      we are nested inside an ng-repeat, which creates its own scope. So we are 3 scope levels down. 
      no love lost
    */
    $rootScope.allCartItems.forEach(function(item){
      if (item.product.id == productId){
        cartItem = item;
      }
    });

    return cartItem;
  }

  $scope.addProductToCart = function(product){
        product.quantityInCart = 1;
        Cart.addProductToCart(product,$rootScope.cart_id);
        ga('ec:addProduct', productData);
        ga('ec:setAction', 'add');
    };

  $scope.removeProductFromCart = function(product){

      if (product.quantityInCart != 1) return;
      var cartItem = getCartItem(product.id)

      if (cartItem) {
        Cart.removeProductFromCart(cartItem);
      }

  }

  $scope.increaseProductQuantity = function(product){
      var cartItem = getCartItem(product.id);
      if (cartItem){

        Cart.increaseItemQuantity(cartItem,function(response){

          if (response.success){
              NotificationCenter.post('didIncreaseCartItemQuantity', cartItem);
            
          }
          
          
        });
      }
    };

  $scope.decreaseProductQuantity = function(product){
      var cartItem = getCartItem(product.id);
      if (cartItem && product.quantityInCart){

        Cart.decreaseItemQuantity(cartItem,function(response){

          if (response.success){
            NotificationCenter.post('didDecreaseCartItemQuantity', cartItem);
          }
          
          
        });
      }
    };

  $scope.close = function () {
    $modalInstance.close($scope.product);
  };
});
