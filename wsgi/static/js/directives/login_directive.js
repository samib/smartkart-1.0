app.directive("resize", ['$timeout','$location','$rootScope',
    function(timer, location, rootScope) {
        return {

            link: function(scope, element, attrs, controller) {

                timer(function() {
                    if ($(window).width() > 1024) var w = $(window).width();
                    else var w = $(window).width();
                    $('.top_box_left, .top_box_right').css('border-left-width', w);
                    $('.bot_box_left, .bot_box_right').css('border-right-width', w);

                    //easy scroll
                    $(".easyscroll").click(function(event) {
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: 0
                        }, 'slow');

                    });

                    showForm(location, rootScope);

                    hasChangeCheck();

                }, 0);
            }
        }
    }
]);

function showForm(location, rootScope) {
    var hashwindow = location.$$path;
    var hash = hashwindow.replace("/", "");
    var hashs = ["register", "login", "forgot"];
    if (hash == "register") {
        if (false) {
            location.path('/noregister')
        } else {

            $("#" + hash + "-form").show().animate({
                opacity: 1,
            }, {
                duration: 300,
                queue: false
            });
            $("#" + hash + "-form").animate({
                marginRight: '0px'
            }, {
                duration: 300,
                queue: false
            });
        }
    } else if (hash == "noregister") {

        $("#postal-form").effect("bounce", 500);

    } else if (hash != "" && hashs.indexOf(hash) != -1) {

        $("#" + hash + "-form").show().animate({
            opacity: 1,
        }, {
            duration: 300,
            queue: false
        });
        $("#" + hash + "-form").animate({
            marginRight: '0px'
        }, {
            duration: 300,
            queue: false
        });

    } else {

        $("#postal-form").show("slide", {
            direction: "up"
        }, 500);

    }

}

app.directive('focus', [function() {
  var FOCUS_CLASS = "ng-focused";
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attrs, ctrl) {
      ctrl.$focused = false;
      element.bind('focus', function(evt) {
        element.addClass(FOCUS_CLASS);
        scope.$apply(function() {ctrl.$focused = true;});
      }).bind('blur', function(evt) {
        element.removeClass(FOCUS_CLASS);
        scope.$apply(function() {ctrl.$focused = false;});
      });
    }
  }
}]);

app.directive('formAutofillFix', function() {
  return function(scope, elem, attrs) {
    // Fix autofill issues where Angular doesn't know about autofilled inputs
    if(attrs.ngSubmit) {
      setTimeout(function() {
        elem.unbind('submit').submit(function(e) {
          e.preventDefault();
          elem.find('input, textarea, select').trigger('input').trigger('change').trigger('keydown');
          scope.$apply(attrs.ngSubmit);
        });
      }, 0);
    }
  };
});

function hasChangeCheck() {
    $(window).resize(function() {
        var w = $(window).width();
        if (w < 768)
            var ipadw = w
        if (w > 768)
            var ipadw = w - 15

        $('.top_box_left, .top_box_right').css('border-left-width', w);
        $('.bot_box_left, .bot_box_right').css('border-right-width', w);
    });
}