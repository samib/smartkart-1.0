app.controller('TimePickerCtrl', function ($scope) {
  $scope.deliveryTime = new Date();

  $scope.hstep = 1;
  $scope.mstep = 5;

  $scope.options = {
    hstep: [1, 2, 3],
    mstep: [1, 5, 10, 15, 25, 30]
  };

  $scope.ismeridian = true;
  $scope.toggleMode = function() {
    $scope.ismeridian = ! $scope.ismeridian;
  };

  $scope.update = function() {
    var d = new Date();
    d.setHours( 14 );
    d.setMinutes( 0 );
    $scope.deliveryTime = d;
  };

  $scope.changed = function () {
    //console.log('Time changed to: ' + $scope.deliveryTime);
  };

  $scope.clear = function() {
    $scope.deliveryTime = null;
  };
});