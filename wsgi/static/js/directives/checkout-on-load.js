app.directive('checkoutOnLoad',['$timeout', function(timer) {
    return {
        link: function(scope, element, attrs) {
              
           	scope.$on('jqueryForCheckout', function(){
                
        		timer(checkDOM,0);
                
        	});

        	var checkDOM = function(){
                timer(function(){
                    $.getScript("/static/js/input.js");
                }, 0);
        	 	$('#checkoutWizard').bootstrapWizard();
	            $('#checkoutWizard').bootstrapWizard('show',0);
	            $('.no-click').click(function(e) { return false; });
           
	        }



           
                
            
        	
        	//note there is no timer added, the timer just acts as a async thing to run after the dom has finished rendering
        	//here is the link og the blog: http://blog.brunoscopelliti.com/run-a-directive-after-the-dom-has-finished-rendering
        	       	
            
        }
    };
}]);

app.directive('onlyDigits', function () {

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;
            ngModel.$parsers.unshift(function (inputValue) {
                if(inputValue != undefined)
                    var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
                ngModel.$viewValue = digits;
                ngModel.$render();
                return digits;
            });
        }
    };
});

