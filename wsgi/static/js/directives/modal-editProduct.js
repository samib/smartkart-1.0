app.controller('EditProductModalCtrl', function ($scope, $modal, $log, $rootScope, $cookies) {

  $scope.editProduct = function (product,aisleId,departmentId,storeId) {

    var modalInstance = $modal.open({
      templateUrl: 'static/partials/modal-editProduct.html',
      controller: 'EditProductModalInstanceCtrl',
      resolve: {
        product: function () {
          $scope.product.aisle_id = aisleId;
          $scope.product.department_id = departmentId;
          $scope.product.store_id = storeId;
          return $scope.product;
        },
        aisle: function(){
          console.log('aisle : ' + JSON.stringify($scope.aisle));
          return $scope.aisle;

        }
      }
    });

    modalInstance.result.then(function (product) {
      $scope.product = product;
    }, function () {
      //$log.info('Modal dismissed at: ' + new Date());
    });
  };
});

app.controller('EditProductModalInstanceCtrl', function ($scope, $rootScope, $modalInstance, product, Product, Store, toaster,aisle) {

  $scope.selectedStore = {};
  $scope.product = typeof product != 'undefined'? product : {};
  $scope.storeDetails = $rootScope.storeDetails;

  console.log('aisle : ' + JSON.stringify(aisle));
  console.log('product : ' + JSON.stringify(product));

  //fetch the departments
  angular.forEach($rootScope.storeDetails, function(store){
            
            if (store.id == $scope.product.store_id) {
              $scope.departments = store.departments;
            }
          });

  $scope.updateAisles = function(){

   //console.log(JSON.stringify($scope.product.store_id));

    //get the store associated with thtis id, fetch all the department objects for that store and update scope.departments
    //TODO: encapsulate the findById for JSON objects or turn it into a service

  angular.forEach($scope.departments, function(department){
            
            if (department.id == $scope.product.department_id) {
              $scope.aisles = department.aisles;
            }

          });
  };

  $scope.updateAisles();

  $scope.pop = function(message){
        toaster.pop('success', message);
    };

  $scope.save = function () {

    //yes we have to recreate the object here because of JS protypical inheritance or some bs
    var product = {} ;
    product.id = $scope.product.id ? $scope.product.id : null;
    product.name = $scope.product.name;
    product.brand_name = $scope.product.brand_name;
    product.description = $scope.product.description;
    product.cost_price = $scope.product.price;
    product.mark_up_category = $scope.product.mark_up_category;
    product.image_url = $scope.product.image_url;
    product.store_id = $scope.product.store_id;
    product.department_id = $scope.product.department_id;
    product.aisle_id = $scope.product.aisle_id;
    product.taxable = $scope.product.taxable;
    product.product_available = $scope.product.product_available?$scope.product.product_available:false;
    product.position_on_aisle = $scope.product.position_on_aisle;

    //If a product ID is already defined, we know that we are editing an existing product and not creating a new one
    if (product.id && product.cost_price){
        Product.updateProduct(product, function(data){
            if (data.success){
              Store.getAllStores(function(data){
                  $rootScope.storeDetails = data.objects;
                  $modalInstance.close($scope.product);
              });
              $scope.pop('Successfully updated product');
            }
            $modalInstance.close($scope.product);
          }); 

    } else {

        Product.addProduct(product, function(data){
            if (data.success){
             
             product.id = data.new_product_id; 
              aisle.products.push(product);
             $modalInstance.close($scope.product);

              $scope.pop('Sucessfully created product');
            }
            $modalInstance.close($scope.product);
          });

    }

   
  };

  
  $scope.close = function () {
    $modalInstance.close($scope.product);
  };
});