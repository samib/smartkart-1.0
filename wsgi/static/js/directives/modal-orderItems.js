app.controller('OrderModalCtrl', function (NotificationCenter, $scope, $modal, $log, Order, $rootScope, $cookies, toaster, Driver) {

  $scope.cancelOrder = function(order){

    var cancelledOrder = order;
    NotificationCenter.post('didStartLoading',null);
    Order.getOrderStatus(cancelledOrder.id, function(data){
      NotificationCenter.post('doneLoading',null);
      if(data.order_status == "ASSIGNED TO SHOPPER"){
        var modalInstance = $modal.open({
          templateUrl: 'static/partials/modal-CannotCancelOrderAlert.html',
          controller: 'CancelOrderBanCtrl',
          resolve: {
            order: function () {
              return $scope.order;
            }
          }
        });
        //popup a modal saying that the prder has already been assigned to a shopper, and set the order status to pending
      } else if(data.order_status == "PENDING"){
        var modalInstance = $modal.open({
          templateUrl: 'static/partials/modal-ConfirmCancel.html',
          controller: 'CancelOrderModalInstanceCtrl',
          resolve: {
            order: function () {
              return $scope.order;
            }
          }
        });
      }
    }, function(error){
      alert(JSON.stringify(error));
    });
 };

  $scope.assignOrder = function(order){
    
    function assignCallBack(response){

        NotificationCenter.post('doneLoading',null);
        if (response.success){
          var message = 'Assigned Order #' + order.order_number + ' to ' + Driver.getDriverName(order.assigned_driver.id);
          toaster.pop('info', message);

        } else {

          toaster.pop('error', 'There was an error in assigning this order.');

        }
    }
    
    Order.assignOrder(order.id, order.assigned_driver.id, assignCallBack);
  };

  $scope.viewOrder = function (order) {

    var modalInstance = $modal.open({
      templateUrl: 'static/partials/orderModal.html',
      controller: 'OrderModalInstanceCtrl',
      resolve: {
        order: function () {
          return $scope.order;
        }
      }
    });

    modalInstance.result.then(function (order) {
      $scope.order = order;
    }, function () {
      //$log.info('Modal dismissed at: ' + new Date());
    });
  };
});

app.controller('OrderModalInstanceCtrl', function ($scope, $rootScope, $modalInstance, order, Order, Cart) {

  $scope.order = order;
  $scope.order.cartItems = order.items;
 //console.log(JSON.stringify($scope.order.cartItems));
  $scope.isAdmin = $rootScope.isAdmin;

  $scope.close = function () {
    $modalInstance.close($scope.order);
  };
});

app.controller('CancelOrderModalInstanceCtrl', function ($scope, $rootScope, $modalInstance, Order, order, toaster,NotificationCenter) {

  $scope.isLoading = false;

  $scope.cancel = function(){
    $scope.isLoading = true;
    Order.cancelOrder(order.id, function(data){
      
      if(data.success == true){
          order.status = 'CANCELED';
          toaster.pop('success', "Order was successfully cancelled");

        } else {

          toaster.pop('error', "There was an error in cancelling your order");

        }
        $scope.isLoading = false;
        $modalInstance.close($scope.order);
   });
  };

  $scope.close = function () {
    $modalInstance.close($scope.order);
  };
});

app.controller('CancelOrderBanCtrl', function ($scope, $rootScope, $modalInstance, $timeout, order) {
    $timeout(function(){
        order.status = 'ASSIGNED TO SHOPPER';
        $modalInstance.close($scope.order);
    },2000); 
});


