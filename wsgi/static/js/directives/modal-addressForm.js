app.controller('AddressModalCtrl', function ($scope, $modal, $log, Address,$rootScope,NotificationCenter) {


  $scope.deleteShippingAddress = function(address){
    NotificationCenter.post('didStartLoading',null);
    Address.deleteShippingAddress(address, function(data){
        NotificationCenter.post('doneLoading',null);
      //remove from our local cache of addresses
       $rootScope.ownAddresses.splice($rootScope.ownAddresses.indexOf(address),1);

     });

  };

  $scope.open = function () {

    var modalInstance = $modal.open({
      templateUrl: 'static/partials/addressForm.html',
      controller: 'AddressModalInstanceCtrl',
      resolve: {
        address: function () {
          return $scope.address;
        }
      }
    });

    modalInstance.result.then(function (address) {
      $scope.address = address;
    }, function () {
      //$log.info('Modal dismissed at: ' + new Date());
    });
  };
});

app.controller('AddressModalInstanceCtrl', function (toaster, $scope, $rootScope, $modalInstance, address, Address,NotificationCenter) {

  //reminder: this controller uses the child scope of its creator

  //using this to go one level down in inheritance, scope issue with child controller instance
  $scope.address = { label: 'Home' };
  
  //just in case ck
  if (typeof $rootScope.ownAddresses == 'undefined')
  {
    $rootScope.ownAddresses = new Object();
  }

  $scope.ok = function () {

    //yes we have to recreate the object here because of JS protypical inheritance or some bs
    var address = {} ;
    address.label = $scope.address.label;
    address.note = $scope.address.note;
    address.apt_number = $scope.address.apt_number;
    address.street_address = $scope.address.street_address;
    address.postal_code = $scope.address.postal_code;

    NotificationCenter.post('didStartLoading',null);
    Address.addShippingAddress(address, function(data){

      if (data.success){
        $scope.address.id = data.shipping_address_id;
        $rootScope.chosenAddress = $scope.address;
        $rootScope.ownAddresses.push($scope.address);
        $modalInstance.close($scope.address);
      } else {
        var wrongShippingAddr = document.getElementById("wrongShippingAddr");
        wrongShippingAddr.style.display = 'block';
        $scope.message = data.message;
        $scope.showMessage = true;
      }
      NotificationCenter.post('doneLoading',null);


    });    
  };

  $scope.close = function () {
    $modalInstance.close('cancel');
  };
});