app.directive("partial", function() {
  return {
    restrict: "E",        // directive is an Element (not Attribute)
    templateUrl: "static/partials/partial_header.html",          // replacement HTML (can use our scope vars here)
    replace: true,        // replace original markup with template
    transclude: false    // do not copy original HTML content
    
    // link: function (scope, element, attrs, controller) {…}
  }
});   