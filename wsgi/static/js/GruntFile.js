module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      
      js:{
        src:[
        'vendor/jquery-1.10.2.min.js',
        'vendor/jquery-ui-1.10.3.custom.min.js',
        'vendor/jqueryBlockUI.js',
        'vendor/jquery.maskedinput.min.js',
        'lib/angular.min.js',
        'lib/angular-animate.min.js',
        'lib/angular-cookies.js',
        'lib/angular-resource.min.js',
        'lib/angular-route.min.js',
        'lib/angular-ui-router.js',
        'vendor/ui-bootstrap-tpls-0.10.0.js',
        'lib/angular-local-storage.js', 
        'vendor/truncate.js', 
        'jquery.mousewheel.js', 
        'perfect-scrollbar.js', 
        'directives/angular-perfect-scrollbar.js',
        'directives/dirPagination.js', 
        'lib/loading-bar.js',

        'app.js',
        'services.js',
        'controllers/main.js',
        'controllers/cart.js',
        'controllers/login_signup.js', 
        'controllers/driverDashboard.js',
        'directives/modal-stripeCardForm.js',
        'controllers/departments.js',
        'controllers/checkout.js',
        'directives/datePicker.js',
        'directives/match.js',
        'directives/timePicker.js',
        'controllers/misc.js',
        'directives/partial_directive.js',
        'directives/login_directive.js',
        'directives/store-on-load.js',
        'directives/checkout-on-load.js',
        'lib/angular-payments.min.js',
        'lib/spin.min.js',
        'lib/ladda.min.js',
        'directives/modal-addressForm.js',
        'directives/modal-orderItems.js',
        'directives/modal-productDetail.js',
        'controllers/orders.js',
        'controllers/admin.js',
        'controllers/search.js',
        'directives/modal-editProduct.js',
        'vendor/toaster.js',
        'vendor/bootstrap.min.js',
        'vendor/jquery.validate.min.js',
        'vendor/spin.js',
        'vendor/angular-spinner.js',
        'jquery.bootstrap.wizard.js',
        'side-menu.js',
        'directives/backbuttonDirective.js'
        ],
        dest:'build/app.js'
      }

    },

    ngAnnotate:{

      app:{
        files:{

          'build/app.js' : ['build/app.js']

        }
      }

    },

    uglify:{

      js : {

        src:['build/app.js'],
        dest:'build/app.js'

      }

    }

  });

  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');



  grunt.registerTask('default', ['concat','ngAnnotate','uglify']);

};