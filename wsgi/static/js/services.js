'use strict';
app.service('NotificationCenter', function($rootScope){ 

    this.post = function(notificationName,params){
        $rootScope.$broadcast(notificationName,params);
    };
    
});

app.service('browser', ['$window', function($window) {

     return function() {

         var userAgent = $window.navigator.userAgent;

        var browsers = {chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i};

        for(var key in browsers) {
            if (browsers[key].test(userAgent)) {
                return key;
            }
       };

       return 'unknown';
    }

}]);

app.factory('HttpService', function($http,$cookies) {
  return {
    patchRequest: function(url,params,callback) {
            $http.defaults.headers.post["Content-Type"] = "application/JSON";
            $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
            $http({ method: 'PATCH', url: url, data: params }).success(callback?callback:function(response){});
          },
    patchRequestWithErrorCallback: function(url,params,callback,errorCallback) {
            $http.defaults.headers.post["Content-Type"] = "application/JSON";
            $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
            $http({ method: 'PATCH', url: url, data: params }).success(callback?callback:function(response){}).error(errorCallback?errorCallback:function(response){});
          },
    };
  });

app.factory('CardsResource', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'payment/cards', {}, {});
  }]);

app.factory('StoreResource', ['$resource', 'apiPrefix', function($resource,apiPrefix){
    return $resource(apiPrefix + 'current/store/:store_id/', { store_id : '@store_id' }, {
            // Static info, so we cache this - can be reused without having to make another HTTP request
            getAllStores:{method: 'GET', isArray:false, cache:true}
    });
  }]);

app.factory('DptResource', ['$resource', 'apiPrefix', function($resource,apiPrefix){
    return $resource(apiPrefix + 'department/:dptId/', { dptId : '@dptId' }, {
            // Static info, so we cache this - can be reused without having to make another HTTP request
            get:{method: 'GET', isArray:false, cache:true}
    });
  }]);

app.factory('CouponResource', ['$resource', 'apiPrefix', function($resource,apiPrefix){
    return $resource(apiPrefix + 'coupon/:couponId/', { couponId : '@couponId' }, {
            get:{method: 'GET', isArray:false, cache:false}
    });
  }]);

app.factory('AddressResource', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'shipping/', {}, {});
  }]);

app.factory('ProductsInAisle', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'asile/products/?aisle__id=:aisle_id&store__id=:store_id', 
              { 
                aisle_id : '@aisle_id',
                store_id : '@store_id' 

              }, {});
  }]);

app.service('ProductHelper', function(CartResource,$q){ 

    this.premarkCartQuantities = function(products,cartItems){
      // Iterate through cartItems, mark the products that ARE in the cart with respective quantities
          if (!products || !cartItems) return;
          cartItems.forEach(function(cartItem){
                  products.forEach(function(product){
                      if (cartItem.product.id == product.id){
                          product.quantityInCart = cartItem.quantity;
                      }
                  });
              });
      
    };
});

app.factory('PriceChangeRequests', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'product/?change=true', {}, {

            fetch : {
              method: 'GET', 
              isArray:false, 
              cache:false
            },

    });
  }]);

app.factory('DriverOrders', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'order/?d=t', {}, {

            //getting the cart
            fetch : {
              method: 'GET', 
              isArray:false, 
              cache:false
            },

    });
  }]);

app.factory('CartResource', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'cart/show/', {}, {
            //getting the cart
            retrieveCart:{
              method: 'GET', 
              isArray:true, 
              cache:false
            }
        });
  }]);

app.factory('UserResource', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'user/', {}, {});
  }]);

app.factory('OrderResource', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'order/:orderId/', { orderId : '@orderId'}, {});
  }]);

app.factory('MyOrdersResource', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'order/?a=f', {}, {});
  }]);

app.factory('AisleResource', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'aisle/:id/', { id : '@id'}, {});
  }]);

app.factory('Search', ['$resource', 'apiPrefix', function($resource, apiPrefix){
    return $resource(apiPrefix + 'search/?query=:queryString/', { queryString : '@queryString' }, {});
  }]);

app.factory('Sorting', function($rootScope,StoreResource) {
  return {
    sortOn: function (collection, name) {

      collection.sort(
        function( a, b ) {

          if ( a[ name ] <= b[ name ] ) {

            return( -1 );

          }

          return( 1 );

        }
        );

    },
    
    groupCartItemsByStore: function(cartItems,storeInfo) { //called on the items currently in the cart

          // Sort compare function
          function sortByIdCompare(firstItem,secondItem) {
            return secondItem.id - firstItem.id;
          }

          if (!(cartItems && storeInfo)) return;
          var self = this;
          var cartItemsByStore = []; //this is the grouped cartitems array

          //helper object for store information
          var indexedStoreInfo = {}; //will be used below as : indexedStoreInfo[store.id].delivery_fee etc.

          storeInfo.objects.forEach(function(store){
              indexedStoreInfo[store.id] = {
                store_logo_url : store.store_logo_url,
                delivery_fee : store.delivery_fee,
                minimum_order : store.minimum_order,
                min_order_for_free_delivery : store.min_order_for_free_delivery,
                name : store.name
              }
          });

          //premark cart items' store_id for Sorting
          cartItems.forEach(function(cartItem){
            if (!cartItem.store_id)
              cartItem.store_id = cartItem.product.store_id;
            });

          self.sortOn(cartItems, 'store_id' );

          var storeValue = "_INVALID_STORE_VALUE_";

          ////console.log('Sorting, storeInfo : \n' + JSON.stringify(storeInfo));
          //console.log('Sorting, cartItems : \n' + JSON.stringify(cartItems));
          //console.log('Sorting, indexedStoreInfo : \n' + JSON.stringify(indexedStoreInfo));

          for ( var i = 0 ; i < cartItems.length ; i++ ) {

            var cartItem = cartItems[ i ];
            
            if ( cartItem.store_id !== storeValue ) {
              //console.log('cartItem.store_id: ' + JSON.stringify(cartItem));
              var delivery_fee = indexedStoreInfo[cartItem.store_id].delivery_fee;
              var store_logo_url = indexedStoreInfo[cartItem.store_id].store_logo_url;
              var minimum_order = indexedStoreInfo[cartItem.store_id].minimum_order;
              var min_order_for_free_delivery = indexedStoreInfo[cartItem.store_id].min_order_for_free_delivery;
              var name = indexedStoreInfo[cartItem.store_id].name;
              var store = {
                id: cartItem.store_id,
                delivery_fee: delivery_fee,
                minimum_order: minimum_order,
                name: name,
                store_logo_url: store_logo_url,
                min_order_for_free_delivery : min_order_for_free_delivery,
                cartItems: []
              };

              storeValue = store.id;

              cartItemsByStore.push( store );

            }

            store.cartItems.push( cartItem );

            store.cartItems.sort(sortByIdCompare);

          }

          cartItemsByStore.forEach(function(store){
                var totalForStore = 0;
                store.cartItems.forEach(function(cartItem){

                    totalForStore += parseFloat(cartItem.product.price) * cartItem.quantity;

                });
                
                store.totalForStore = totalForStore;


                store.differenceAmountForFreeDelivery = store.min_order_for_free_delivery - totalForStore;
                store.isDeliveryFree = totalForStore >= store.min_order_for_free_delivery && store.is_free_delivery_active;
                store.hasReachedMinOrderForStore = totalForStore >= parseFloat(store.minimum_order);
          });


          return cartItemsByStore;
        },

        groupRefundsByOrder: function(refunds) {
          var self = this;
          var groupedRefunds = [];

          self.sortOn( refunds, 'order' );

          var orderNumber = "_INVALID_CART_ID_VALUE_";

          for ( var i = 0 ; i < refunds.length ; i++ ) {

            var refund = refunds[ i ];

            if ( refund[ 'order' ] !== orderNumber ) {

              var order = {
                orderNumber: refund[ 'order' ],
                refundedItems: []
              };

              orderNumber = order.orderNumber;

              groupedRefunds.push( order );

            }

            order.refundedItems.push( refund );
        } 

          return groupedRefunds;

        },
        groupRefundsByCart: function(allRefunds) {
          var self = this;
          var groupedCarts = [];

          self.sortOn( allRefunds, 'cart_id' );

          var cartIdValue = "_INVALID_CART_ID_VALUE_";

          for ( var i = 0 ; i < allRefunds.length ; i++ ) {

            var refund = allRefunds[ i ];

            if ( refund[ 'cart_id' ] !== cartIdValue ) {

              var cart = {
                id: refund[ 'cart_id' ],
                refunds: []
              };

              cartIdValue = cart.id;

              groupedCarts.push( cart );

            }

            cart.refunds.push( refund );
            
          } 

          groupedCarts.forEach(function(cart){

            cart.refunds = self.groupRefundsByOrder(cart.refunds);

          });
          return groupedCarts;
        },
        groupProductsByAisle: function(products) {
          var self = this;
          var groupedProducts = [];

          self.sortOn( products, 'aisle_id' );

          var aisleIdValue = "_INVALID_AISLE_ID_VALUE_";

          for ( var i = 0 ; i < products.length ; i++ ) {

            var product = products[ i ];

            if ( product[ 'aisle_id' ] !== aisleIdValue ) {

              var aisle = {
                aisle_id: product[ 'aisle_id' ],
                aisle_name: product['aisle_name'],
                dptId: product.department_id,
                isCollapsed: false,
                products: []
              };

              aisleIdValue = aisle.aisle_id;

              groupedProducts.push( aisle );

            }

            aisle.products.push( product );
            
          } 
          return groupedProducts;
        },
        groupSearchResultsByStoreAisle: function(searchResults) {
          var self = this;
          var groupedResults = []; //results grouped by store

          self.sortOn( searchResults, 'store_id' );

          var storeIdValue = "_INVALID_STORE_ID_VALUE_";

          for ( var i = 0 ; i < searchResults.length ; i++ ) {

            var product = searchResults[ i ];

            if ( product[ 'store_id' ] !== storeIdValue ) {


              var store = {
                store_id: product[ 'store_id' ],
                store_name: product['store_name'],
                resultsByAisle: []
              };

              storeIdValue = store.store_id;

              groupedResults.push( store );

            }

            store.resultsByAisle.push( product );
            
          } 

          groupedResults.forEach(function(store){

            store.resultsByAisle = self.groupProductsByAisle(store.resultsByAisle);

          });

          return groupedResults;
        }

      };
    });

app.service('SearchHelper', function(Sorting,ProductHelper){ 

    this.harmonizeSearchResults = function(rawSearchResults,queryString,cartItems){
          rawSearchResults.forEach(function(product){

            // Necessary for grouping raw results by store and aisle
            product.store_id = product.store.id;
            product.store_name = product.store.name;
            product.aisle_id = product.aisle.id;
            product.aisle_name = product.aisle.aisle_name;
            product.department_id = product.department.id;

          });

          // Group search results by store and by aisle within store
          var groupedSearchResults = Sorting.groupSearchResultsByStoreAisle(rawSearchResults);

          // Premark per-store result counts and product quantities
          groupedSearchResults.forEach(function(store){
              store.resultCountForStore = 0;
              store.resultsByAisle.forEach(function(aisle){
                  store.resultCountForStore += aisle.products.length;
                  ProductHelper.premarkCartQuantities(aisle.products,cartItems);
              });

          });

          // Might be useful for caching search results against their query string as key later on
          var searchResults = {

            groupedSearchResults : groupedSearchResults,
            queryString : queryString,
            count : rawSearchResults.length,
            rawSearchResults : rawSearchResults

          };
          return searchResults;
      
    };
});

app.service('CouponService', function($http,$cookies,NotificationCenter,HttpService,toaster) {

    function getUrl(id) {
      id = typeof id !== 'undefined' ? id : '';
      return 'api/aisle/' + id + '?format=json';
    }

    return {
      
      generateCoupons: function(couponInfo, callback, errorCallback) {
        $http.defaults.headers.post["Content-Type"] = "application/JSON";
        $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
        return $http.post('api/coupon/admin/create/', JSON.stringify(couponInfo)).success(callback).error(errorCallback);


      }, 

      reApplyCoupon: function(couponCode,cart_id,num_of_stores,callback){

          NotificationCenter.post('didStartLoading',null);
          HttpService.patchRequest('api/coupon/redeem/', {
              code: couponCode,
              cart_id: cart_id,
              num_of_stores: num_of_stores,
              re_apply: true
          }, function(response){

              NotificationCenter.post('doneLoading',null);

              if (!response.success)
              {
                  toaster.pop('error', 'Error redeeming coupon', response.message, 5000, 'trustedHtml');
                  
              } 

              if (callback) callback(response);

          });

      },

      redeemCoupon: function(couponCode,cart_id,num_of_stores,callback){
          NotificationCenter.post('didStartLoading',null);
          HttpService.patchRequest('api/coupon/redeem/', {
              code: couponCode,
              cart_id: cart_id,
              num_of_stores: num_of_stores
          }, function(response){

              NotificationCenter.post('doneLoading',null);
              //console.log('Response : ' + JSON.stringify(response));

              if (response.success)
              {
                  if (callback) callback(response);
              } else {

                  toaster.pop('error', 'Error redeeming coupon', response.message, 5000, 'trustedHtml');

              }

          });

      }

    };
  });

app.factory('LoginService', function ($q, $http, $cookieStore, $cookies) {
    var login = function (email, password) {

        $http.defaults.headers.post["Content-Type"] = "application/JSON";
        $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;

        var deferred = $q.defer();
        $http.post('api/user/login/', {
                'email': email,
                'password': password
        }).success(function (data, status, headers, config) {
            var token = getCookie('csrftoken');
           //console.log('token =========== : ' + token);
            $http.defaults.headers.common["X-CSRFToken"] = token;
            $cookieStore.put('userToken', token);
            deferred.resolve(data);
          }).error(function (data, status, headers, config) {
            deferred.reject(data);
          });
          return deferred.promise;
        };
        return {
          login: login
        };
      });
    

app.factory('RegisterService', function ($q, $http, $cookieStore) {
    var register = function (info) {
        var deferred = $q.defer();
        $http.post('api/user/registration/', {
                'first_name': info.firstName,
                'last_name': info.lastName,
                'email': info.email,
                'password': info.password
        }).success(function (data, status, headers, config) {
            var token = getCookie('csrftoken');
            // var token = someCrazyParsing(cookies); //<-- Your magic here
            $http.defaults.headers.common["X-CSRFToken"] = token;
            $cookieStore.put('userToken', token);
            deferred.resolve(data);
          }).error(function (data, status, headers, config) {
            deferred.reject(data);
          });
          return deferred.promise;
        };
        return {
          register: register
        };
      });

//service instead of factory because it is a singleton
app.service('CacheManager', function($rootScope, localStorageService){

    var self = this;

    this.cache = function(key,value){
        localStorageService.add(key, value);
    };

    this.get = function(key){
        return localStorageService.get(key);
    };
  
    this.clearAll = function(){

      localStorageService.clearAll();
    };   

    this.remove = function(key){
      localStorageService.remove(key);
    };
});

app.service('UserInfoHelper', function($rootScope,CacheManager){

    this.recordUserInfo = function(userInfo){

        var userProperties = ['isAdmin','isDriver','userName','is_first_order','email'];
        userProperties.forEach(function(property){

          // Load into root model and cache. Must be cleared on logout
          $rootScope[property] = userInfo[property];
          CacheManager.cache(property,userInfo[property]);

        });


      }
    
    this.didCompleteFirstOrder = function(){

          $rootScope['is_first_order'] = false;
          CacheManager.cache('is_first_order',false);

    }

    this.isFirstOrder = function(){

          if (!$rootScope.is_first_order){
              $rootScope.is_first_order = CacheManager.get('is_first_order');
          }

          if ($rootScope.is_first_order == 'false') return false;

          return $rootScope.is_first_order;

      }


    this.isAdmin = function(){

        if (!$rootScope.isAdmin){
            $rootScope.isAdmin = CacheManager.get('isAdmin');
        }

        return $rootScope.isAdmin;

    }

    this.isDriver = function(){

        if (!$rootScope.isDriver){
            $rootScope.isDriver = CacheManager.get('isDriver');
        }

        return $rootScope.isDriver;
    }

    this.getUserName = function(){

        if (!$rootScope.userName){
            $rootScope.userName = CacheManager.get('userName');
        }

        return $rootScope.userName;
    }

    this.getEmail = function(){

        if (!$rootScope.email){
            $rootScope.email = CacheManager.get('email');
        }

        return $rootScope.email;
    }

});

app.service('StoreHelper', function($rootScope,StoreResource,CacheManager,HttpService){

    var self = this;
    this.setLastStoreVisited = function(storeId, callback) {
      return HttpService.patchRequest('api/user/last_store/', { store_id:storeId },
        callback);
    }

    this.recordStoreInfo = function(callback){

        StoreResource.getAllStores(function(response){

              CacheManager.cache('globalStoreInfo', response.objects);

              $rootScope.globalStoreInfo = response.objects;

              $rootScope.globalDptInfo = {};
              $rootScope.globalAisleInfo = {};

              $rootScope.globalStoreInfo.forEach(function(store){

                // Group aisles by id
                
                store.departments.forEach(function(department){

                  $rootScope.globalDptInfo[department.id] = department;

                  // Group departments by id

                  department.aisles.forEach(function(aisle){

                    $rootScope.globalAisleInfo[aisle.id] = aisle;

                  });

                  CacheManager.cache('globalAisleInfo', $rootScope.globalAisleInfo);

                });

                CacheManager.cache('globalDptInfo', $rootScope.globalDptInfo);

              });


              if (callback) callback();
      });


    };

    this.getStoreById = function(storeId){

        var theStore;
        if (!$rootScope.globalStoreInfo){
            $rootScope.globalStoreInfo = CacheManager.get('globalStoreInfo');
        }
      
        $rootScope.globalStoreInfo.forEach(function(store){

        if (store.id == storeId){
            theStore = store;
          }

        });

        return theStore;

    };

    this.getAisleById = function(aisleId){

        if (!$rootScope.globalAisleInfo){
            $rootScope.globalAisleInfo = CacheManager.get('globalAisleInfo');
        }

        return $rootScope.globalAisleInfo[aisleId];


    };

    this.getDepartmentById = function(dptId){

        if (!$rootScope.globalDptInfo){
            $rootScope.globalDptInfo = CacheManager.get('globalDptInfo');
        }

        return $rootScope.globalDptInfo[dptId];

    };
});

//Store service
app.service('Store', function($http,$rootScope) {
    
    // TODO: Response too big, revisit this
    this.getFullStoreDetails = function(callback) {
      return $http.get('api/store/').success(callback);
    }

    this.getAllStores = function(callback) {
      return $http.get('api/current/store/').success(callback);
    }  
});

//Aisle service
app.factory('Aisle', function($http,$cookies,$rootScope) {

    function getUrl(id) {
      id = typeof id !== 'undefined' ? id : '';
      return 'api/aisle/' + id + '?format=json';
    }

    return {

      addAisle: function(aisle, callback) {
        $http.defaults.headers.post["Content-Type"] = "application/JSON";
        $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
        return $http.post('api/admin/aisle/add/', aisle).success(callback);
      }

      //TODO: setActive(active): admin/aisle/status (false for delete)
      //TODO: updateAisle: takes the whole aisle object to edit
    };
  });

//Department service
app.factory('Department', function($http,$cookies) {
    //must pass in storeId
    return {
      addDepartment: function(dptName, storeId, callback) {
        $http.defaults.headers.post["Content-Type"] = "application/JSON";
        $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
        return $http.post('api/admin/department/add/', { department_name : dptName, store_id : storeId }).success(callback);
      }
    };
});

//Order service
app.factory('Order', function($http,$cookies,HttpService) {

  var self = this;
  return {
    createOrder: function(orderInfo, callback, errorCallback) {
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
      return $http.post('api/order/create/', JSON.stringify(orderInfo)).success(callback).error(errorCallback);
    },
    assignOrder: function(orderId, driverId, callback) {
      return HttpService.patchRequest('api/order/assign/', {driver_id : driverId, order_id : orderId},
        callback);
      
    },
    cancelOrder: function(orderId, callback) {
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;

      return HttpService.patchRequest('api/order/update/', {order_id : orderId, order_status : 'CANCELED'}, callback);
    },
    retrieveOrdersByDay: function(date,callback) {
      return $http.get('api/order/?date=' + date).success(callback);
    },
    retrieveMyOrders: function(callback) {
      return $http.get('api/order/?a=f').success(callback);
    },
    getOrdersForRange: function(from,to,callback) {
      return $http.get('/api/order/?from=' + from + '&to=' + to).success(callback);
    },    
    getOrderStatus: function(orderId,callback) {
      return $http.get('api/order/status/?order_id=' + orderId).success(callback);
    },
    editPricePaid : function(orderId, pricePaid, taxPaid, callback){
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;

      return HttpService.patchRequest('api/management/order/expense/', {order_id : orderId, tax_paid : Number(taxPaid), price_paid : Number(pricePaid)}, callback);
    },
    getStatusIcons: function(status){
          return {
            'PENDING' : 'fa-clock-o',
            'FOUND' : 'fa-check-circle',
            'SUBSTITUTE' : 'fa-repeat',
            'REFUND' : 'fa-reply',
            'CANCELED' : 'fa-times-circle'
          }

      }
  };
});

app.service('Cart', function(NotificationCenter,$http, $rootScope, $cookies, HttpService,Sorting, $q, $cookieStore,Store,$stateParams, TAX_RATE, UserInfoHelper) {

    this.removeProductFromCart = function(cartItem,callback) {

            NotificationCenter.post('didRemoveCartItem', cartItem);
            $http.defaults.headers.post["Content-Type"] = "application/JSON;charset=UTF-8";
            $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
            return $http.delete('api/cart_item/remove/?id='+cartItem.id).success(function(response){



                                    if (!response.success){
                                        NotificationCenter.post('didAddCartItem', cartItem);
                                    } else {
                                      
                                        var product = cartItem.product;

                                        // Fire GA Event
                                        var eventDesc = UserInfoHelper.getUserName() + ' removed ' + product.name + ' (' + product.id + ')' + ' for $' + product.price + ' from ' + $rootScope.selectedStore.name;

                                        var aisleName = $rootScope.selectedAisle.aisle_name;
                                        var dptName = $rootScope.selectedDepartment.department_name;
                                        var storeName = $rootScope.selectedStore.name;
                                          // productFieldObject
                                        
                                        var productData = {
                                          'id': product.id,                 
                                          'name': product.name,
                                          'category': storeName + '/' + dptName + '/' + aisleName,
                                          'brand': product.brand_name,
                                          'price' : product.price,
                                          'list' : aisleName
                                        };

                                        ga('ec:addProduct', productData);
                                        ga('ec:setAction', 'remove');
                                        ga('send', {
                                                    hitType: 'event',
                                                    eventCategory: 'Cart item',
                                                    eventAction: UserInfoHelper.getUserName() + ' removed a cart item',
                                                    eventLabel:  eventDesc
                                                  });

                                    }
                            });
          }

    this.addProductToCart = function(product,cart_id,callback) {
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;application/JSON";
        $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
        return $http.post('api/cart_item/add/', 
          JSON.stringify({'product_id' : product.id, 
            'quantity' : '1',
            'cart_id'  : cart_id,
            'store_id' : $stateParams.storeId
          })
          ).success(function(response){

        if (response.success){
          //create new cart item to be pushed to listeners
          var newItem = {
                        id : parseInt(response.cart_item_id), 
                        quantity : 1,
                        price: product.price,
                        imageURL: product.image_url, 
                        tax : product.price * TAX_RATE,
                        product : product,
                        note : '',
                        store_id : parseInt($stateParams['storeId']) ,
                        cart : cart_id
                    };

          // Notify whoever is listening
          NotificationCenter.post('didAddCartItem', newItem);

        }else {
          product.quantityInCart = 0;
        }

      }).error(function(error){
            product.quantityInCart = 0;
           //console.log('Failure Adding product to cart : ' + JSON.stringify(error));
          });

        }
  
  this.hasStoreInCartUnderMinimumForFreeDelivery = function(){

        var isGoodForFreeDelivery = function(store){

          return Cart.getTotalPriceForStore(store.id) > store.min_order_for_free_delivery;
        };
        //this function is observed so if cartItemsByStore is not loaded yet, it will get called again at which time it should no longer be null
        var cartItemsByStore = self.getCartItemsByStore();
        return cartItemsByStore?!cartItemsByStore.every(isGoodForFreeDelivery):false;
    }

    this.decreaseItemQuantity = function(cartItem,callback) {
        HttpService.patchRequest('api/cart_item/update/', {'cart_item_id': cartItem.id, 'quantity':cartItem.quantity-1}, callback);
      }
    this.increaseItemQuantity = function(cartItem,callback) {
        HttpService.patchRequest('api/cart_item/update/', {'cart_item_id': cartItem.id, 'quantity':cartItem.quantity+1}, callback);
      }
});   


//Product service
app.service('Product', function($http, $cookies, $rootScope, Aisle, Cart, NotificationCenter, HttpService) {
  
    var self = this;

    this.approvePriceChange = function(productId, callback){
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;

      return HttpService.patchRequest('api/admin/product/price/approve/', {id : productId}, callback);
    }

    this.addProduct = function(product, callback) {
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
      return $http.post('api/product/admin/add_product/', product).success(callback);
    }
    this.updateProduct = function(product, callback) {
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
      return $http.put('api/product/update/', JSON.stringify(product)).success(callback);
    }

    this.editCostPrice = function(productId, cost_price, selling_price, callback){
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;

      return HttpService.patchRequest('api/admin/product/price/change/', {id : productId, new_cost_price : Number(cost_price), new_selling_price : Number(selling_price)}, callback);
    }
});

//Address service
app.factory('Address', function($http,$cookies,NotificationCenter) {

  return {

    addShippingAddress: function(addressInfo, callback) {
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;

        return $http.post('api/shipping/add/', JSON.stringify(addressInfo)
        ).success(callback);
    },

    //gets addresses saved for current user
    retrieveAddresses: function(callback){
      return $http.get('api/shipping/').success(callback);
    },
    deleteShippingAddress: function(shippingAddress,callback) {
      $http.defaults.headers.post["Content-Type"] = "application/JSON;charset=UTF-8";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
	  //return $http.delete('api/shipping/remove/', {'id' : shippingAddress.id}).success(callback);
      return $http.delete('api/shipping/remove/?id='+shippingAddress.id).success(callback).error(function(errorResponse){

          NotificationCenter.post('doneLoading',null);

      });
    },
  }
});

app.factory('Payment', function($http,$cookies,$rootScope,NotificationCenter) {

  return {
    pay: function(paymentInfo, cardId, isNewCard,callback, errorCallBack) {

      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
      return $http.post('api/payment/pay/', paymentInfo).success(callback).error(errorCallBack);
    },
    retrieveCards: function(callback, errorCallback) {
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;

      return $http.get('api/payment/cards').success(callback).error(errorCallback);
    },
    getImageForCardType: function(cardType) {

      if (!cardType){

        return "/static/img/credit-card.png";
      }

      if (cardType.toLowerCase() == 'mastercard'){

        return "/static/img/mastercard.png";

      } else if (cardType.toLowerCase() == 'american express'){

        return "/static/img/amex.png";

      } else if (cardType.toLowerCase() == 'visa') {

        return "/static/img/visa.jpg";

      }

    },

    removeCard: function(cardId, callback) {
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;

      return $http.post('api/payment/card/delete/', {'card_id':cardId}).success(callback).error(function(errorResponse){

          NotificationCenter.post('doneLoading',null);

      });
    }
  };
});

app.factory('DateHelper', function() {
  return {
          //Returns YYYY-MM-DD
          cleanDate: function (date){
              if (date){

                  var month = 1+ date.getMonth();
                  var clean = date.getFullYear() + '-' + month /* 0-based index for months */ + '-' + date.getDate();
                  return clean;

              }
          },
          // time is of format 12:00:00. Converting to 12:00 PM
          convert24hrTime: function (time){
              
            if (time){
                time = time.substring(0,time.length-3); // remove the seconds

              // Check correct time format and split into components
              time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

              if (time.length > 1) { // If time format correct
                time = time.slice (1);  // Remove full string match value
                time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
                time[0] = +time[0] % 12 || 12; // Adjust hours
              }
              return time.join (''); // return adjusted time or original string

            }

          },

          // Check IFF today or later
          checkDateIsTodayOrLater: function(date) {

            if (!date) return false;

             //get today's date in string
             var todayDate = new Date();
             //need to add one to get current month as it is start with 0
             var todayMonth = todayDate.getMonth() + 1;
             var todayDay = todayDate.getDate();
             var todayYear = todayDate.getFullYear();
             var todayDateText = todayDay + "/" + todayMonth + "/" + todayYear;
             //
             
            //get date input from SharePoint DateTime Control
             var inputDateText = date;
             
            //Convert both input to date type
            var inputToDate = Date.parse(inputDateText);
            var todayToDate = Date.parse(todayDateText);
             //
             
            //compare dates /*
            if (inputToDate > todayToDate) {}
            else if (inputToDate < todayToDate) {}
            else {}

            return !(inputToDate < todayToDate)

          }
        };
      });


app.factory('Driver', function($http,$rootScope) {
  return {
    getDriverName: function(driverId) {
      var name;
      $rootScope.drivers.forEach(function(driver){
          if (driver.id == driverId){
            name = driver.first_name + ' ' + driver.last_name;
          }
      });
      return name;
    },
    retrieveDriverOrders: function(callback) {
      return $http.get('api/order/?d=t').success(callback);
    },

    getAllDrivers: function(callback) {
      return $http.get('api/drivers').success(callback);
    }
  };
});

app.factory('Refund', function($http,HttpService) {
  return {
    getAllRefunds: function(callback) {
      return $http.get('api/refund/').success(callback);
    },
    approveRefund: function(cartId,callback) {
      //api/admin/refund/items (cartId) - use as a patch
      return HttpService.patchRequest('api/admin/refund/items/', {cart_id : cartId}, callback);
    },
    getRefundsForDate: function(date,callback) {
      return $http.get('api/refund?date='+date).success(callback);
    }
  };
});


app.factory('isEmpty', function() {

    var hasOwnProperty = Object.prototype.hasOwnProperty;

    return function isEmpty(obj) {
        var ret = true;

        var isUndefined = typeof obj == 'undefined';;

        if (obj == null || typeof obj == 'undefined') return true;

        if (obj.length > 0)    return false;
        if (obj.length === 0)  return true;

        for (var key in obj) {
            if (hasOwnProperty.call(obj, key) && obj[key] && obj[key] != "") ret = false;
        }

        return ret;
    }
      

});

app.factory('AddItemNote', function($http,$cookies,HttpService) {

  return {

    addNote: function(cartItemId, note, callback) {
      return HttpService.patchRequest('api/cart_item/note/', {cart_item_id : cartItemId, note : note},
        callback);
      
    }
  };
});

app.service('Timeslot', function($http, HttpService, $cookies, NotificationCenter){

    this.getTimeslots = function(callback){
      return $http.get('api/delivery/').success(callback).error(function(errorResponse){

          NotificationCenter.post('doneLoading',null);

      });
    }

    this.addTimeslot = function(timeslot,callback){
      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
      return $http.post('api/admin/delivery_time/add/', { delivery_time : timeslot }).success(callback).error(function(errorResponse){

          NotificationCenter.post('doneLoading',null);

      });

    }

    this.getAvailableSlotsForDate = function(date,storeId,callback){
      return $http.get('api/delivery/available/?date=' + date + '&store_id=' + storeId).success(callback).error(function(errorResponse){

          NotificationCenter.post('doneLoading',null);

      });
    }

    this.blockTimeslot = function(storeId,deliveryDate,fromTimeToBlock,toTimeToBlock,callback){

      $http.defaults.headers.post["Content-Type"] = "application/JSON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
      return $http.post('api/admin/delivery/block/', { store_id      : storeId,
                                                delivery_date : deliveryDate,
                                                from_time_to_block : fromTimeToBlock,
                                                to_time_to_block:  toTimeToBlock}).success(callback).error(function(errorResponse){

          NotificationCenter.post('doneLoading',null);

      })
      
    }

    this.enableTimeslot = function(fromBlockedTime,toBlockedTime,deliveryDate,storeId,callback){
      /*http://127.0.0.1:8080/api/admin/delivery/block/remove/?blocked_time=5:00 PM&date=2014-07-17&store_id=1*/
      $http.defaults.headers.post["Content-Type"] = "application/JS0ON";
      $http.defaults.headers.common["X-CSRFToken"] = $cookies.csrftoken;
      var requestUrl = 'api/admin/delivery/block/remove/?from_blocked_time='+fromBlockedTime+'&to_blocked_time='+toBlockedTime+'&date='+deliveryDate+'&store_id='+storeId;
      return $http.delete(requestUrl).success(callback).error(function(errorResponse){

          NotificationCenter.post('doneLoading',null);

      });
      
    }
});

app.factory('EditPhoneNumber', function($http,$cookies,HttpService) {

  return {

    EditPhone: function(orderId, phone, callback) {
      return HttpService.patchRequest('api/order/phone/', {order_id : orderId, phone : phone},
        callback);
      
    }
  };
});
