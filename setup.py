#!/usr/bin/env python

from setuptools import setup

setup(
    name='SmartKart Production',
    version='1.0',
    description='SmartKart Production Server',
    author='Dev Team',
    author_email='dev@smartkart.ca',
    url='http://www.python.org/sigs/distutils-sig/',
    install_requires=['Django==1.5','psycopg2', 'python-memcached','stripe>=1.9.2','django-tastypie>=0.9.11','South==0.8.2', 'django-celery', 'django-celery-email', 'pytz'],
)
